import { Component, OnInit } from '@angular/core';
import { Socket } from 'ngx-socket-io';
import { WebsocketService } from 'src/app/services/websocket.service';
import { CortesService } from 'src/app/services/cortes.service';

@Component({
  selector: 'app-cortes',
  templateUrl: './cortes.component.html',
  styleUrls: ['./cortes.component.css']
})
export class CortesComponent implements OnInit {

  public allCortes : any 
  public suma = 0
  public dateNow = new Date()

  constructor(
    private socket: Socket,
    private websocketService: WebsocketService,
    private cortesService: CortesService) { 
  }

  ngOnInit() 
  {
    this.getCortesById(111760852) 
  }

  getCortesById(id: number)
  {
      this.cortesService.getCorteById(id).subscribe( (res:any) => {
        this.allCortes = res.info
        for (let i = 0; i < res.info.length; i++) 
        {
          this.suma += res.info[i].totalefectivo << 0
        }
      })
  }
  

}
