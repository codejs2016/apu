import { Component, OnInit } from '@angular/core';
import { CortesService } from 'src/app/services/cortes.service';

@Component({
  selector: 'app-controlcaja',
  templateUrl: './controlcaja.component.html',
  styleUrls: ['./controlcaja.component.css']
})
export class ControlcajaComponent implements OnInit {

  public allCortes : any 
  public suma = 0
  public sumachance = 0
  public sumagiro = 0
  public countEfectivo = 0
  public countGiros = 0
  public countPagos = 10
  public valorPagos = 100000
  public relaciones : number

  constructor(private cortesService: CortesService) { }

  ngOnInit() {
    this.getCortesById(1)
  }

  getCortesById(id: number)
  {
      this.cortesService.getCorteById(id).subscribe( (res:any) => {
        this.allCortes = res
        for (let i = 0; i < res.length; i++) 
        {
          if(res[i].relacion === "chance"){
            this.sumachance += res[i].totalefectivo << 0
            this.countEfectivo = res[i].relacion.length

          }
          
          if(res[i].relacion === "giros"){
            this.sumagiro += res[i].totalefectivo << 0
          }
          
        }
      })
  }

}
