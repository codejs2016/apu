import { Component, OnInit } from '@angular/core';
import { ConfigService } from 'src/app/services/config-service';
import { LoginService } from 'src/app/services/login.service';
import $ from "jquery";

@Component({
  selector: 'app-asesora',
  templateUrl: './asesora.component.html',
  styleUrls: ['./asesora.component.css']
})
export class AsesoraComponent implements OnInit {

  public listModules: any

  constructor(private config: ConfigService,
              private loginService: LoginService)
  {}
  
  ngOnInit(){
    this.getListModules()
    
  }
  
  getListModules()
  {
    this.listModules = this.config.getConfig().modulos
  }
  
}
