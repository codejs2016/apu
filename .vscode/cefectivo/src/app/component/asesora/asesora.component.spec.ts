import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AsesoraComponent } from './asesora.component';

describe('AsesoraComponent', () => {
  let component: AsesoraComponent;
  let fixture: ComponentFixture<AsesoraComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AsesoraComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AsesoraComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
