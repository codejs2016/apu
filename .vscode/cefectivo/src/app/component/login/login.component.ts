import { Component, OnInit } from '@angular/core';
import { LoginService } from 'src/app/services/login.service';
import { Router } from "@angular/router";
import swal from 'sweetalert2';
import { ConfigService } from 'src/app/services/config-service';
import { LoginGuard } from 'src/app/guards/login.guard';
import $ from "jquery";


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  public dateNow = new Date();
  
  constructor(private loginService: LoginService, 
              private loginGuard: LoginGuard,
              private router: Router,
              private config: ConfigService) { }

  ngOnInit() {
    sessionStorage.removeItem('name')
    sessionStorage.removeItem('iduser')
    sessionStorage.removeItem('token')
  }

  loginUser(event:any)
  {
   event.preventDefault()
   const target = event.target  
   const username =  target.querySelector('#username').value
   const password =  target.querySelector('#password').value

   if(username && password)
   {
      const data =
      {
        username :username.toUpperCase(),
        password: password
      } 
      this.loginService.loginUser(data).subscribe( (res: any) => {
        
        console.log( res )
        if(res.status == true)
        {
          if(res.data.tokensession != null){
             switch(res.data.role) {
                case this.config.getConfig().typerol.asesora:
                  /*let usercv = {
                    token:res.data.tokensession,
                    loginuser:res.data.loginuser,
                    name:res.data.name,
                    document:res.data.document,
                    username:res.data.username,
                    territorio:res.data.territorio,
                    role:res.data.role
                }*/
                  sessionStorage.removeItem('token');
                  sessionStorage.setItem('token',res.data.tokensession);
                  this.redirect('asesora');
                break;

                case this.config.getConfig().typerol.jefeseguridad:
                  sessionStorage.removeItem('token');
                  sessionStorage.setItem('token',res.data.tokensession);
                  this.redirect('admin');
                break;

                case this.config.getConfig().typerol.jefetesoreria:
                  sessionStorage.removeItem('token');
                  sessionStorage.setItem('token',res.data.tokensession);
                  this.redirect('tesoreria');
                break;

              default:
                break;
            }
          }else{
            swal.fire("Inicio de sesión ",res.mgs, "warning")
          }
        }else{
          swal.fire("Inicio de sesión ","El usuario ingresado no existe!", "warning")
        }
      },
      error => {
        console.error("Jola",error);
      })

    }else{
      swal.fire("Aviso ","Por favor ingresar usuario y contraseña ", "warning")
    }
  }

  redirect(role:string){
    switch (role) {
        case 'asesora':
          this.router.navigate(['asesora'])
        break;

        case 'cefectivo':
          this.router.navigate(['cefectivo'])
        break;

        case 'guarda':
          this.router.navigate(['guarda'])
        break;

        case 'jefeseguridad':
          this.router.navigate(['aseguridadsesora'])
        break;
    
      default:
        break;
    }
  }

  loader() 
  {
		setTimeout(function() { 
			if($('#idloader').length > 0) {
				$('#idloader').removeClass('show');
			}
		}, 600);
	};
	
 
}
