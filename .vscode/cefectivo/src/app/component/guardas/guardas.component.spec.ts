import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GuardasComponent } from './guardas.component';

describe('GuardasComponent', () => {
  let component: GuardasComponent;
  let fixture: ComponentFixture<GuardasComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GuardasComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GuardasComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
