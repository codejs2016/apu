import { Component, OnInit } from '@angular/core';
import { GuardasService } from 'src/app/services/guardas.service';
import { FormGroup, AbstractControl, FormBuilder, Validators } from '@angular/forms';
import swal from 'sweetalert2';

@Component({
  selector: 'app-guardas',
  templateUrl: './guardas.component.html',
  styleUrls: ['./guardas.component.css']
})
export class GuardasComponent implements OnInit {

  public guardas: any
  public vehiculos: any
  public lottieConfig: Object
  public spinner = false
  public submitted = false

  public formRegister: FormGroup
  public id: AbstractControl
  public idplaca: AbstractControl
  public idruta: AbstractControl

  constructor(private guardasService: GuardasService,
             private formBuilder: FormBuilder) {
   
    this.formRegister = this.formBuilder.group({
      id: ['', [ Validators.required ] ],
      idplaca: ['', [Validators.required ] ],
      idruta: ['', [Validators.required ] ]
    })

    this.id = this.formRegister.controls['id']
    this.idplaca = this.formRegister.controls['idplaca']
    this.idruta = this.formRegister.controls['idruta']
  }

  ngOnInit() {
    //this.getAllGuardas()
    let data = []
        let one = {
          documento:123,
          nombres:'Javier',
          apellido1:'Renteria',
          apellido2:'Hinojosa'
        }
        let two = {
          documento:1234,
          nombres:'Camilo',
          apellido1:'Mendoza',
          apellido2:'Del Castillo'
        }

        let three = {
          documento:12345,
          nombres:'Hector Mauricio',
          apellido1:'Hinojosa',
          apellido2:'Ibarguen'
        }
        data.push(one)
        data.push(two)
        data.push(three)
        this.guardas = data
  }
 
  get f() {
    return this.formRegister.controls;
  }

  getAllGuardas(){
    this.guardasService.get().subscribe( (res:any) =>{
      this.guardas = res.data
    })
    
  }

  getAllVehiculos(){
    /*this.guardasService.getVehiculo().subscribe( (res:any) =>{
      this.vehiculos = res.data
    })*/

    let data = []
        let one = {
          placa:'JGR345'
        }
        let two = {
          placa:'TGR555'
        }

        let three = {
          placa:'FTR095'
        }
        data.push(one)
        data.push(two)
        data.push(three) 
        this.vehiculos = data
  }

  save(){
    this.submitted = true
    if(this.formRegister.invalid)
     return false
    
   
    this.spinner = true
    this.guardasService.saveProgramacion(this.formRegister.value).subscribe( (res:any)=>{
      if(res.status == true){
        swal.fire("Programación de Ruta", res.msg, "success")
        this.spinner = false
      }
      else{
        swal.fire("Programación de Ruta", res.msg, "warning")
        this.spinner = false
      }
    })
  }

  clear(){
    this.formRegister.reset()
  }
}
