import { Component, OnInit } from '@angular/core';
import { ConfigService } from 'src/app/services/config-service';

@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.css']
})
export class AdminComponent implements OnInit {

  public listModules: any
  constructor(private config: ConfigService) { }

  ngOnInit() {
    this.getListModules()
  }

  getListModules()
  {
    this.listModules = this.config.getConfig().modulosmonitoreo
  }
  
}
