import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, AbstractControl, Validators } from '@angular/forms';
import { WebsocketService } from 'src/app/services/websocket.service';
import { AlertController } from '@ionic/angular';
import { Socket } from 'ngx-socket-io';
import swal from 'sweetalert2';
import { ConfigService } from 'src/app/services/config-service';
import { CortesService } from 'src/app/services/cortes.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  public isinvalid : any
  public typecienmil : any
  public typecincuentamil : any
  public typeveintemil : any
  public typediezmil : any
  public typecincomil: any
  public typedosmil : any
  public typemil : any
  public typeServices: any
  public submitted = false
  public statusSum = false
  public showNoveda = false
  public billetes: any

  public suma = 0
  public sumachance = 0
  public sumagiro = 0
  public sumapagos = 0

  public idUserApp = 111760852;


  public formCefectivo: FormGroup
  public tolamonedas: AbstractControl 
  public ciemil : AbstractControl 
  public cincuentamil : AbstractControl 
  public veintemil : AbstractControl 
  public diezmil : AbstractControl 
  public cincomil : AbstractControl 
  public dosmil : AbstractControl 
  public mil : AbstractControl 
  public totalefectivo: AbstractControl 
  public relacion: AbstractControl
  public observacion: AbstractControl
  public iduser: AbstractControl
  public obsnovedad: AbstractControl

  constructor(
    private config: ConfigService,
    private formBuilder: FormBuilder,
    private websocketService: WebsocketService,
    private cortesService: CortesService,
    public alertController: AlertController,
    private socket: Socket) {

    this.formCefectivo = this.formBuilder.group({
      id: [''],
      tolamonedas: ['', Validators.compose([Validators.required, Validators.min(1), Validators.maxLength(32767)]) ],
      ciemil: ['', Validators.compose([Validators.required, Validators.min(1), Validators.maxLength(32767)]) ],
      cincuentamil: ['', Validators.compose([Validators.required, Validators.min(1), Validators.maxLength(32767)]) ],
      veintemil: ['', Validators.compose([Validators.required, Validators.min(1), Validators.maxLength(32767)]) ],
      diezmil: ['', Validators.compose([Validators.required, Validators.min(1), Validators.maxLength(32767)]) ],
      cincomil: ['', Validators.compose([Validators.required, Validators.min(1), Validators.maxLength(32767)]) ],
      dosmil: ['', Validators.compose([Validators.required, Validators.min(1), Validators.maxLength(32767)]) ],
      mil: ['', Validators.compose([Validators.required, Validators.min(1), Validators.maxLength(32767)]) ],
      totalefectivo: ['', Validators.compose([Validators.required, Validators.min(1), Validators.maxLength(32767)]) ],
      relacion: ['', Validators.compose([Validators.required, Validators.min(1), Validators.maxLength(32767)]) ],
      observacion: ['', Validators.compose([Validators.required, Validators.min(1), Validators.maxLength(32767)]) ],
      iduser: ['', Validators.compose([Validators.required, Validators.min(1), Validators.maxLength(32767)]) ],
      obsnovedad:['']
    })

    this.tolamonedas = this.formCefectivo.controls['tolamonedas']
    this.ciemil = this.formCefectivo.controls['ciemil']
    this.cincuentamil = this.formCefectivo.controls['cincuentamil']
    this.veintemil = this.formCefectivo.controls['veintemil']
    this.diezmil = this.formCefectivo.controls['diezmil']
    this.cincomil = this.formCefectivo.controls['cincomil']
    this.dosmil = this.formCefectivo.controls['dosmil']
    this.mil = this.formCefectivo.controls['mil']
    this.totalefectivo = this.formCefectivo.controls['totalefectivo']
    this.relacion = this.formCefectivo.controls['relacion']
    this.observacion = this.formCefectivo.controls['observacion']
    this.iduser = this.formCefectivo.controls['iduser']
    this.obsnovedad = this.formCefectivo.controls['obsnovedad']

    this.onSuccessRegister()
  }

  ngOnInit() 
  {
    //this.getCortesById(1)
    this.typecienmil =  this.config.getConfig().typenumbers.ciemil
    this.typecincuentamil = this.config.getConfig().typenumbers.cincuentamil
    this.typeveintemil = this.config.getConfig().typenumbers.veintemil
    this.typediezmil = this.config.getConfig().typenumbers.diezmil
    this.typecincomil= this.config.getConfig().typenumbers.cincomil
    this.typedosmil = this.config.getConfig().typenumbers.dosmil
    this.typemil = this.config.getConfig().typenumbers.mil
    this.typeServices = this.config.getConfig().typservices
    this.init()
  }

  init(){
    this.billetes = this.config.getConfig().billetes
  }

  get f() {
    return this.formCefectivo.controls;
  }

  getCortesById(id: number)
  {
      this.cortesService.getCorteById(id).subscribe( (res:any) => {
        for (let i = 0; i < res.length; i++) 
        {
          if(res[i].relacion === "chance"){
            this.sumachance += res[i].totalefectivo << 0
          }
          
          if(res[i].relacion === "giros"){
            this.sumagiro += res[i].totalefectivo << 0
          }
          
        }
      })
  }

  save()
  {
    const swalWithBootstrapButtons = swal.mixin({
      customClass: {
        confirmButton: 'btn btn-success',
        cancelButton: 'btn btn-danger'
      },
      buttonsStyling: false
    })
      
    swalWithBootstrapButtons.fire({
      title: 'Esta seguro de registrar efectivo ?',
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Si',
      cancelButtonText: 'No',
      reverseButtons: true
    }).then((result) => {
    if (result.value)
    {
      this.submitted = true;
      this.formCefectivo.get('iduser').setValue(this.idUserApp)
      this.formCefectivo.get('totalefectivo').setValue(this.suma)
      if(this.formCefectivo.invalid)
        return false;

      this.formCefectivo.value.totalefectivo = this.suma
      this.websocketService.sendEfectivo( this.formCefectivo.value )
      this.resetForm( this.formCefectivo )
      this.suma = 0
      this.submitted = false
     }
    })
  }

  resetForm(form: any)
  {
    if (form) {
      form.reset();
    }
  }

  clear(){
    this.resetForm( this.formCefectivo )
    this.suma = 0
  }
  

  onSuccessRegister() 
  {
    this.socket.on('efectivo', (data: any) => {
       if(data.idUser == this.idUserApp){
         this.sumachance = parseInt(data.dataTransport[1].chance)
         this.sumagiro = parseInt(data.dataTransport[0].giros)
         this.sumapagos = parseInt(data.dataTransport[0].pagos)
       }
    })
  }

  validateNumber(event: any, type: number)
  { 
    event.target.value = (event.target.value + '').replace(/[^0-9]/g, '')
    if(parseInt(event.target.value) > 0)
    {
      
      let suma = 0
      if(this.f.ciemil.value.length > 0)
        suma += parseInt(this.f.ciemil.value) * 100000
     
      if(this.f.diezmil.value.length > 0)
        suma += parseInt(this.f.diezmil.value) * 10000
      
      if(this.f.cincuentamil.value.length > 0)
        suma += parseInt(this.f.cincuentamil.value) * 50000
      
      if(this.f.cincomil.value.length > 0)
        suma += parseInt(this.f.cincomil.value) * 5000
      
      if(this.f.veintemil.value.length > 0)
        suma += parseInt(this.f.veintemil.value) * 20000
      
      if(this.f.dosmil.value.length > 0)
        suma += parseInt(this.f.dosmil.value) * 2000
      
      if(this.f.mil.value.length > 0)
        suma += parseInt(this.f.mil.value) * 1000
      
      if(this.f.tolamonedas.value.length > 0)
        suma += parseInt(this.f.tolamonedas.value) 
      
      this.suma = suma
    }
  }

  calcularEfectivo(cantidad: number, typeIn: number)
  {
    if(typeIn == 0){
      this.suma += this.suma + parseInt(this.formCefectivo.controls.tolamonedas.value)
     }
     else{
      for(let i = 0; i < this.billetes.length; i++) {
        if(this.billetes[i].cienmil == typeIn){
         this.billetes[0].total = ( cantidad * this.billetes[i].cienmil )
        }
         
        if(this.billetes[i].cincuentamil == typeIn){
         this.billetes[1].total = ( cantidad * this.billetes[i].cincuentamil )
        }

        if(this.billetes[i].veintemil == typeIn){ 
          this.billetes[2].total = ( cantidad * this.billetes[i].veintemil )
        }

        if(this.billetes[i].diezmil == typeIn){
          this.billetes[3].total = ( cantidad * this.billetes[i].diezmil )
        }
        
        if(this.billetes[i].cincomil == typeIn){
          this.billetes[4].total = ( cantidad * this.billetes[i].cincomil )
        }
       
        if(this.billetes[i].dosmil == typeIn){
          this.billetes[5].total = ( cantidad * this.billetes[i].dosmil )
        }
        
        if(this.billetes[i].mil == typeIn){
          this.billetes[6].total = ( cantidad * this.billetes[i].mil )
        }
        this.suma += this.billetes[i].total << 0
      }
     }
  }
 

  getOb(obs:any){
    switch (obs) {
       case "con novedad":
          this.showNoveda = true
       break;
       case "sin novedad":
          this.showNoveda = false
       break;
     default:
       break;
   }
  }

}
