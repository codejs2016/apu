import { Component, OnInit } from '@angular/core';
import swal from 'sweetalert2';
import { PagoService } from 'src/app/services/pago.service';
import { FormGroup, AbstractControl, Validators, FormBuilder } from '@angular/forms';

@Component({
  selector: 'app-pagos',
  templateUrl: './pagos.component.html',
  styleUrls: ['./pagos.component.css']
})
export class PagosComponent implements OnInit {

  public pagos: any
  public formPagos: FormGroup
  public serie: AbstractControl 
  public numero : AbstractControl 
  public submitted = false
  public showTable = false
  public showLoader = false
  public showpay = false
  public valor = null
  public showgrid = true
  public showform = false
  public listPagos: any
  
  constructor(private pagoService: PagoService,   
    private formBuilder: FormBuilder)
    { 
    this.formPagos = this.formBuilder.group({
      serie: ['', Validators.required],
      numero: ['', Validators.required],
    })

    this.serie = this.formPagos.controls['serie']
    this.numero = this.formPagos.controls['numero']
  }

  ngOnInit() {
    
  }

  get f() {
    return this.formPagos.controls;
  }

  show(){
    this.showLoader = true
  }

  hide(){
    this.showLoader = false
  }

  add(){
    this.pagos.cedula =  1111;
    this.pagoService.save(this.pagos).subscribe( (res:any)=>{
      if(res.status == true){
        this.showTable = true
        swal.fire("Aviso ",res.msg, "success")
      }
      else
       swal.fire("Aviso ",res.msg, "warning")
    }) 
  }

  validatePago(){
    this.submitted = true
    if(this.formPagos.invalid)
      return false
    
    this.show()
    this.pagoService.searchs(this.formPagos.value).subscribe( (res:any)=>{
       if(res.status == true){
         if(res.data.estadopremio == 1 || res.data.estadopremio == 62){
           this.pagos = res.data
           this.valor = res.data.totalpremio
           this.showpay = true
          }
          else
           swal.fire("Aviso ","No es premio valido", "warning")
       }
       this.hide()
    }) 
  }

  validateNumber(event: any)
  { 
    event.target.value = (event.target.value + '').replace(/[^0-9]/g, '')
  }

  delete(num: any)
  {
    swal.fire({
      title: 'Está seguro?',
      text: 'Seguro que desea eliminar numero ?',
      type: 'warning',
      showCancelButton: true,
      cancelButtonText: 'Cancelar',
      confirmButtonText: 'Sí'
    }).then((result) => {
      if (result.value) {
        for (let i = 0; i < this.pagos.length; i++) 
        {
          if(this.pagos[i].numero == num){
            this.pagos.splice(i,1)
          } 
        }
      }
    })
  }


  showformData() {
    this.showform = !this.showform
    this.showgrid = !this.showgrid
  }
}
