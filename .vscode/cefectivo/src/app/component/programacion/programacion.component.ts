import { Component, OnInit } from '@angular/core';
import { FormGroup, AbstractControl, Validators, FormBuilder } from '@angular/forms';
import { GuardasService } from 'src/app/services/guardas.service';
import { NgbDate, NgbCalendar, NgbDateParserFormatter, NgbModal, ModalDismissReasons, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import swal from 'sweetalert2';


@Component({
  selector: 'app-programacion',
  templateUrl: './programacion.component.html',
  styleUrls: ['./programacion.component.css']
})
export class ProgramacionComponent implements OnInit {

  public guardas = [];
  public lisguardas = [];
  public vehiculos: any
  public dataProgramacion = []
  public lottieConfig: Object
  public spinner = false
  public submitted = false
  public closeResult: string
  private modalRef: NgbModalRef

  public formRegister: FormGroup
  public numeroprg: AbstractControl
  public finicial: AbstractControl
  public ffinal: AbstractControl

  constructor(private guardasService: GuardasService,
    private formBuilder: FormBuilder,
    private calendar: NgbCalendar,
    public formatter: NgbDateParserFormatter,
    private modalService: NgbModal) {

    this.formRegister = this.formBuilder.group({
      numeroprg: ['', [Validators.required]],
      finicial: ['', [Validators.required]],
      ffinal: ['', [Validators.required]]
    })

    this.numeroprg = this.formRegister.controls['numeroprg']
    this.finicial = this.formRegister.controls['finicial']
    this.ffinal = this.formRegister.controls['ffinal']
  }


  ngOnInit() {
   this.getGuardas()
  }

  getGuardas(){
    this.lisguardas.push({
      id:1,
      name :'Jhon Javier Renteria'
    });
    this.lisguardas.push({
      id:2,
      name :'Esteven Giraldo'
    });
    this.lisguardas.push({
      id:3,
      name :'Jhon Javier Renteria'
    });
  }
  

  get f() {
    return this.formRegister.controls;
  }

  save() {
    this.submitted = true
    if(this.formRegister.invalid)
     return
    
    this.dataProgramacion.push(this.formRegister.value)
    swal.fire("Programación ","Registrada satisfactoriamente", "success")
  }

  asociar(data:any){
     this.guardas.push(data);
     console.log( data.id );
     this.delete(data.id)
  }

  delete(id:number){
    for (let i = 0; i < this.lisguardas.length; i++) {
      if(this.lisguardas[i].id == id){
        this.lisguardas.splice(i,1)
      }
    }
  }

  sendata(){
    swal.fire({
      title: 'Registro',
      text: 'Está seguro de registrar programación',
      type: 'warning',
      showCancelButton: true,
      cancelButtonText: 'Cancelar',
      confirmButtonText: 'Sí'
    }).then((result) => {
        if(result.value){
           this.onCloseModal()
        }
    })
  }

  onCloseModal() 
  {
    this.modalRef.close();
  }
  
  open(content:any, item?:any) {
    this.modalService.open(content, {ariaLabelledBy: 'modal-basic-title'}).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }

  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return  `with: ${reason}`;
    }
  }

  clear() {

  }

  validateNumber(event: any) {
    event.target.value = (event.target.value + '').replace(/[^0-9]/g, '')
  }


  validateInput(currentValue: NgbDate, input: string): NgbDate {
    const parsed = this.formatter.parse(input);
    return parsed && this.calendar.isValid(NgbDate.from(parsed)) ? NgbDate.from(parsed) : currentValue;
  }

}
