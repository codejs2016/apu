import { Injectable, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ConfigService } from './config-service';


@Injectable({
  providedIn: 'root'
})
export class SedesService {

readonly URL_API = this.config.getConfig().bussinesServer.url

  constructor(
    private http: HttpClient, 
    private config:ConfigService)
  {

  }
  
  saveSedes(data: any)
  {
    return this.http.post(this.URL_API+"/api/sedes", data)
  }

  updateSedes(data: any)
  {
    return this.http.post(this.URL_API+"/api/sedes/save", data)
  }

  getSedes(data: any)
  {
    return this.http.post(this.URL_API+"/api/sedes/get", data)
  }
}
