import { Injectable } from '@angular/core';
import { HttpClient } from "@angular/common/http";
import { ConfigService } from "./config-service";

@Injectable({
  providedIn: 'root'
})
export class VehiclesService {

  readonly URL_API = this.config.getConfig().bussinesServer.url;

  constructor(private http: HttpClient, private config: ConfigService) {}
  
  getVehicles() {
    return this.http.get(this.URL_API + "/api/vehicle");
  }

  getVehiclesPlates() {
    return this.http.get(this.URL_API + "/api/vehicle/plates");
  }

  getVehicle(_id: string) {
    return this.http.get(this.URL_API + "/api/vehicle" + `/${_id}`);
  }

  searchs(name: any) {
    return this.http.post(this.URL_API + "/api/vehicle/search", name);
  }

  register(data: any) {
    return this.http.post(this.URL_API + "/api/vehicle/create", data);
  }

  put(data: any) {
    return this.http.put(this.URL_API + `/api/vehicle/${data._id}`, data);
  }

  delete(_id: string) {
    return this.http.delete(this.URL_API + "/api/vehicle" + `/${_id}`);
  }

}
