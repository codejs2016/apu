import { Injectable, OnInit } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { ConfigService } from "./config-service";

@Injectable({
  providedIn: "root"
})
export class TravelService implements OnInit {

  readonly URL_API = this.config.getConfig().bussinesServer.url;

  constructor(private http: HttpClient, private config: ConfigService) {}
  
  ngOnInit() {}

  getTravels() 
  {
    return this.http.get(this.URL_API + "/api/travel")
  }

  getTravelByCompany(_id: string) 
  {
    return this.http.get(this.URL_API + "/api/travel" + `/${_id}`)
  }

  registerTravel(travel: any)
  {
    return this.http.post(this.URL_API + "/api/travel/create", travel)
  }

  putTravel(travel: any) 
  {
    return this.http.put(this.URL_API + `/api/travel/${travel._id}`, travel)
  }

  deleteTravel(_id: string) 
  {
    return this.http.delete(this.URL_API + "/api/travel" + `/${_id}`)
  }


  searchs(travel: any)
  {
    return this.http.post(this.URL_API + "/api/travel/search", travel)
  }
}
