import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ConfigService } from './config-service';

@Injectable({
  providedIn: 'root'
})
export class BranchofficeService {

  readonly URL_API = this.config.getConfig().bussinesServer.url

  constructor(
    private http: HttpClient, 
    private config:ConfigService) { }

  getBranchoffice(branch:any)
  {
    return this.http.post(this.URL_API+"/api/branchoffice", branch)
  }

  getBranch(id:String)
  {
    return this.http.get(this.URL_API+"/api/branchoffice/"+`${id}`)
  }

  getBranchoffices(id:any)
  {
    return this.http.get(this.URL_API+"/api/branchoffice/company/"+`${id}`)
  }

  putPoint(branch: any){
    return this.http.put(this.URL_API + `/api/branchoffice/${branch._id}`, branch)
  }

  deletePoint(_id: string){
    return this.http.delete(this.URL_API + "/api/branchoffice"+ `/${_id}`)
  }

  getBranchofficeBycity(city: any)
  {
    return this.http.post(this.URL_API+"/api/branchoffice/", city)
  }

  registerBranchoffice(data: any)
  {
    return this.http.post(this.URL_API+"/api/branchoffice/", data)
  }
}
