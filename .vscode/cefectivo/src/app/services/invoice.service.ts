import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ConfigService } from './config-service';

@Injectable({
  providedIn: 'root'
})
export class InvoiceService {


  readonly URL_API = this.config.getConfig().bussinesServer.url

  constructor(private http: HttpClient, private config:ConfigService) { }


  getinvoices()
  {
    return this.http.get(this.URL_API+"/api/invoice")
  }

  getinvoice(_id: string){
    return this.http.get(this.URL_API + "/api/invoice"+ `/${_id}`)
  }

  createinvoice(data: any)
  {
    return this.http.post(this.URL_API+"/api/invoice", data)
  }

}
