import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ConfigService } from './config-service';

@Injectable({
  providedIn: 'root'
})
export class SalesService {

  readonly URL_API = this.config.getConfig().bussinesServer.url

  constructor(private http: HttpClient, private config:ConfigService)
  {
  }
  ngOnInit()
  {
  }

  getSales()
  {
    return this.http.get(this.URL_API+"/api/sales")
  }

  getSalesByCompany(_id: string){
    return this.http.get(this.URL_API + "/api/sales/company"+ `/${_id}`)
  }
}
