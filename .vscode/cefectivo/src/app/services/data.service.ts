import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class DataService {

  public storage: any;
  public cart: any;
  public products: any;
  public company: any;
  public tw: boolean;
  public cntsedes: any
  public disable = null
  public placa : any

  constructor() { }
}
