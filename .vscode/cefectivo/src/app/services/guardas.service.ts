import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ConfigService } from './config-service';

@Injectable({
  providedIn: 'root'
})
export class GuardasService {

  
  readonly URL_API = this.config.getConfig().bussinesServer.url
  constructor(
    private http: HttpClient, 
    private config:ConfigService)
  {}
   
  get()
  {
    return this.http.get(this.URL_API+"/api/guardas")
  }

  getVehiculo()
  {
    return this.http.get(this.URL_API+"/api/guardas/vehiculos")
  }

  saveProgramacion(data: any)
  {
    return this.http.post(this.URL_API+"/api/guardas/saveprogramacion",data)
  }
}


