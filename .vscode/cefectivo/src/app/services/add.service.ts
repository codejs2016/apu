import { Injectable, OnInit } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { ConfigService } from "./config-service";

@Injectable({
  providedIn: "root"
})
export class AddService implements OnInit {
  readonly URL_API = this.config.getConfig().bussinesServer.url;

  constructor(private http: HttpClient, private config: ConfigService) {}
  ngOnInit() {}

  getAdds() {
    return this.http.get(this.URL_API + "/api/adds");
  }

  getProduct(_id: string) {
    return this.http.get(this.URL_API + "/api/adds" + `/${_id}`);
  }

  getAddsCompany(_id: string) {
    return this.http.get(this.URL_API + "/api/adds/company" + `/${_id}`);
  }

  getAddsCategory(add: any) {
    return this.http.get(this.URL_API + "/api/adds/category" + `/${add._id}`, add);
  }

  searchsAdds(nameAdd: any) {
    return this.http.post(this.URL_API + "/api/adds/search", nameAdd);
  }

  registerAdds(add: any) {
    return this.http.post(this.URL_API + "/api/adds", add);
  }

  putAdd(add: any) {
    return this.http.put(this.URL_API + `/api/adds/${add._id}`, add);
  }

  deleteAdd(_id: string) {
    return this.http.delete(this.URL_API + "/api/adds" + `/${_id}`);
  }
}
