import { Injectable, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ConfigService } from './config-service';


@Injectable({
  providedIn: 'root'
})
export class PromocionService {

readonly URL_API = this.config.getConfig().bussinesServer.url

  constructor(
    private http: HttpClient, 
    private config:ConfigService)
  {

  }
  
  getPromociones()
  {
    return this.http.get(this.URL_API+"/api/promocion")
  }

  getPromocionesSinImage()
  {
    return this.http.get(this.URL_API+"/api/promocion/sin")
  }

  save(data: any)
  {
    return this.http.post(this.URL_API+"/api/promocion", data)
  }

  update(data: any)
  {
    return this.http.put(this.URL_API+'/api/promocion' + `/${data.id}`, data)
  }
  
  delete(_id: string )
  {
    return this.http.delete(this.URL_API+"/api/promocion/" + `/${_id}`)
  }
}
