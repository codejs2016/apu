import { Injectable } from '@angular/core';
import { Socket } from 'ngx-socket-io';

@Injectable({
  providedIn: 'root'
})
export class WebsocketService{
  
  constructor(private socket: Socket)
  {

  }

  sendEfectivo(data: any)
  {
    this.socket.emit('efectivo', data)
  }

  getCortes(data: any)
  {
    this.socket.emit('getefectivocortes', data)
  }


}
