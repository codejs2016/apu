import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ConfigService } from './config-service';
import { Socket } from 'ngx-socket-io';

@Injectable({
  providedIn: 'root'
})
export class OrderService {

  
  readonly URL_API = this.config.getConfig().bussinesServer.url

  constructor(private Sc: Socket, private http: HttpClient, private config:ConfigService) { 

  }

  newOrder(order) {
    this.Sc.emit('order', order);
  }

  ResOrderSock(){
    this.Sc.on('order', function(data) {
      return data;
    })
  }

  getOrders()
  {
    return this.http.get(this.URL_API+"/api/order")
  }

  getOrder(_id: string){
    return this.http.get(this.URL_API + "/api/order"+ `/${_id}`)
  }
  
  getOrdersByUser(id: string){
    return this.http.get(this.URL_API + "/api/order"+ `/${id}`)
  }

  getOrdersByBrach(id: any){
    return this.http.get(this.URL_API + "/api/order/branch"+ `/${id}`)
  }

  getProductsOrders(id: any){
    return this.http.get(this.URL_API + "/api/order/branch/product"+ `/${id}`)
  }

  createOrder(order: any)
  {
    return this.http.post(this.URL_API+"/api/order", order)
  }

  successOrder(order: any)
  {
    return this.http.put(this.URL_API + "/api/order"+ `/${order._id}`, order)
  }

}
