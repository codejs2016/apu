import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ConfigService } from './config-service';

@Injectable({
  providedIn: 'root'
})
export class FactReferenceService {
  
  readonly URL_API = this.config.getConfig().bussinesServer.url

  constructor(private http: HttpClient, private config:ConfigService) { }


  getfactrefs()
  {
    return this.http.get(this.URL_API+"/api/factureref")
  }

  getfactref(id: string){
    return this.http.get(this.URL_API + "/api/factureref"+ `/${id}`)
  }

  createfactref(factref: any)
  {
    return this.http.post(this.URL_API+"/api/factureref", factref)
  }

}
