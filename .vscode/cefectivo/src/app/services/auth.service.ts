import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  public isLoggedIn: Boolean
  public user: any

  constructor() {

  }

  isAuthenticated() {
    return this.isLoggedIn
  }
}
