import { Injectable, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ConfigService } from './config-service';


@Injectable({
  providedIn: 'root'
})
export class GenderService{

  readonly URL_API = this.config.getConfig().bussinesServer.url
  constructor(
    private http: HttpClient, 
    private config:ConfigService)
  {}
   
  listGender()
  {
    return this.http.get(this.URL_API+"/api/gender")
  }

  listTypeDocuments()
  {
    return this.http.get(this.URL_API+"/api/documentypes")
  }

}
