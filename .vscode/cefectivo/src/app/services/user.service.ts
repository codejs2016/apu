import { Injectable, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ConfigService } from './config-service';


@Injectable({
  providedIn: 'root'
})
export class UserService implements OnInit {
  
  readonly URL_API = this.config.getConfig().bussinesServer.url

  constructor(private http: HttpClient, private config:ConfigService) {
  }

  ngOnInit(){}

  getUsers()
  {
    return this.http.get(this.URL_API)
  }

  getUsersConductors()
  {
    return this.http.get(this.URL_API+'/api/driver')
  }

  getUserByEmail(user:any)
  {
    return this.http.get(this.URL_API+'/api/user/email' + `/${user}`)
  }

  getUserByUsername(user:any)
  {
    return this.http.get(this.URL_API+'/api/user/username' + `/${user}`)
  }

  postUser(user:any)
  {
    return this.http.post(this.URL_API+'/api/user/create', user)
  }

  putUser(element: any)
  {
    return this.http.put(this.URL_API+'/api/user' + `/${element.id}`, element.user)
  }

  putUserConductor(data: any)
  {
    return this.http.put(this.URL_API+'/api/user/conductors' + `/${data.id}`, data)
  }

  deleteUser(_id: string)
  {
    return this.http.delete(this.URL_API + `/${_id}`)
  }
}
