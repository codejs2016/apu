export const configuration = {
   bussinesServer:{
    url:'http://localhost:3000'
    //url:'http://192.168.41.8:6000'
   },
   initComponent:{
    login:'Entrar',
    register:'Registrar',
    cefectivo:'Control de Efectivo',
    },
    loginComponent:{
     titlte:'Control de Efectivo',
     placeholderUser:'Usuario',
     placeholderPassword:'Contraseña',
     textLogin:'Iniciar sesión' 
    },
    resetComponent:{
      titlte:'Trucker',
      placeholderUser:'Usuario',
      textreset:'Recuperar contraseña' 
     },
     todo:{
      name:'Todo'
     },
    registerComponent:{
      placeholderDocument:'Número de documento',
      placeholderType:'Tipo de Documento',
      placeholderName:'Nombres',
      placeholderSurName:'Apellidos',
      placeholderGender:'Genero',
      placeholderAdress:'Dirección',
      placeholderPhone:'Celular',
      placeholderEmail:'Correo',
      placeholderUser:'Usuario',
      placeholderPassword:'Contraseña',
      placeholderRepeatPassword:'Repetir Contraseña',
      accept:'Aceptar',
      yes:'Si',
      no:'No'
    },
    typedocuments:{
      documents:[
        { id:'5ca57bb35e9dc70d6ac5a5c9', type: 'Cedula' },
        { id:'5ca57bb35e9dc70d6ac5a5ca', type: 'Cedula extranjera' },
        { id:'5ca57bb35e9dc70d6ac5a5cb', type: 'Pasaporte' }
      ]
    },
    modulos:[
      { name:'Control de efectivo', icon:'ion-ios-calculator', call:'register' },
      { name:'Historial de cortes', icon:'ion-ios-calendar', call:'cortes' },
      { name:'Pago de Premios', icon:'ion-md-wallet', call:'pagos' },
      { name:'Control de caja', icon:'ion-ios-stats', call:'caja' }
    ],
    modulosmonitoreo:[
      { name:'Programación', icon:'ion-ios-calendar', call:'programacion' },
      { name:'Guardas', icon:'ion-ios-person', call:'guardas' },
      { name:'Puntos de Ventas', icon:'ion-ios-apps', call:'ptvs' },
      { name:'Recogidas', icon:'ion-ios-basket', call:'recogidas' }
    ],
    typenumbers:
    {
      ciemil : 100000,
      cincuentamil : 50000,
      veintemil : 20000,
      diezmil : 10000,
      cincomil: 5000,
      dosmil : 2000,
      mil : 1000
    },
    billetes:
    [
      { cienmil : 100000 },
      { cincuentamil : 50000 },
      { veintemil : 20000 },
      { diezmil : 10000 },
      { cincomil: 5000 },
      { dosmil : 2000 },
      { mil : 1000 }
    ],
    typservices:[
      { id:1, name:'Chance' },
      { id:2, name:'Giros' }
    ],
    typegender:{
      female:'5c342fd24f5fbf23e32dc8e1',
      male:'5c342fd24f5fbf23e32dc8e2' 
    },
    typerol:{
      asesora:'ASESORA',
      auxtesoreria:'AUXILIAR DE TESORERIA',
      cajera:'CAJERO (A)',
      guarda:'GUARDA DE SEGURIDAD',
      jefeseguridad:'JEFE DE SEGURIDAD',
      jefetesoreria:'JEFE DE TESORERIA'
    }
  }