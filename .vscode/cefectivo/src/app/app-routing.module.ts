import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { RegisterComponent } from './component/register/register.component';
import { CortesComponent } from './component/cortes/cortes.component';
import { PagosComponent } from './component/pagos/pagos.component';
import { ControlcajaComponent } from './component/controlcaja/controlcaja.component';
import { LoginComponent } from './component/login/login.component';
import { AsesoraComponent } from './component/asesora/asesora.component';
import { NotfoundComponent } from './component/notfound/notfound.component';
import { LogoutComponent } from './component/logout/logout.component';
import { AdminComponent } from './component/admin/admin.component';
import { LoginGuard } from './guards/login.guard';
import { AdminGuard } from './guards/admin.guard';
import { GuardasComponent } from './component/guardas/guardas.component';
import { ProgramacionComponent } from './component/programacion/programacion.component';
import { PtvsComponent } from './component/ptvs/ptvs.component';
import { RecogidasComponent } from './component/recogidas/recogidas.component';

const routes: Routes = [
  { 
    path: 'asesora', component: AsesoraComponent, //canActivate: [ LoginGuard ],
    children: [
      { path: 'register', component: RegisterComponent, canActivateChild: [ LoginGuard ]},
      { path: 'cortes', component: CortesComponent, canActivateChild: [ LoginGuard ] },
      { path: 'pagos', component: PagosComponent, canActivateChild: [ LoginGuard ] },
      { path: 'caja', component: ControlcajaComponent, canActivateChild: [ LoginGuard ] }
    ]
  },
  { path: 'login', component: LoginComponent },
  { path: 'monitoreo', component: AdminComponent,
    children: [
    { path: 'programacion', component: ProgramacionComponent  },
    { path: 'guardas', component: GuardasComponent },
    { path: 'ptvs', component: PtvsComponent  },
    { path: 'recogidas', component: RecogidasComponent  }
  ]
  },
  {
    path: 'logout',
    component: LogoutComponent
  },
  {
    path: 'not-found',
    component: NotfoundComponent
  },
  {
    path: '**',
    redirectTo: 'login'
  }
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes),
    CommonModule
  ],
  exports:[
    RouterModule,
  ],
  declarations: []
})
export class AppRoutingModule { }
