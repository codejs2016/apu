import { BrowserModule } from '@angular/platform-browser';
import { NgModule, LOCALE_ID } from '@angular/core';
import es from '@angular/common/locales/es';
import { registerLocaleData } from '@angular/common';

registerLocaleData(es);

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

import { WebsocketService } from './services/websocket.service';
import { HttpClientModule } from '@angular/common/http';
import { SocketIoConfig, SocketIoModule } from 'ngx-socket-io';
import { configuration } from './configuration/configuration';
import { RegisterComponent } from './component/register/register.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CortesComponent } from './component/cortes/cortes.component';
import { PagosComponent } from './component/pagos/pagos.component';
import { ControlcajaComponent } from './component/controlcaja/controlcaja.component';
import { CortesService } from './services/cortes.service';
import { LoginComponent } from './component/login/login.component';
import { AsesoraComponent } from './component/asesora/asesora.component';
import { NotfoundComponent } from './component/notfound/notfound.component';
import { LogoutComponent } from './component/logout/logout.component';
import { AdminComponent } from './component/admin/admin.component';
import { LoginGuard } from './guards/login.guard';
import { AdminGuard } from './guards/admin.guard';
import { GuardasComponent } from './component/guardas/guardas.component';
import { LottieAnimationViewModule } from 'ng-lottie';
import { ProgramacionComponent } from './component/programacion/programacion.component';
import { PtvsComponent } from './component/ptvs/ptvs.component';
import { RecogidasComponent } from './component/recogidas/recogidas.component';
import { DpDatePickerModule } from 'ng2-date-picker';

import { ButtonsModule } from '@progress/kendo-angular-buttons';
import { GridModule } from '@progress/kendo-angular-grid';
import '@progress/kendo-angular-intl/locales/de/all';
import '@progress/kendo-angular-intl/locales/es/all';
import { IntlModule } from '@progress/kendo-angular-intl';
import { DateInputsModule } from '@progress/kendo-angular-dateinputs';

const configSocket: SocketIoConfig = { url: configuration.bussinesServer.url , options: {} };

@NgModule({
  declarations: [
    AppComponent,
    RegisterComponent,
    CortesComponent,
    PagosComponent,
    ControlcajaComponent,
    LoginComponent,
    AsesoraComponent,
    NotfoundComponent,
    LogoutComponent,
    AdminComponent,
    GuardasComponent,
    ProgramacionComponent,
    PtvsComponent,
    RecogidasComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    NgbModule,
    FormsModule,
    NgbModule,
    ReactiveFormsModule,
    DpDatePickerModule,
    GridModule,
    ButtonsModule,
    DateInputsModule,
    IntlModule,
    SocketIoModule.forRoot(configSocket),
    LottieAnimationViewModule.forRoot()
  ],
  providers: [
    WebsocketService,
    CortesService,
   // LoginGuard,
    //AdminGuard
    //{ provide: LOCALE_ID, useValue: 'es-ES' }
    //{ provide: LOCALE_ID, useValue: 'es' } 
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
