import { Component, OnInit } from '@angular/core';
import { AbstractControl, FormGroup, FormBuilder, Validators } from '@angular/forms';
import swal from 'sweetalert2';
import { GuardasService } from 'src/app/services/guardas.service';
import { VehiclesService } from 'src/app/services/vehicles.service';


@Component({
  selector: 'app-vehiculos',
  templateUrl: './vehiculos.component.html',
  styleUrls: ['./vehiculos.component.css']
})
export class VehiculosComponent implements OnInit {


  public form: FormGroup
  public placa: AbstractControl 
  public submitted = false
  public spinner = false
  public show = true
  public vehiculos: any
  public p: number = 1
  public text = 'Registrar'
  
  constructor(
    private formBuilder: FormBuilder,
    private guardasService: GuardasService,
    private vehiclesService: VehiclesService)
    {
    this.form = this.formBuilder.group({
      id:[],
      placa: ['', Validators.required ]
    })
    this.placa = this.form.controls['placa']
  }

  ngOnInit() {
    this.getVehicles()
  }


  get f() {
    return this.form.controls;
  }

  showSpinner(){
    this.spinner = true
  }

  hideSpinner(){
    this.spinner = false
  }

  getVehicles(){
    this.showSpinner()
    this.guardasService.getVehiculos().subscribe((res:any)=>{
      if(res.status){
       this.vehiculos = res.info
       this.hideSpinner()
      }
    }, 
    error =>{
       this.hideSpinner()
       swal.fire("Ocurrio un error al consultar guardas", "warning")
    })
  }

  save(){
    this.submitted = true
    if(this.form.invalid)
     return false

    if(this.form.value.id)
      this.update()
    else
      this.create()
  }


  create(){
     this.vehiclesService.register(this.form.value).subscribe( (res:any) =>{
       if(res.status){
        this.form.reset()
        this.submitted = false
        swal.fire("Registro","Vehículo registrado satisfactoriamente", "success")
        this.getVehicles()
       }
     }, 
     error =>{
      this.hideSpinner()
      swal.fire("Registro","Ocurrio un error al registrar vehículo", "error")
     })
  }

  update(){
    this.vehiclesService.update(this.form.value).subscribe( (res:any) =>{
      if(res.status){
       this.text = "Registrar"
       this.form.reset()
       this.submitted = false
       swal.fire("Registro","Vehículo actualizado satisfactoriamente", "success")
       this.getVehicles()
      }
    }, 
    error =>{
      this.hideSpinner()
       swal.fire("Registro","Ocurrio un error al actualizar vehículo", "error")
    })
  }

  edit(item: any){
     this.text = "Actualizar"
     this.form.get('id').setValue(item.idvehiculo)
     this.form.get('placa').setValue(item.placa)
  }

  delete(item:any){
    const swalWithBootstrapButtons = swal.mixin({
      customClass: {
        confirmButton: 'btn btn-success',
        cancelButton: 'btn btn-danger'
      },
      buttonsStyling: false
    })
    swalWithBootstrapButtons.fire({
      title: 'Está seguro de eliminar vehículo ?',
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Si',
      cancelButtonText: 'No',
      reverseButtons: true
    }).then((result) => {
    if (result.value)
    {
      this.vehiclesService.delete({ id: item.idvehiculo }).subscribe( (res:any)=>{
          if(res.status)
            swal.fire("Aviso","Vehículo eliminado satisfactoriamente", "success")
            this.getVehicles()
          },
          error => {
           swal.fire("Aviso","Ocurrio un error al elminar vehículo", "error")
         })
     }
   })
  }

}
