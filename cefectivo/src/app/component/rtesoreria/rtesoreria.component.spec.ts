import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RtesoreriaComponent } from './rtesoreria.component';

describe('RtesoreriaComponent', () => {
  let component: RtesoreriaComponent;
  let fixture: ComponentFixture<RtesoreriaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RtesoreriaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RtesoreriaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
