import { Component, OnInit } from '@angular/core';
import { FormGroup, AbstractControl, Validators, FormBuilder } from '@angular/forms';
import { NgbDate, NgbCalendar, NgbDateParserFormatter, NgbModal, ModalDismissReasons, NgbModalRef, NgbDatepickerI18n } from '@ng-bootstrap/ng-bootstrap';
import swal from 'sweetalert2';
import { TesoreriaService } from 'src/app/services/tesoreria.service';
import { TipoService } from 'src/app/services/tipo.service';
import { WebsocketService } from 'src/app/services/websocket.service';

@Component({
  selector: 'app-rtesoreria',
  templateUrl: './rtesoreria.component.html',
  styleUrls: ['./rtesoreria.component.css']
})
export class RtesoreriaComponent implements OnInit {

  public spinner = false
  public submitted = false
  public allCortes: any
  public obsVerificacion: number
  public suma = 0
  public opc = 0
  public closeResult: any
  public idCorte: any
  public info : any
  public observacion: string
  public typeServices: any
  public formR: FormGroup
  public valorverificacion: AbstractControl
 
  public fromDate;
  public toDate;

  public p: number = 1
  public formRegister: FormGroup

  public documento: AbstractControl
  public finicial: AbstractControl
  public ffinal: AbstractControl
  public estado: AbstractControl
 
  constructor(
    private formBuilder: FormBuilder,
    private calendar: NgbCalendar,
    public formatter: NgbDateParserFormatter,
    private tesoreriaService: TesoreriaService,
    private tipoService: TipoService,
    private websocketService: WebsocketService,
    private modalService: NgbModal) {
    this.formRegister = this.formBuilder.group({
      documento: ['', Validators.required],
      estado: [1],
      finicial: [''],
      ffinal: ['']
    })

    this.documento = this.formRegister.controls['documento']
    this.estado = this.formRegister.controls['estado']
    this.finicial = this.formRegister.controls['finicial']
    this.ffinal = this.formRegister.controls['ffinal']

    this.formR = this.formBuilder.group({
      valorverificacion: ['', Validators.required]
    })
    this.valorverificacion = this.formR.controls['valorverificacion']
    
  }

  ngOnInit() {
   this.getTipos()
  }

  setObs(){
    swal.fire({
      title: 'Observación',
      input: 'textarea',
      inputAttributes: {
        autocapitalize: 'off'
      },
      showCancelButton: true,
      confirmButtonText: 'Si',
      cancelButtonText: 'No',
      showLoaderOnConfirm: true,
      
     }).then((result) => {
      if(result.value)
      {
        console.log(" result ", result )
        this.observacion = result.value

        console.log(" this.observacion ", this.observacion )
      }
    })
  }
  get f() {
    return this.formRegister.controls;
  }

  get fv() {
    return this.formR.controls;
  }

  validateInput(currentValue: NgbDate, input: string): NgbDate {
    const parsed = this.formatter.parse(input);
    return parsed && this.calendar.isValid(NgbDate.from(parsed)) ? NgbDate.from(parsed) : currentValue;
  }

  cancelar(){
    this.formRegister.reset();
  }
  search() {
    this.submitted = true
    if(this.formRegister.invalid)
     return false;

    if (typeof (this.fromDate) === 'object') {
      this.formRegister.value.finicial =
      this.fromDate.day + "/" + this.fromDate.month + "/" +this.fromDate.year
    }
    else{
      this.formRegister.value.finicial = ""
    }

    if (typeof (this.toDate) === 'object') {
      this.formRegister.value.ffinal = 
      this.toDate.day + "/" + this.toDate.month + "/" + this.toDate.year
    }
    else{
      this.formRegister.value.ffinal = ""
    }

    this.getCortes()
  }

  getCortes() {
    
    this.spinner = true
    this.tesoreriaService.searhCortes(this.formRegister.value).subscribe((res: any) => {
      if (res.status) {
        this.submitted = false
        this.suma = 0
        this.allCortes = res.info
        for (let i = 0; i < res.info.length; i++) {
          res.info[i].corte  = 0
          this.suma += res.info[i].valorverficacion << 0
          res.info[i].corte += i+1
        }
        this.spinner = false

      }
      else{
        swal.fire("Aviso!", res.msg, "warning")
        this.spinner = false
        this.allCortes = null
      }
    },
      error => {
        swal.fire("Aviso!", "Ocurrio un error al consultar cortes ", "warning")
        this.spinner = false
        this.allCortes = null
      })
  }

  onCloseModal() {
    this.modalService.dismissAll();
  }

  showLoder()
  {
    swal.fire({
      text: 'Por favor espere un momento ...',
      imageUrl: '/assets/animation/Eclipse-1s-200px.svg',
      imageWidth: 100
    })
  }

  closeLoader()
  {
    swal.close()
  }

  printer(item:any, index:number)
  {
    item.totalmonedas =  parseInt(item.totalmonedas)
    item.corte = index
    const swalWithBootstrapButtons = swal.mixin({
      customClass: {
        confirmButton: 'btn btn-success',
        cancelButton: 'btn btn-danger'
      },
      buttonsStyling: false
    })
      
    swalWithBootstrapButtons.fire({
      title: 'Está seguro de imprimir relación?',
      text:'Al confirmar se imprimira el recibo de este corte, recuerde cambiar a papeleria blanca.',
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Si',
      cancelButtonText: 'No',
      reverseButtons: true
    }).then((result) => {
    if (result.value)
    {
      this.showLoder()
      item.iduser = sessionStorage.getItem('loginuser')
      let data = {
        corte:index,
        item:item
      }
      this.websocketService.printCorteCE(data)

      setTimeout(() => {
          this.closeLoader()
      }, 3000);
     }
    })
  }

  open(content: any, item: any) {
    this.formR.reset()
    this.idCorte = item.idcontrolefectivo
    this.closeResult = this.modalService.open(content, { size: 'lg' });
  }

  saveDirect(info: any) {
    const swalWithBootstrapButtons = swal.mixin({
      customClass: {
        confirmButton: 'btn btn-success',
        cancelButton: 'btn btn-danger'
      },
      buttonsStyling: false
    })

    swalWithBootstrapButtons.fire({
      title: 'Está seguro de registrar verificación ?',
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Si',
      cancelButtonText: 'No',
      reverseButtons: true
    }).then((result) => {
      if (result.value) {
        this.tesoreriaService.verificacion(info).subscribe((res: any) => {
          if (res.status) {
            swal.fire("Aviso!", "Verificación registrada satisfactoriamente ", "success")
            this.onCloseModal()
            this.getCortes()

            this.formR.reset()
            this.obsVerificacion = null
            this.observacion = ''
          }else{
            this.onCloseModal()
            swal.fire("Aviso!", "Ocurrio un error al registrar verificación", "warning")
          }
        }, error => {
          swal.fire("Aviso!", "Ocurrio un error al registrar verificación", "warning")
        })
      }
    })
  }

  save()
  {
    this.submitted = true
    if (this.formR.invalid)
    return false

    this.formR.value.valorverificacion = this.replaceAll(this.formR.value.valorverificacion, ".", "")
    for (let i = 0; i < this.allCortes.length; i++) 
    {
      if(this.idCorte == this.allCortes[i].idcontrolefectivo)
      {
        this.allCortes[i].valorverficacion = this.formR.value.valorverificacion 
      }
    }
 
    this.info = {
      id: this.idCorte,
      valor: this.formR.value.valorverificacion,
      obs:this.obsVerificacion,
      nota: this.observacion
    }
    this.opc = 1
    
    swal.fire("Aviso!", "Datos registrados satisfactoriamente ", "success")
    this.onCloseModal()
    this.submitted = false
    this.saveok(this.opc)

  }

  saveok(opc:number) {
    if(opc == 1)
    {
      this.opc = 0
      this.saveDirect(this.info)
    }
  }

  sendData(id:number){
    const swalWithBootstrapButtons = swal.mixin({
      customClass: {
        confirmButton: 'btn btn-success',
        cancelButton: 'btn btn-danger'
      },
      buttonsStyling: false
    })

    swalWithBootstrapButtons.fire({
      title: 'Está seguro de registrar verificación ?',
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Si',
      cancelButtonText: 'No',
      reverseButtons: true
    }).then((result) => {
      if (result.value) {
        this.tesoreriaService.verificacionok({ id: id , nota: this.observacion, obs:this.obsVerificacion }).subscribe((res: any) => {
          if (res.status) {
            swal.fire("Aviso!", "Verificación registrada satisfactoriamente", "success")
            this.onCloseModal()
            this.getCortes()
            this.observacion = ''
            this.obsVerificacion = null
          }
        }, error => {
          swal.fire("Aviso!", "Ocurrio un error al registrar verificación", "warning")
        })
      }
    })
  }


  validateNumber(event: any) {
    event.target.value = (event.target.value + '').replace(/[^0-9]/g, '')
    this.formR.get('valorverificacion').setValue(this.formatNumber(event.target.value, " "))
  }

  clear() {
    this.formR.reset()
  }

  formatNumber(num: any, prefix: any) {
    prefix = prefix || '';
    num += '';
    var splitStr = num.split('.');
    var splitLeft = splitStr[0];
    var splitRight = splitStr.length > 1 ? '.' + splitStr[1] : '';
    var regx = /(\d+)(\d{3})/;
    while (regx.test(splitLeft)) {
      splitLeft = splitLeft.replace(regx, '$1' + '.' + '$2');
    }
    return prefix + splitLeft + splitRight;
  }

  replaceAll(text: any, busca: any, reemplaza: any) {
    while (text.toString().indexOf(busca) != -1)
      text = text.toString().replace(busca, reemplaza)
    return text;
  }
  
  getTipos(){
    this.tipoService.getObs().subscribe( (res:any)=>{
      if(res.status){
        for(let i = 0; i < res.info.length; i++) {
           if(res.info[i].id == 1)
             res.info.splice(i,1)
           if(res.info[i].id == 2)
             res.info.splice(i,1)
           if(res.info[i].id == 0)
             res.info.splice(i,1)
        }
        this.typeServices = res.info
      }
    }, error =>{
      swal.fire("Aviso","Ocurrio un error al consultar tipos de observaciones ", "warning")
    })
  }

  getObs(event: any, id:number ){
    for (let i = 0; i < this.allCortes.length; i++) {
     if(this.allCortes[i].idcontrolefectivo == id)
     { 
       this.obsVerificacion = event
     }
   }
   this.setObs()
  }

}



