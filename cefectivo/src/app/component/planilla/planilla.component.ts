import { Component, OnInit } from '@angular/core';
import { WebsocketService } from 'src/app/services/websocket.service';
import { CortesService } from 'src/app/services/cortes.service';
import swal from 'sweetalert2';
import { PagoService } from 'src/app/services/pago.service';
import { DataService } from 'src/app/services/data.service';

@Component({
  selector: 'app-planilla',
  templateUrl: './planilla.component.html',
  styleUrls: ['./planilla.component.css']
})
export class PlanillaComponent implements OnInit {

  public spinner = false
  public allCortes  = [] 
  public listPagos = []
  public suma = 0
  public status = 0
  public sumapagos = 0
  public dateNow = new Date()

  constructor(
    private websocketService: WebsocketService,
    private cortesService: CortesService,
    private pagoService: PagoService,
    private dataService: DataService) { }

  ngOnInit() {
  }

  showloader(){
    this.spinner = true
  }

  hideloader(){
    this.spinner = false
  }

  printer(){
    if(this.status != 1)
    {
      this.showloader()
      this.getCortesById(this.dataService.getSessionUser().loginuser)
      this.getPagos()

      setTimeout(() => {
        let data = {
          cortes: this.allCortes.length,
          totalcortes: this.suma,
          premios:this.listPagos.length,
          totalpremios: this.sumapagos,
          iduser:this.dataService.getSessionUser().loginuser
        }
        this.swPlanilla(data)
        this.status = 1
      }, 8000)
    }

    if(this.status == 1){
      this.showloader()
      let data = {
        cortes: this.allCortes.length,
        totalcortes: this.suma,
        premios:this.listPagos.length,
        totalpremios: this.sumapagos,
        iduser:this.dataService.getSessionUser().loginuser
      }
      this.swPlanilla(data)
    }
  }

  swPlanilla(data:any){
    setTimeout(() => {
      this.websocketService.printerPlanilla(data)
      this.hideloader()
    }, 1000)
  }

  getCortesById(id: string)
  {
      this.cortesService.getCorteById(id).subscribe( (res:any) => {
        if(res.status)
        {
          this.allCortes = res.info
          for(let i = 0; i < res.info.length; i++) 
          {
            this.suma += res.info[i].totalefectivo << 0
          }
        }
      }, error => {
        swal.fire("Aviso","Ocurrio un error al consultar cortes", "warning")
        this.hideloader()
     })
  }

  getPagos(){
    //this.sumapagos = 0
    this.pagoService.getPagos(this.dataService.getSessionUser().loginuser).subscribe( (res:any)=>{
      if(res.status == true){
        this.listPagos  = res.info 
        for(let i = 0; i < res.info.length; i++) 
        {
          this.sumapagos += res.info[i].valor << 0
        }
      }
      //else 
       //swal.fire("Aviso ",res.msg, "warning")
    },
    error => {
      swal.fire("Aviso ","Ocurrio un error al consultar pagos", "warning")
      this.hideloader()
    })
  }

}
