import { Component, OnInit } from '@angular/core';
import { FormGroup, AbstractControl, FormBuilder, Validators } from '@angular/forms';
import { GuardasService } from 'src/app/services/guardas.service';
import { TesoreriaService } from 'src/app/services/tesoreria.service';
import swal from 'sweetalert2';
import { NgbModalRef, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { WebsocketService } from 'src/app/services/websocket.service';
import { Socket } from 'ngx-socket-io';
import { DataService } from 'src/app/services/data.service';
import { CentroEfectivoService } from 'src/app/services/ceefectivo.service';
import { CortesService } from 'src/app/services/cortes.service';


@Component({
  selector: 'app-centroefectivorecibe',
  templateUrl: './centroefectivorecibe.component.html',
  styleUrls: ['./centroefectivorecibe.component.css']
})
export class CentroefectivorecibeComponent implements OnInit {

  public guardas: any
  public recogidas: any
  public observacion: any
  public allCortes: any
  public spinner = false
  public showRecogida = false
  public submitted = false
  public idObs = null
  public count = 0
  public restante = 0
  public obstula = 1
  public obscandado = 1
  public p: number = 1
  public faltante: number
  public suma = 0

  public closeResult: string
  public modalRef: NgbModalRef

  public formRegister: FormGroup
  public formRecogida: FormGroup
  public formValidate: FormGroup
  public id: AbstractControl
  public idplaca: AbstractControl
  public idruta: AbstractControl
  public entregas: any
  public dateNow = new Date()
  public idnovedad: AbstractControl
  public password: AbstractControl

  constructor( 
    private formBuilder: FormBuilder,
    private guardasService: GuardasService,
    private tesoreriaService: TesoreriaService,
    private webSocketService: WebsocketService,
    private dataService: DataService,
    private centroEfectivoService: CentroEfectivoService,
    private cortesService: CortesService,
    private socket: Socket,
    private modalService: NgbModal) {

    this.formRegister = this.formBuilder.group({
      id: ['0', [ Validators.required ] ],
      idplaca: ['', [Validators.required ] ],
      idruta: ['', [Validators.required ] ]
    })

    this.id = this.formRegister.controls['id']
    this.idplaca = this.formRegister.controls['idplaca']
    this.idruta = this.formRegister.controls['idruta']

    this.formRecogida = this.formBuilder.group({
      idnovedad: [1, [ Validators.required ] ]
    })
    this.idnovedad = this.formRecogida.controls['idnovedad']

    this.formValidate = this.formBuilder.group({
      password: ['', [ Validators.required ] ]
    })
    this.password = this.formValidate.controls['password']

    //this.onSuccessRecogidaByGuarda()
   }

  ngOnInit() {
    /*this.getAllGuardas()
    this.getObservacion()*/
    //this.getAll()
    this.allEntregas()
  }

  showLoder()
  {
    swal.fire({
      text: 'Por favor espere un momento ...',
      imageUrl: '/assets/animation/Eclipse-1s-200px.svg',
      imageWidth: 100
    })
  }

  closeLoader()
  {
    swal.close()
  }

 

  allEntregas(){
    this.showLoder()
    this.centroEfectivoService.getEntregas().subscribe( (res:any)=>{
       let dataSend = []
       if(res.status){
        res.info.forEach((i:any) => {
          dataSend.push(i.guarda)
        })
       
        this.centroEfectivoService.all({guardas : dataSend }).subscribe( (res:any) =>{
         this.recogidas = res.info
         this.hide()
         this.closeLoader()
        }, error =>{
         swal.fire("", "Error al validar entregas ", "warning")
         this.hide()
         this.closeLoader()
        })
      }else{
        swal.fire("", res.msg, "warning")
        this.hide()
        this.closeLoader()
      }
    },
    error =>{
      swal.fire("", "Error al consultar entregas", "warning")
      this.hide()
      this.closeLoader()
    })
  }

  showloader(){
    this.spinner = true
  }

  hideloader(){
    this.spinner = false
  }

  getCortesById(id: string)
  {
      this.showloader()
      this.cortesService.getCorteById(id).subscribe( (res:any) => {
        if(res.status)
        {
          this.allCortes = res.info
          for(let i = 0; i < res.info.length; i++) 
          {
            this.suma += res.info[i].totalefectivo << 0
          }
        }
        this.hideloader()
      }, error => {
        swal.fire("Aviso","Ocurrio un error al consultar cortes", "warning")
        this.hideloader()
     })
  }


  getAll(){
    this.show()
    this.centroEfectivoService.get().subscribe( (res:any) =>{
      if(res.status){
        this.recogidas = res.info
      }
      this.hide()
    })
  }
  get f() {
    return this.formRegister.controls;
  }

  get fr() {
    return this.formRecogida.controls;
  }

  get fv() {
    return this.formValidate.controls;
  }

  getAllGuardas(){
    this.show()
    this.guardasService.get().subscribe( (res:any) =>{
      if(res.status){
        this.guardas =  res.info
        this.hide()
      }
    }, error =>{
      swal.fire("", "Erro al consultar guardas", "warning")
      this.hide()
   })
  }

  show(){
    this.spinner = true
  }

  hide(){
    this.spinner = false
  }

  getObservacion(){
    this.tesoreriaService.getObservacion().subscribe( (res:any) =>{
      if(res.status){
        this.observacion = res.info
      }
    }, error =>{
      swal.fire("", "Erro al consultar observaciones de tesesoreria", "warning")
   })
  }

  onSuccessRecogidaByGuarda() 
  {
    this.socket.on('rguarda', (data: any) => {
      
      this.showRecogida = true
      this.recogidas = data.dataTransportMonitoreo
      this.count = data.dataTransportMonitoreo.length
      this.hide()
    })
  }
  searchByGuarda(event: number){
     if(event > 0){
      this.show()
      this.tesoreriaService.getRecogidas(event).subscribe( (res:any)=>{
      if(res.status){
        this.showRecogida = true
        this.recogidas =  res.info
       if(res.info.length > 0)
        this.count = res.info.length
      }
      else{
        swal.fire("",res.msg, "warning") 
        this.showRecogida = false
      }
      this.hide()
        
    }, error =>{
       swal.fire("", "Error al consultar recogidas", "warning")
       this.hide()
    })
  }
  }
  
  getObservacionRecogida(event: number){
      this.idObs = event
  }

  novedad(item: any){

  }


  detalle(item: any){

  }

  save(){
    if(this.recogidas.length > 0){
      const swalWithBootstrapButtons = swal.mixin({
        customClass: {
          confirmButton: 'btn btn-success',
          cancelButton: 'btn btn-danger'
        },
        buttonsStyling: false
      })
        
      swalWithBootstrapButtons.fire({
        title: 'Está seguro de registrar entrega?',
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Si',
        cancelButtonText: 'No',
        reverseButtons: true
      }).then((result) => {
      if (result.value)
      {
        for (let i = 0; i < this.recogidas.length; i++) {
          this.recogidas[i].rtesoreria = this.dataService.getSessionUser().loginuser
        }
        this.tesoreriaService.save(this.recogidas).subscribe( (res:any)=>{
         if(res.status){
           swal.fire("Aviso","Entrega de tulas y candados registrada satisfactoriamente", "success")
           this.recogidas = null
           this.showRecogida = false
           this.webSocketService.getMonitoreo()
          }
       },error =>{
          swal.fire("", "Error al registrar entrega de Tulas y Candados", "warning")
          this.recogidas = null
          this.hide()
        })
       }
      })
     }
  }

  getObs(event: string, id:number ){
     for (let i = 0; i < this.recogidas.length; i++) {
      if(this.recogidas[i].id == id)
      { 
        this.recogidas[i].obs = parseInt(event)
        if(parseInt(event) == 5)
          this.recogidas[i].faltante = 1
        else
        {
          this.recogidas[i].faltante = 0
          this.recogidas[i].obs = 0
        }
      }
    }
  }

  validateNumber(event: any, id:number)
  { 
    event.target.value = (event.target.value + '').replace(/[^0-9]/g, '')
    for(let i = 0; i < this.recogidas.length; i++) {
      if(this.recogidas[i].id == id)
      {
        this.recogidas[i].faltante = parseInt(event.target.value)
      }
    }
  }


  validarEntrega(){
    this.submitted = true
    if(this.formValidate.invalid)
     return false;

    console.log( this.formValidate.value );
  }

  novedadTula(item: any){
    for (let i = 0; i < this.recogidas.length; i++) {
      if(this.recogidas[i].id == item.id){

      }
    }
    console.log( item );
  }

  printer(item:any, index:number)
  {
    const swalWithBootstrapButtons = swal.mixin({
      customClass: {
        confirmButton: 'btn btn-success',
        cancelButton: 'btn btn-danger'
      },
      buttonsStyling: false
    })
      
    swalWithBootstrapButtons.fire({
      title: 'Está seguro de imprimir relación?',
      text:'Al confirmar se imprimira el recibo de este corte, recuerde cambiar a papeleria blanca.',
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Si',
      cancelButtonText: 'No',
      reverseButtons: true
    }).then((result) => {
    if (result.value)
    {
      item.iduser = sessionStorage.getItem('loginuser')
      let data = {
        corte:index,
        item:item
      }
      this.webSocketService.printCorteCE(data)
     }
    })
  }

}
