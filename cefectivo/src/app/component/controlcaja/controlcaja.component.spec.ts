import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ControlcajaComponent } from './controlcaja.component';

describe('ControlcajaComponent', () => {
  let component: ControlcajaComponent;
  let fixture: ComponentFixture<ControlcajaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ControlcajaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ControlcajaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
