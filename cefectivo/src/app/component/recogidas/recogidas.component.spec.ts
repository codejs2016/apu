import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RecogidasComponent } from './recogidas.component';

describe('RecogidasComponent', () => {
  let component: RecogidasComponent;
  let fixture: ComponentFixture<RecogidasComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RecogidasComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RecogidasComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
