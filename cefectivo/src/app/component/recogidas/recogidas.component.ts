import { Component, OnInit } from '@angular/core';
import { GuardasService } from 'src/app/services/guardas.service';
import { EntregaefectivoComponent } from '../entregaefectivo/entregaefectivo.component';
import { FormGroup, AbstractControl, Validators, FormBuilder } from '@angular/forms';
import swal from 'sweetalert2';
import { NgbModalRef, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { RecogidasService } from 'src/app/services/recogidas.service';

@Component({
  selector: 'app-recogidas',
  templateUrl: './recogidas.component.html',
  styleUrls: ['./recogidas.component.css']
})
export class RecogidasComponent implements OnInit {
  
  public p: number = 1
  public formRegister: FormGroup
  public finicial: AbstractControl
  public ffinal: AbstractControl
  public recolector: AbstractControl
  public id: AbstractControl
  public dataFormHT = []

  public guardas : any
  public dataRecogidas: any
  public spinner = false
  public submitted = false
  public sh = false
  
  constructor(
    private formBuilder: FormBuilder,
    private guardasService: GuardasService,
    private recogidasService:RecogidasService) {
       
     this.initForm()
  }

  ngOnInit() {
    this.getGuardas()
  }

  initForm()
  {
    this.formRegister = this.formBuilder.group({
      id: [14472879, Validators.required ],
      finicial: ['', [Validators.required]],
      ffinal: ['', [Validators.required]],
    })

    this.id = this.formRegister.controls['id']
    this.finicial = this.formRegister.controls['finicial']
    this.ffinal = this.formRegister.controls['ffinal']
  }

  get f() {
    return this.formRegister.controls;
  }

  getGuardas(){
    this.show()
    this.guardasService.get().subscribe((res:any)=>{
      if(res.status){
       this.guardas = res.info
       this.hide()
      }
      else
       this.hide()
    }, 
    error =>{
     swal.fire("Ocurrio un error al consultar guardas", "warning")
     this.hide()
    })
   }

  clearForm(){
   this.formRegister.reset() 
   this.initForm()
  }

  show(){
    this.spinner = true
  }

  hide(){
    this.spinner = false
  }

  getAllGuardas(){
    this.show()
    this.guardasService.get().subscribe( (res:any) =>{
      if(res.status){
        this.guardas =  res.info
        this.hide()
      }
      else
        this.hide()
    }, error =>{
      swal.fire("", "Erro al consultar guardas", "warning")
      this.hide()
   })
  }

  search(){
    this.submitted = true
    if(this.formRegister.invalid)
     return

     this.spinner = true
     this.dataFormHT = []
     this.dataFormHT.push(this.formRegister.value)
     this.formRegister.value.finicial =  
     this.dataFormHT[0].finicial.day+"/"+this.dataFormHT[0].finicial.month+"/"+this.dataFormHT[0].finicial.year

     this.formRegister.value.ffinal =  
     this.dataFormHT[0].ffinal.day+"/"+this.dataFormHT[0].ffinal.month+"/"+this.dataFormHT[0].ffinal.year

     this.recogidasService.search(this.formRegister.value).subscribe( (res:any)=>{
      if(res.status) {
        this.dataRecogidas = res.info
        this.formRegister.reset()
        this.submitted = false
        this.sh = true
        this.spinner = false
      }else{
        swal.fire("No se encuentran recogidas")
        this.sh = false
        this.spinner = false
      }
    }, 
     error => {
      this.sh = false
      this.spinner = false
      swal.fire("Ocurrio un error al consultar recogidas")
    })
  }

}
