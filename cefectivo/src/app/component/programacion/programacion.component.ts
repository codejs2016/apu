import { Component, OnInit } from '@angular/core';
import { FormGroup, AbstractControl, Validators, FormBuilder } from '@angular/forms';
import { GuardasService } from 'src/app/services/guardas.service';
import { NgbDate, NgbCalendar, NgbDateParserFormatter, NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import swal from 'sweetalert2';
import { ConfigService } from 'src/app/services/config-service';
import { WebsocketService } from 'src/app/services/websocket.service';
import { Socket } from 'ngx-socket-io';
import * as moment from 'moment';

@Component({
  selector: 'app-programacion',
  templateUrl: './programacion.component.html',
  styleUrls: ['./programacion.component.css']
})
export class ProgramacionComponent implements OnInit {


  public tipocambio = 0
  public ObsGD: any
  public daysPrg: any
  public guardas = []
  public lisguardas: any
  public vehiculos: any
  public rutas: any
  public idplacaVehiculo:number = 0
  public idrutaVehiculo:number = 0
  public dataProgramacion: any
  public dataFormProg = []
  public recursos: any
  public lottieConfig: Object
  public spinner = false
  public spinnerRT = false
  public spinnerUp = false
  public submitted = false
  public disabledSelect = false
  public showSuccess = false
  public showError = false
  public idAgenda = null
  public countrc = 0
  public currentPlaca: string
  public currentRuta: string
  public msg: any
  public closeResult: any
  public selectR: any


  public hoveredDate: NgbDate;

  public fromDate: NgbDate;
  public toDate: NgbDate;

  public formRegister: FormGroup
  
  public finicial: AbstractControl
  public ffinal: AbstractControl

  public formAgenda: FormGroup
  public id: AbstractControl
  public idplaca: AbstractControl
  public idruta: AbstractControl


  constructor(private guardasService: GuardasService,
    private websocketService: WebsocketService,
    private socket: Socket,
    private formBuilder: FormBuilder,
    private calendar: NgbCalendar,
    public configService: ConfigService,
    public formatter: NgbDateParserFormatter,
    private modalService: NgbModal) {

    this.formRegister = this.formBuilder.group({
      finicial: ['', [Validators.required]],
      ffinal: ['', [Validators.required]]
    })

    this.finicial = this.formRegister.controls['finicial']
    this.ffinal = this.formRegister.controls['ffinal']

    this.formAgenda = this.formBuilder.group({
      id: [''],
      idplaca: [1, [Validators.required ] ],
      idruta: ['', [Validators.required ] ]
    })

    this.id = this.formAgenda.controls['id']
    this.idplaca = this.formAgenda.controls['idplaca']
    this.idruta = this.formAgenda.controls['idruta']

    this.getObsGD()
    this.getRenew()
  }


  ngOnInit() {
    this.getGuardas()
    this.getVehicles()
    this.getDayProgramming()
   //this.getProgramacion()
    this.getRutas()
  }


  getGuardas(){
   this.guardasService.get().subscribe((res:any)=>{
     if(res.status){
      this.lisguardas = res.info
     }
   }, 
   error =>{
    swal.fire("Ocurrio un error al consultar guardas", "warning")
   })
  }

  getRutas(){
    this.guardasService.getRutas().subscribe((res:any)=>{
      if(res.status){
       this.rutas = res.info
      }
    }, 
    error =>{
     swal.fire("Ocurrio un error al consultar rutas", "warning")
    })
  }

  getVehicles(){
    this.guardasService.getVehiculos().subscribe((res:any)=>{
      if(res.status){
       this.vehiculos = res.info
      }
    }, 
    error =>{
     swal.fire("Ocurrio un error al consultar guardas", "warning")
    })
  }

  removePlaca(){
   this.currentPlaca = "" 
  }

  alertOk(item:any){
   this.recursos.forEach((i:any) => {
       if(item.documento == i.documento){
         i.status = true
         i.popu = false
       }
    })
  }

  getPlacaPR(idplaca:number){
      if(idplaca > 0){
       this.idplacaVehiculo = idplaca
        for (let i = 0; i < this.vehiculos.length; i++) {
           if(this.vehiculos[i].idvehiculo == idplaca){
            this.currentPlaca = this.vehiculos[i].placa
           } 
        }
      }
  }

  getPlaca(idplaca:number, item:any, tcp: number){
    if(this.tipocambio == tcp){
      if(idplaca > 0){
        this.alertOk(item)
        this.idplacaVehiculo = idplaca
        for (let i = 0; i < this.vehiculos.length; i++) {
           if(this.vehiculos[i].idvehiculo == idplaca){
            this.currentPlaca = this.vehiculos[i].placa
           } 
        }
      }
    }else
       this.alert(item)
    
  }

  getRutaPR(idruta:number ){
    if(idruta > 0){
      this.idrutaVehiculo = idruta
      for (let i = 0; i < this.rutas.length; i++) {
          if(this.rutas[i].idseguridadruta == idruta){
          this.currentRuta = this.rutas[i].ruta
          } 
      }
    }
  }

  getRuta(idruta:number, item: any, tcp: number){
    if(this.tipocambio == tcp)
    {
      if(idruta > 0){
        this.alertOk(item)
        this.idrutaVehiculo = idruta
        for (let i = 0; i < this.rutas.length; i++) {
           if(this.rutas[i].idseguridadruta == idruta){
            this.currentRuta = this.rutas[i].ruta
           } 
        }
      }
    }
    else{
      this.alert(item)
    }
  }

  get fa() {
    return this.formAgenda.controls;
  }

  get f() {
    return this.formRegister.controls;
  }

  save(){
    if( typeof(this.fromDate) === 'object' && typeof(this.toDate) === 'object' ) 
    {
      this.submitted = false
      this.formRegister.value.finicial =  
      this.fromDate.day+"/"+this.fromDate.month+"/"+this.fromDate.year
      this.formRegister.value.ffinal =  
      this.toDate.day+"/"+this.toDate.month+"/"+this.toDate.year

      this.guardasService.saveProgramacion(this.formRegister.value).subscribe( (res:any)=>{
        switch (res.status) 
        {
           case true:
              swal.fire("Agenda","Registrada satisfactoriamente", "success")
              this.onlypr()
              this.submitted = false
              this.formRegister.reset()
              this.fromDate = null
              this.toDate = null
           break;
  
           case 'existe':
             swal.fire("Aviso!",res.msg,"warning")
           break;
  
          default:
             swal.fire("Ocurrio un error al registrar agenda")
            break;
        } 
      }, 
       error => {
       swal.fire("Aviso!","Ocurrio un error al registrar programación","warning")
      })
      
    }
    else
    {
      this.submitted = true
    }
  }

  getRolPr(rol: number, item:any, opc:number){
     if(opc == 1){
        for (let i = 0; i < this.guardas.length; i++) {
          if(this.guardas[i].documento == item.documento)
          {
            this.guardas[i].rol = rol
            this.recursos[i].popu = false
          }
       }
      }else{
        for (let i = 0; i < this.recursos.length; i++) {
          this.recursos[i].stobs = false
          this.recursos[i].status = false
          this.recursos[i].popu = false
          if(this.recursos[i].documento == item.documento)
          {
            this.recursos[i].rol = rol
            this.recursos[i].stobs = true
            this.recursos[i].status = true
          }
       }
      }
  }

  getRol(rol: number, item:any, opc:number, tpc:number){
    if(this.tipocambio == tpc){
      if(opc == 1){
        for (let i = 0; i < this.guardas.length; i++) {
          if(this.guardas[i].documento == item.documento)
          {
            this.guardas[i].rol = rol
            this.recursos[i].popu = false
          }
       }
       this.alertOk(item)
      }else{
        for (let i = 0; i < this.recursos.length; i++) {
          this.recursos[i].stobs = false
          this.recursos[i].status = false
          this.recursos[i].popu = false
          if(this.recursos[i].documento == item.documento)
          {
            this.recursos[i].rol = rol
            this.recursos[i].stobs = true
            this.recursos[i].status = true
          }
       }
      }
    }else
     this.alert(item)
  }

  alert(item:any){
    swal.fire("El tipo de cambio no coincide")
    this.recursos.forEach((i:any) => {
       if(item.documento == i.documento){
         i.status = false
       }
    })
  }


  addObs(value: any, item: any){
    for (let i = 0; i < this.recursos.length; i++) {
      this.recursos[i].stobs = false
      if(this.recursos[i].documento == item.documento)
      {
        this.recursos[i].obs = value
        this.recursos[i].stobs = true
      }
   }
  }
  
  
  getCambio(tipocambio: any, item:any){
     
     console.log( " tipocambio ", parseInt(tipocambio) )
     this.tipocambio = parseInt(tipocambio)
      console.log( typeof this.tipocambio  )
      for (let i = 0; i < this.recursos.length; i++) {
        this.recursos[i].popu = false
        this.recursos[i].stobs = false
        this.recursos[i].status = false
        if(this.recursos[i].documento == item.documento)
        {
          this.recursos[i].stobs = true
          this.recursos[i].tipocambio = tipocambio
          this.recursos[i].status = true
        }
     }
  }

  saveAgenda(){
    const swalWithBootstrapButtons = swal.mixin({
      customClass: {
        confirmButton: 'btn btn-success',
        cancelButton: 'btn btn-danger'
      },
      buttonsStyling: false
    })
      
    swalWithBootstrapButtons.fire({
      title: 'Está seguro de registrar recursos?',
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Si',
      cancelButtonText: 'No',
      reverseButtons: true
    }).then((result) => {
    if (result.value)
    {
       if(this.guardas.length > 0){
        this.spinner = true
        for (let i = 0; i < this.guardas.length; i++) {
          this.guardas[i].idplaca = this.idplacaVehiculo
          this.guardas[i].idplaca = parseInt(this.guardas[i].idplaca)
          this.guardas[i].idruta = this.idrutaVehiculo
          this.guardas[i].idruta = parseInt(this.guardas[i].idruta)
          this.guardas[i].tipocambio = 0
        }
  
        this.spinner = true
        let info = {
          data:this.guardas
        }
        
        this.guardasService.saveAgenda(info).subscribe( (res:any)=>{
          if(res.status){
            swal.fire("Ruta registrada exitosamente", res.msg, "success")
            this.spinner = false
            this.onCloseModal()
            this.onlypr()
            //this.getProgramacion()
          }
        }, 
        error => {
          swal.fire("Ocurrio un error al agendar ruta", "warning")
          this.spinner = false
        })
      }
      else
        swal.fire("Por favor asociar vehiculo y guardas", "warning") 
     }
    })
  }

  getDayProgramming(){
    this.guardasService.getProgramacionDays().subscribe( (res:any) =>{
      this.daysPrg = res.info
      this.getProgramacion()
    },
    error => {
      swal.fire("Aviso","Ocurrio un error al consultar programación", "warning")
    })
  }

  onlypr(){
    this.guardasService.getProgramacion().subscribe( (res:any) =>{
      if(res.status){
        this.dataProgramacion = res.info
        this.spinnerRT = false
      }else
        this.spinnerRT = false
    },
    error => {
      swal.fire("Aviso","Ocurrio un error al consultar programación", "warning")
      this.spinnerRT = false
    })
  }

  getProgramacion(){
    this.spinnerRT = true
    this.guardasService.getProgramacion().subscribe( (res:any) =>{
      if(res.status){
         for (let i = 0; i < res.info.length; i++) {
           res.info[i].days = 0
           for(let j = 0; j < this.daysPrg.length; j++) {
            var now = moment(new Date())
            var end = moment(res.info[i].ffinal).format('YYYY-MM-DD'); // another date
            var duration = moment.duration(now.diff(end));
            res.info[i].days= duration.asDays(); 
            if(parseInt(res.info[i].days) >= this.daysPrg[i].days){
              let dataSend = []
              dataSend.push(this.daysPrg[j].id)
              this.websocketService.enable(dataSend)
            }
          }
          res.info[i].cnt = 0
        }
        this.dataProgramacion = res.info
        this.spinnerRT = false
      }else
        this.spinnerRT = false
    },
    error => {
      swal.fire("Aviso","Ocurrio un error al consultar programación", "warning")
      this.spinnerRT = false
    })
  }

  deleteProgramacion(id:any){
    const swalWithBootstrapButtons = swal.mixin({
      customClass: {
        confirmButton: 'btn btn-success',
        cancelButton: 'btn btn-danger'
      },
      buttonsStyling: false
    })
    swalWithBootstrapButtons.fire({
      title: 'Está seguro de eliminar agenda ?',
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Si',
      cancelButtonText: 'No',
      reverseButtons: true
    }).then((result) => {
    if (result.value)
    {
      let data = {
      idagenda:id
    }
    this.guardasService.deleteAgenda(data).subscribe( (res:any) =>{
        setTimeout(() => {
            this.dataProgramacion = null
            this.getProgramacion()
        }, 1000);
    },
    error => {
      swal.fire("Ocurrio un error al eliminar agenda", "warning")
      this.spinnerRT = false
    })
     }
   })
  }

  deleteRuta(id:number){
    const swalWithBootstrapButtons = swal.mixin({
      customClass: {
        confirmButton: 'btn btn-success',
        cancelButton: 'btn btn-danger'
      },
      buttonsStyling: false
    })
    swalWithBootstrapButtons.fire({
      title: 'Está seguro de eliminar guarda de la programación ?',
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Si',
      cancelButtonText: 'No',
      reverseButtons: true
    }).then((result) => {
    if (result.value)
    {
      let data = {
        documento:id,
        idagenda:this.idAgenda
       }

       this.guardasService.deleteGuardaAgenda(data).subscribe( (res:any)=>{
          if(res.status){
            for (let i = 0; i < this.recursos.length; i++) {
              if(this.recursos[i].documento == id){
                this.recursos.splice(i,1)
              }
            }
            this.showSuccess = true
            this.msg = "Guarda borrado de programación satisfactoriamente"
          }
        },
        error => {
          this.msg = "Ocurrio un error al eliminar guarda"
          this.showError = false
        })
     }
   })
  }

  closeAlert(){
    this.spinnerRT = false
    this.showError = false
    this.spinnerRT = false
    this.showSuccess = false
  }
 
   editRutaGuarda(item:any){

    console.log(" item.tipocambio ",  item.tipocambio )
    if (typeof item.tipocambio === 'undefined') {
      swal.fire({
        text: '!Por favor seleccionar el tipo de cambio!'
      })
    }else{
      switch(parseInt(item.tipocambio)) {
        case 1:
          if(this.idplacaVehiculo == 0 ){
            swal.fire({
              text: '!Por favor seleccionar placa!'
            })
          }
        break
        case 2:
          if(this.idrutaVehiculo == 0 ){
            swal.fire({
              text: '!Por favor seleccionar ruta!'
            })
          }
        break
        case 3:
          if(typeof item.rol === 'undefined'){
            swal.fire({
              text: '!Por favor seleccionar rol!'
            })
          }
        break
        case 4:
          if(this.idrutaVehiculo == 0 || this.idplacaVehiculo == 0 ){
            swal.fire({
              text: '!Por favor seleccionar placa y ruta !'
            })
          }
        break
        case 5:
          if(item.rol == 0 || this.idplacaVehiculo == 0 ){
            swal.fire({
              text: '!Por favor seleccionar placa y rol!'
            })
          }
        break;
        case 6:
          if(item.rol == 0 || this.idplacaVehiculo == 0 ){
            swal.fire({
              text: '!Por favor seleccionar rol y ruta!'
            })
          }
        break
        case 7:
          if(item.rol == 0 || this.idplacaVehiculo == 0 || this.idrutaVehiculo == 0){
            swal.fire({
              text: '!Por favor seleccionar placa, ruta y rol!'
            })
          }
        break
      }
    }
     
    //valida la observación
    if(typeof item.obs === 'undefined'){
      swal.fire({
        text: '!Por favor agregar observación!'
      })
    }
    else
    {
      if(parseInt(item.tipocambio) > 0){
       let data = { }
          if(this.idplacaVehiculo > 0){
            data = {
              rol:parseInt(item.rol),
              idruta:this.idrutaVehiculo,
              idplaca:this.idplacaVehiculo,
              documento:item.documento,
              obs:item.obs,
              idagenda:this.idAgenda,
              tipocambio:parseInt(item.tipocambio)
            }

            console.log(" data idplacaVehiculo ", data )
            this.sendUpdateGD(data)
          }

          if(this.idrutaVehiculo > 0){
            data = {
              rol:parseInt(item.rol),
              idruta:this.idrutaVehiculo,
              idplaca:this.idplacaVehiculo,
              documento:item.documento,
              obs:item.obs,
              idagenda:this.idAgenda,
              tipocambio:parseInt(item.tipocambio)
            }
            this.sendUpdateGD(data)
          }

         if(parseInt(item.rol) > 0){
            data = {
              rol:parseInt(item.rol),
              idruta:this.idrutaVehiculo,
              idplaca:this.idplacaVehiculo,
              documento:item.documento,
              obs:item.obs,
              idagenda:this.idAgenda,
              tipocambio:parseInt(item.tipocambio)
            }
            this.sendUpdateGD(data)
          }
      }
    }
  }

  sendUpdateGD(data: any){
    this.guardasService.updateGuaraAgenda(data).subscribe( (res:any)=>{
      if(res.status){
        this.msg = "Datos actualizados satisfactoriamente"
        this.getRecursosActualizados(this.idAgenda)
        this.spinnerRT = false
        this.showSuccess = true
      }
    },
    error => {
      swal.fire("Ocurrio un error al actualizar recurso", "warning")
      this.spinnerRT = false
    }) 
  }

  viewOBs(item:any){
    this.recursos.forEach((i:any) => {
      if(item.documento == i.documento)
      {
        i.loader = true 
        i.stobs = false
      }
    })
    this.websocketService.getObsGD({ documento: item.documento, idagenda: this.idAgenda })
  }

  hideOBs(item:any){
    for(let i = 0; i < this.recursos.length; i++) {
      if(this.recursos[i].documento == item.documento)
        this.recursos[i].popu = !this.recursos[i].popu
     }
  }


  getObsGD(){
   this.socket.on('getObsGD', (data: any) => 
   {
     if(data.data.length > 0)
     {
      for(let i = 0; i < this.recursos.length; i++) {
        this.recursos[i].popu = false
        this.recursos[i].loader = false
        if(this.recursos[i].documento == data.documento)
          this.recursos[i].popu = true
        }
        this.ObsGD = data.data
     }
     else
     {
       this.recursos.forEach((i:any) => {
         i.loader = false 
         i.popu = false
       })
     }
    })
  }


  getRenew(){
    this.socket.on('enable', (data: any) => 
    {
      this.onlypr()
    })
  }

  getRecursosActualizados(id:number){
    this.guardasService.getRecurso(id).subscribe( (res:any) =>{
      if(res.status){
        for (let i = 0; i < res.info.length; i++) {
          res.info[i].status = false
          res.info[i].stobs = false
          res.info[i].loader = false
        }
        this.recursos = res.info
      }
      else
      {
        swal.fire("No hay recursos asociados a esta programación") 
        this.spinnerRT = false 
      }  
    },
    error => {
      swal.fire("Ocurrio un error al consultar recursos ")
      this.spinnerRT = false
    })
  }
  

  getRecurso(id:number, recurso?:any){
    this.spinnerRT = true
    this.idAgenda = id
    this.guardasService.getRecurso(id).subscribe( (res:any) =>{
      if(res.status){
        for (let i = 0; i < res.info.length; i++) {
          res.info[i].status = false
          res.info[i].popu = false

          res.info[i].crol = false
          res.info[i].cruta = false
          res.info[i].cplaca = false
        }
        this.recursos = res.info
        this.spinnerRT = false
        this.openRecurso(recurso)
      }
      else{
        this.spinnerRT = false
        swal.fire("No hay recursos asociados a esta programación")  
      }
    },
    error => {
      swal.fire("Ocurrio un error al consultar recursos ")
      this.spinnerRT = false
    })
  }

  deleteRc(item:any){
    for (let i = 0; i < this.guardas.length; i++) {
      if(this.guardas[i].documento == item.documento){
        this.guardas.splice(i,1)
        this.lisguardas.push(item)
        this.countrc = this.guardas.length
      }
    }
  }

  asociar(data:any){
    if(this.idplacaVehiculo > 0 && this.idrutaVehiculo > 0){
      for (let i = 0; i < this.dataProgramacion.length; i++) {
        if(this.dataProgramacion[i].id == this.idAgenda){
          this.dataProgramacion[i].cnt = this.dataProgramacion[i].cnt + 1 
          this.countrc = this.dataProgramacion[i].cnt
        }
      }
      this.guardas.push({ 
            idAgenda:this.idAgenda,
            nombres:data.nombres,
            apellido1:data.apellido1,
            apellido2:data.apellido2,
            documento:data.documento,
            idplaca: this.idplacaVehiculo,
            idruta: this.idrutaVehiculo 
      });
      this.delete(data.documento)
    }
    else
      swal.fire("Aviso!","Por favor seleccionar un placa y una ruta para poder agregar guarda ","warning")
  }

  delete(id:number){
    for (let i = 0; i < this.lisguardas.length; i++) {
      if(this.lisguardas[i].documento == id){
        this.lisguardas.splice(i,1)
      }
    }
  }

  sendata(){
    swal.fire({
      title: 'Registro',
      text: 'Está seguro de registrar programación',
      type: 'warning',
      showCancelButton: true,
      cancelButtonText: 'Cancelar',
      confirmButtonText: 'Sí'
    }).then((result) => {
        if(result.value){
           this.onCloseModal()
        }
    })
  }

  onCloseModal() 
  {
   this.modalService.dismissAll();
  }

  openRecurso(content:any) {
    this.closeResult =  this.modalService.open(content, { size: 'lg' });
  }

  openObs(content:any) {
    this.closeResult =  this.modalService.open(content);
  }

  enable(item:any){
    for (let i = 0; i < this.recursos.length; i++) {
      if(this.recursos[i].documento == item.documento){
        this.recursos[i].estado = !this.recursos[i].estado
        this.guardasService.status({ id: item.documento ,  
                                     st : this.recursos[i].estado,
                                     idagenda: this.idAgenda }).subscribe((res: any) =>{ })
      }
    }
  }

  cancelAction(item:any){
    for (let i = 0; i < this.recursos.length; i++) {
      if(this.recursos[i].documento == item.documento)
        this.recursos[i].status = false
    }
  }

  editRuta(item:any, type?: number){
    for (let i = 0; i < this.recursos.length; i++) {
       if(type == 1){
         this.recursos[i].status = false
       }
       else{
        this.recursos[i].stobs = false
        this.recursos[i].popu = false
        if(this.recursos[i].documento == item.documento){
          this.recursos[i].status = true
          this.recursos[i].stobs = true
        }
       }
    }
  }
    
  open(content:any, item?:any) {
    this.idAgenda = item.id
    this.closeResult = this.modalService.open(content, {ariaLabelledBy: 'modal-basic-title'}).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }

  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return  `with: ${reason}`;
    }
  }

  clearForm(){
    this.formRegister.reset()
  }

  clear() {
    this.guardas = []
    this.currentPlaca = ""
    this.idplacaVehiculo = 0
    this.formAgenda.reset()
    this.getProgramacion()
    this.getVehicles()
    this.getGuardas()
    this.onCloseModal()
  }

  limpiarRG(){
    this.guardas = []
    this.currentPlaca = ""
    this.idplacaVehiculo = 0
    this.formAgenda.reset()
    this.getProgramacion()
    this.getVehicles()
    this.getGuardas()
  }

  validateNumber(event: any) {
    event.target.value = (event.target.value + '').replace(/[^0-9]/g, '')
  }

  onDateSelection(date: NgbDate) {
    if (!this.fromDate && !this.toDate) {
      this.fromDate = date;
    } else if (this.fromDate && !this.toDate && date.after(this.fromDate)) {
      this.toDate = date;
    } else {
      this.toDate = null;
      this.fromDate = date;
    }
  }

  isHovered(date: NgbDate) {
    return this.fromDate && !this.toDate && this.hoveredDate && date.after(this.fromDate) && date.before(this.hoveredDate);
  }

  isInside(date: NgbDate) {
    return date.after(this.fromDate) && date.before(this.toDate);
  }

  isRange(date: NgbDate) {
    return date.equals(this.fromDate) || date.equals(this.toDate) || this.isInside(date) || this.isHovered(date);
  }

  validateInput(currentValue: NgbDate, input: string): NgbDate {
    const parsed = this.formatter.parse(input);
    return parsed && this.calendar.isValid(NgbDate.from(parsed)) ? NgbDate.from(parsed) : currentValue;
  }

}
