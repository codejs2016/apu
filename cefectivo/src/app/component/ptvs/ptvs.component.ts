import { Component, OnInit } from '@angular/core';
import { ConfigService } from 'src/app/services/config-service';
import { WebsocketService } from 'src/app/services/websocket.service';
import { Socket } from 'ngx-socket-io';
import swal from 'sweetalert2';
import { PuntosService } from 'src/app/services/puntos.service';

@Component({
  selector: 'app-ptvs',
  templateUrl: './ptvs.component.html',
  styleUrls: ['./ptvs.component.css']
})
export class PtvsComponent implements OnInit {

  public zonas:any
  public zcentro:any
  public znorte:any
  public zsur:any

  public zdagua :any
  public puntosVentas: any
  public currenPoint : string
  public spinner = false
  public showcentro = false
  public shownorte = false
  public showsur = false
  public showPoints = false
  public cntPtvRecogidos = 0
  public cntPtvPorRecoger = 0

  constructor(
    private config: ConfigService,
    private websocketService: WebsocketService,
    private puntosServices: PuntosService,
    private socket: Socket) {
     this.onSuccessSums()
    }

  ngOnInit() {
    this.zonas = this.getZonas()
    this.getAllRecogidas()
  }

  getZonas(){
    return this.config.getConfig().zonas
  }
 
  hidezonas(){
    this.shownorte = false
    this.showcentro = false
    this.showsur = false
  }

 getAllRecogidas()
 {
   this.websocketService.getMonitoreo()
   this.socket.on('monitoreozonas', (data: any) => {
     console.log("monitoreozonas  data ", data )
     this.zonas =  data.dataTransportMonitoreo;
      for(let i = 0; i < this.zonas.length; i++)
        {
          switch (parseInt(data.dataTransportMonitoreo[i].zona)) {
            case 1087:
              this.znorte = null
              this.znorte = this.zonas[i].puntos
            break;
  
            case 1151:
              this.zcentro = null
              this.zcentro = this.zonas[i].puntos
            break;
  
            case 1152:
              this.zsur = null
              this.zsur = this.zonas[i].puntos
            break;
          }
        }

     })
 }

  onSuccessSums() 
  {
      this.showLoder();
      this.socket.on('monitoreozonas', (data: any) => {
      this.zonas =  data.dataTransportMonitoreo;
      for(let i = 0; i < this.zonas.length; i++)
        {
          switch (parseInt(data.dataTransportMonitoreo[i].zona)) {
            case 1087:
              this.znorte = null
              this.znorte = this.zonas[i].puntos
            break;
  
            case 1151:
              this.zcentro = null
              this.zcentro = this.zonas[i].puntos
            break;
  
            case 1152:
              this.zsur = null
              this.zsur = this.zonas[i].puntos
            break;
          }
      }
    })
    this.closeLoader();
  }

  closeLoader()
  {
    swal.close()
  }

  showLoder()
 {
   swal.fire({
     text: 'Por favor espere un momento ...',
     imageUrl: '/assets/animation/Eclipse-1s-200px.svg',
     imageWidth: 100
   })
  }

  show(){
    this.spinner = true
  }

  hide(){
    this.spinner = false
  }
}
