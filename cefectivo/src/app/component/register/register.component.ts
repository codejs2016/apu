import { Component, OnInit, ViewChild, ElementRef, AfterViewInit } from '@angular/core';
import { FormGroup, FormBuilder, AbstractControl, Validators } from '@angular/forms';
import { WebsocketService } from 'src/app/services/websocket.service';
import { AlertController } from '@ionic/angular';
import { Socket } from 'ngx-socket-io';
import swal from 'sweetalert2';
import { ConfigService } from 'src/app/services/config-service';
import { CortesService } from 'src/app/services/cortes.service';
import { DataService } from 'src/app/services/data.service';
import { TipoService } from 'src/app/services/tipo.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit, AfterViewInit {

  public isinvalid : any
  public typecienmil : any
  public typecincuentamil : any
  public typeveintemil : any
  public typediezmil : any
  public typecincomil: any
  public typedosmil : any
  public typemil : any
  public typeServices: any
  public typeObs: any
  public submitted = false
  public submittednb = false
  public statusSum = false
  public showNoveda = false
  public spiner = false
  public billetes: any

  public suma = 0
  public sumachance = null
  public sumagiro = null
  public sumapagos = null
  public formCefectivo: FormGroup
  public tolamonedas: AbstractControl 
  public ciemil : AbstractControl 
  public cincuentamil : AbstractControl 
  public veintemil : AbstractControl 
  public diezmil : AbstractControl 
  public cincomil : AbstractControl 
  public dosmil : AbstractControl 
  public mil : AbstractControl 
  public totalefectivo: AbstractControl 
  public relacion: AbstractControl
  public observacion: AbstractControl
  public iduser: AbstractControl
  public obsnovedad: AbstractControl
  public numerobolsa: AbstractControl

  @ViewChild('tab100', {static: false} ) tab100: ElementRef;
  focusTab100(): void
  {
    this.tab100.nativeElement.focus()
    this.formCefectivo.get('ciemil').setValue('')
  }

  @ViewChild('tab50', {static: false} ) tab50: ElementRef;
  focusTab50(): void
  {
    this.tab50.nativeElement.focus()
  }

  @ViewChild('tab20', {static: false} ) tab20: ElementRef;
  focusTab20(): void
  {
    this.tab20.nativeElement.focus()
  }

  @ViewChild('tab10', {static: false} ) tab10: ElementRef;
  focusTab10(): void
  {
    this.tab10.nativeElement.focus()
  }

  @ViewChild('tab5', {static: false} ) tab5: ElementRef;
  focusTab5(): void
  {
    this.tab5.nativeElement.focus()
  }

  @ViewChild('tab2', {static: false} ) tab2: ElementRef;
  focusTab2(): void
  {
    this.tab2.nativeElement.focus()
  }

  @ViewChild('tab1', {static: false} ) tab1: ElementRef;
  focusTab1(): void
  {
    this.tab1.nativeElement.focus()
  }

  @ViewChild('tabM', {static: false} ) tabM: ElementRef;
  focusTabM(): void
  {
    this.tabM.nativeElement.focus()
  }

  constructor(
    private config: ConfigService,
    private dataService:DataService,
    private formBuilder: FormBuilder,
    private websocketService: WebsocketService,
    private tipoService: TipoService,
    private cortesService: CortesService,
    public alertController: AlertController,
    private socket: Socket) {
    this.setForm()
  }
  
  ngAfterViewInit() {
   this.focusTab100()
  }

  ngOnInit() 
  {
    this.init()
    this.onSuccessRegister()
 }

  setForm(){
    this.formCefectivo = this.formBuilder.group({
      territorio: [''],
      tolamonedas: [0, Validators.compose([Validators.required, Validators.min(1), Validators.maxLength(32767)]) ],
      ciemil: [0, Validators.compose([Validators.required, Validators.min(1), Validators.maxLength(32767)]) ],
      cincuentamil: [0, Validators.compose([Validators.required, Validators.min(1), Validators.maxLength(32767)]) ],
      veintemil: [0, Validators.compose([Validators.required, Validators.min(1), Validators.maxLength(32767)]) ],
      diezmil: [0, Validators.compose([Validators.required, Validators.min(1), Validators.maxLength(32767)]) ],
      cincomil: [0, Validators.compose([Validators.required, Validators.min(1), Validators.maxLength(32767)]) ],
      dosmil: [0, Validators.compose([Validators.required, Validators.min(1), Validators.maxLength(32767)]) ],
      mil: [0, Validators.compose([Validators.required, Validators.min(1), Validators.maxLength(32767)]) ],
      totalefectivo: [0, Validators.compose([Validators.required, Validators.min(1), Validators.maxLength(32767)]) ],
      relacion: [1, Validators.compose([Validators.required, Validators.min(1), Validators.maxLength(32767)]) ],
      observacion: [0],
      iduser: ['', Validators.compose([Validators.required, Validators.min(1), Validators.maxLength(32767)]) ],
      obsnovedad:[''],
      numerobolsa: ['', Validators.compose([Validators.required, Validators.pattern('^[A-Za-z0-9ñÑáéíóúÁÉÍÓÚ ]+$')])]
    })

    this.tolamonedas = this.formCefectivo.controls['tolamonedas']
    this.ciemil = this.formCefectivo.controls['ciemil']
    this.cincuentamil = this.formCefectivo.controls['cincuentamil']
    this.veintemil = this.formCefectivo.controls['veintemil']
    this.diezmil = this.formCefectivo.controls['diezmil']
    this.cincomil = this.formCefectivo.controls['cincomil']
    this.dosmil = this.formCefectivo.controls['dosmil']
    this.mil = this.formCefectivo.controls['mil']
    this.totalefectivo = this.formCefectivo.controls['totalefectivo']
    this.relacion = this.formCefectivo.controls['relacion']
    this.observacion = this.formCefectivo.controls['observacion']
    this.iduser = this.formCefectivo.controls['iduser']
    this.obsnovedad = this.formCefectivo.controls['obsnovedad']
    this.numerobolsa =  this.formCefectivo.controls['numerobolsa']
  }

  init(){
    
    this.getTipos()
    this.getObs()

    this.typecienmil = this.config.getConfig().typenumbers.ciemil
    this.typecincuentamil = this.config.getConfig().typenumbers.cincuentamil
    this.typeveintemil = this.config.getConfig().typenumbers.veintemil
    this.typediezmil = this.config.getConfig().typenumbers.diezmil
    this.typecincomil = this.config.getConfig().typenumbers.cincomil
    this.typedosmil = this.config.getConfig().typenumbers.dosmil
    this.typemil = this.config.getConfig().typenumbers.mil
  }

  getTipos(){
    this.tipoService.getTypes().subscribe( (res:any)=>{
      if(res.status==true){
        for(let i = 0; i < res.info.length; i++) {
           if(res.info[i].id == 3)
             res.info.splice(i,1)
           if(res.info[i].id == 4)
             res.info.splice(i,1)
        }
        this.typeServices = res.info
      }
    }, error =>{
      swal.fire("Aviso","Ocurrio un error al consultar tipos de servicios ", "warning")
    })
  }

  getObs(){
    this.tipoService.getObs().subscribe( (res:any)=>{
      if(res.status==true){
        for(let i = 0; i < res.info.length; i++) {
          if(res.info[i].id == 4)
            res.info.splice(i,1)
          if(res.info[i].id == 5)
            res.info.splice(i,1)
          if(res.info[i].id == 6)
            res.info.splice(i,1)
          if(res.info[i].id == 7)
            res.info.splice(i,1)
          if(res.info[i].id == 8)
            res.info.splice(i,1)
          if(res.info[i].id == 9)
            res.info.splice(i,1)
       }
       this.typeObs = res.info
      }
    }, error =>{
      swal.fire("Aviso","Ocurrio un error al consultar tipos de observaciones ", "warning")
    })
  }


  get f() {
    return this.formCefectivo.controls;
  }

  save()
  {
   if(this.formCefectivo.value.numerobolsa.length > 0){
      this.formCefectivo.get('iduser').setValue(this.dataService.getSessionUser().loginuser)
      this.formCefectivo.get('territorio').setValue(this.dataService.getSessionUser().territorio)
      if(this.suma > 0){
      this.formCefectivo.get('totalefectivo').setValue(this.suma)
      const swalWithBootstrapButtons = swal.mixin({
        customClass: {
          confirmButton: 'btn btn-success',
          cancelButton: 'btn btn-danger'
        },
        buttonsStyling: false
      })
        
      swalWithBootstrapButtons.fire({
        title: 'Está seguro de registrar efectivo ?',
        text:'Al confirmar se imprimira el recibo de este corte, recuerde cambiar a papeleria blanca.',
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Si',
        cancelButtonText: 'No',
        reverseButtons: true
      }).then((result) => {
      if (result.value)
      {
        this.showLoder()
        this.formCefectivo.value.totalefectivo = this.suma
        this.websocketService.sendEfectivo( this.formCefectivo.value )
        this.formCefectivo.reset()
        this.onGetCorte()
        this.suma = 0
        this.submitted = false
        this.setForm()
       }
      })
     }
     else{
      this.closeLoader()
      swal.fire("Aviso","Por favor ingresa alguna cantidad de billetes, para poder realizar registro de efectivo.", "warning")
     }

    }else{
       this.submittednb =  true
    }
  }

  showLoder()
  {
    swal.fire({
      text: 'Por favor espere un momento ...',
      imageUrl: '/assets/animation/Eclipse-1s-200px.svg',
      imageWidth: 100
    })
  }

  closeLoader()
  {
    swal.close()
  }

  
  clear(){
    this.setForm()
    this.suma = 0
    this.focusTab100()
  }
  
  onSuccessRegister() 
  {
    setTimeout(() => {
      this.cortesService.getRegister(this.dataService.getSessionUser().loginuser).subscribe( (res:any)=>{
        if(res.status){
           if(res.info.length > 0){
             this.sumagiro = parseInt(res.info[0].giros)
             this.sumachance = parseInt(res.info[1].chance)
             this.sumapagos = parseInt(res.info[2].pagos)
            }
         }
       }, error => {
          swal.fire("Aviso","Ocurrio un error al consultar efectivo", "warning")
       })
    }, 2000);
  }

  onGetCorte() 
  {
    //this.showLoder()
    setTimeout(() => {
      this.cortesService.getRegister(this.dataService.getSessionUser().loginuser).subscribe( (res:any)=>{
        if(res.status){
           if(res.info.length > 0){
             this.sumagiro = parseInt(res.info[0].giros)
             this.sumachance = parseInt(res.info[1].chance)
             this.sumapagos = parseInt(res.info[2].pagos)
             this.spiner = false
             setTimeout(() => {
              this.closeLoader()
             }, 4000)
             swal.fire("Registro","Corte registrado satisfactoriamente", "success")
            }
         }
       }, error => {
          this.closeLoader()
          swal.fire("Aviso","Ocurrio un error al consultar efectivo", "warning")
       })
    }, 1000);
  }


  onSuccessSums() 
  {
    this.websocketService.getSums({ iduser:1 })
    this.socket.on('getSums', (data: any) => {
       if(data.idUser == 1){
         this.sumagiro = parseInt(data.dataTransport[0].giros)
         this.sumachance = parseInt(data.dataTransport[1].chance)
         this.sumapagos = parseInt(data.dataTransport[2].pagos)
       }
    })
  }

  validateNumberLetter(event: any){
    var regex = /^[A-Za-z0-9]+$/
    var isValid = regex.test(event.target.value)
    if(isValid == false){
      event.target.value = ''
    }
  }
  

  
  validateNumber(event: any, type: number)
  { 
    event.target.value = (event.target.value + '').replace(/[^0-9]/g, '')
    if(parseInt(event.target.value) > 0)
    {
      
      let suma = 0
      if(this.formCefectivo.controls.ciemil.value.length > 0)
        suma += parseInt(this.formCefectivo.controls.ciemil.value) * 100000
      
      if(this.formCefectivo.controls.diezmil.value.length > 0)
        suma += parseInt(this.formCefectivo.controls.diezmil.value) * 10000
      
      if(this.formCefectivo.controls.cincuentamil.value.length > 0)
        suma += parseInt(this.formCefectivo.controls.cincuentamil.value) * 50000
      
      if(this.formCefectivo.controls.cincomil.value.length > 0)
        suma += parseInt(this.formCefectivo.controls.cincomil.value) * 5000
      
      if(this.formCefectivo.controls.veintemil.value.length > 0)
        suma += parseInt(this.formCefectivo.controls.veintemil.value) * 20000
      
      if(this.formCefectivo.controls.dosmil.value.length > 0)
        suma += parseInt(this.formCefectivo.controls.dosmil.value) * 2000
      
      if(this.formCefectivo.controls.mil.value.length > 0)
        suma += parseInt(this.formCefectivo.controls.mil.value) * 1000
      
      if(this.formCefectivo.controls.tolamonedas.value.length > 0)
        suma += parseInt(this.formCefectivo.controls.tolamonedas.value) 
      
      this.suma = suma
    }

    if(event.keyCode == 13){
      switch(type){  
        case 100000:
          this.focusTab10()
          this.formCefectivo.get('diezmil').setValue('')
        break;
        case 50000: 
          this.focusTab5()
          this.formCefectivo.get('cincomil').setValue('')
        break;
        case 20000: 
          this.focusTab2()
          this.formCefectivo.get('dosmil').setValue('')
        break;
        case 10000: 
          this.focusTab50()
          this.formCefectivo.get('cincuentamil').setValue('')
        break;
        case 5000: 
         this.focusTab20()
         this.formCefectivo.get('veintemil').setValue('')
        break;
        case 2000: 
         this.focusTab1()
         this.formCefectivo.get('mil').setValue('')
        break;
        case 1000: 
         this.focusTabM()
         this.formCefectivo.get('tolamonedas').setValue('')
        break;
      }
    }
  }

  getOb(obs:any){
    switch (obs) {
       case "1":
       case "2":
       case "3":
          this.showNoveda = true
       break;
     default:
         this.showNoveda = false
       break;
   }
  }

}
