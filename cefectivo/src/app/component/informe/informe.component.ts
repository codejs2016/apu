import { Component, OnInit } from '@angular/core';
import { FormGroup, AbstractControl, Validators, FormBuilder } from '@angular/forms';
import { NgbDate, NgbCalendar, NgbDateParserFormatter, NgbModal, ModalDismissReasons, NgbModalRef, NgbDatepickerI18n } from '@ng-bootstrap/ng-bootstrap';
import swal from 'sweetalert2';
import { TesoreriaService } from 'src/app/services/tesoreria.service';
import { TipoService } from 'src/app/services/tipo.service';
import { WebsocketService } from 'src/app/services/websocket.service';
import { DataService } from 'src/app/services/data.service';
import { AuthService } from 'src/app/services/auth.service';
import { ExcelService } from 'src/app/services/excel.service';

@Component({
  selector: 'app-informe',
  templateUrl: './informe.component.html',
  styleUrls: ['./informe.component.css']
})
export class InformeComponent implements OnInit {

  public spinner = false
  public submitted = false
  public tblc = false
  public tble = false
  public tblr = false
  public tbls = false
  public tblv = false
  public pg = false
  public typeRP = 0

  public allCortes: any
  public allPaquetes: any
  public allRGD: any
  public allSG : any
  public allVRF : any
  public obsVerificacion: number
  public suma = 0
  public opc = 0
  public closeResult: any
  public idCorte: any
  public info : any
  public typeServices: any
  public formR: FormGroup
  public valorverificacion: AbstractControl
 
  public fromDate: any;
  public toDate: any;
  public type = 'Ingrese CV'
  public filter : any

  public p: number = 1
  public formRegister: FormGroup

  public documento: AbstractControl
  public finicial: AbstractControl
  public ffinal: AbstractControl
  public estado: AbstractControl
 
  constructor(
    private formBuilder: FormBuilder,
    private calendar: NgbCalendar,
    public formatter: NgbDateParserFormatter,
    private tesoreriaService: TesoreriaService,
    public dataService: DataService,
    public authService: AuthService,
    private excelService :ExcelService,
    private modalService: NgbModal) {
    this.formRegister = this.formBuilder.group({
      documento: [''],
      estado: [1],
      finicial: [''],
      ffinal: ['']
    })

    this.documento = this.formRegister.controls['documento']
    this.estado = this.formRegister.controls['estado']
    this.finicial = this.formRegister.controls['finicial']
    this.ffinal = this.formRegister.controls['ffinal']

    this.formR = this.formBuilder.group({
      valorverificacion: ['', Validators.required]
    })
    this.valorverificacion = this.formR.controls['valorverificacion']
  }

  ngOnInit() {
    this.formRegister.get('estado').setValue(1)
  }

  typeReport(type: number){
    switch (type) {
      case 1:
       this.exportToExcel(this.allCortes,"cortes", type)
      break
      case 2:
       this.exportToExcel(this.allPaquetes,"entregatulas", type )
      break
      case 3:
        this.exportToExcel(this.allRGD,"detalletulacandado", type )
       break
      case 4:
       this.exportToExcel(this.allSG,"seguridadnovendad",type )
      break
      case 5:
       this.exportToExcel(this.allVRF,"verificacion",type )
      break
    }
  }

  exportToExcel(data: any, nameFile: string, type: number) {
    this.excelService.excelDownload(data, nameFile, type )
  }

  get f() {
    return this.formRegister.controls;
  }

  get fv() {
    return this.formR.controls;
  }

  hide(){
    this.tblc = false
    this.tble = false
    this.tblr = false
    this.tbls = false
    this.tblv = false
  }

  getType(){
     this.hide()
     this.formRegister.get('documento').setValue(null)
     this.formRegister.get('estado').setValue(parseInt(this.filter))
     

     this.typeRP = parseInt(this.filter)
     switch (parseInt(this.filter)) {
         case 1:
         case 5:
           this.type = 'Ingrese CV'
         break
         case 2:
         case 3:
         case 4:
           this.type = 'Ingrese CP'
         break
     }
  }

  validateInput(currentValue: NgbDate, input: string): NgbDate {
    const parsed = this.formatter.parse(input);
    return parsed && this.calendar.isValid(NgbDate.from(parsed)) ? NgbDate.from(parsed) : currentValue;
  }

  cancelar(){
    this.formRegister.reset();
  }

  search() {

    //if(parseInt(this.filter) > 0){
        this.submitted = true
        if(this.formRegister.invalid)
          return false;

        if (typeof (this.fromDate) === 'object') {
          this.formRegister.value.finicial =
          this.fromDate.day + "/" + this.fromDate.month + "/" +this.fromDate.year
        }
        else
          this.formRegister.value.finicial = ""
      

        if (typeof (this.toDate) === 'object') {
          this.formRegister.value.ffinal = 
          this.toDate.day + "/" + this.toDate.month + "/" + this.toDate.year
        }
        else{
          this.formRegister.value.ffinal = ""
        }
        this.getNovedades()
    //}
    //else
     //swal.fire("","Por favor seleccione un tipo de novedad ","warning")
    
  }

  getNovedades() {
    this.spinner = true
    this.tesoreriaService.novedades(this.formRegister.value).subscribe((res: any) => {
      if (res.status) {
       this.submitted = false
       this.spinner = false

       switch(this.filter)
       {
          case 1:
            this.hide()
            this.tblc = true
            for(let i = 0; i < res.info.length; i++) 
            {
              res.info[i].corte  = 0
              res.info[i].corte += i+1 
              this.suma += res.info[i].totalefectivo << 0
            }
            if(res.info.length > 0)
              this.pg = true
            else
              this.pg = false

            this.allCortes = res.info 
          break;

          case 2:
            this.hide()
            this.tble = true
            for(let i = 0; i < res.info.length; i++) 
            {
              res.info[i].corte  = 0
              res.info[i].corte += i+1 
              this.suma += res.info[i].cantidad << 0
            }
            if(res.info.length > 0)
              this.pg = true
            else
              this.pg = false

            this.allPaquetes = res.info 
          break;

          case 3:
            this.hide()
            this.tblr = true
            for(let i = 0; i < res.info.length; i++) 
            {
              this.suma += res.info[i].cantidad << 0
            }
            if(res.info.length > 0)
              this.pg = true
            else
              this.pg = false

            this.allRGD = res.info 
          break;

          case 4:
            this.hide()
            this.tbls = true
            for(let i = 0; i < res.info.length; i++) 
            {
              this.suma += res.info[i].cantidad << 0
              if(res.info[i].tipocambio == 1 ){
                res.info[i].tipocambio = 'CAMBIO PLACA'
              }
              if(res.info[i].tipocambio == 2 ){
                res.info[i].tipocambio = 'CAMBIO RUTA'
              }
              if(res.info[i].tipocambio == 3 ){
                res.info[i].tipocambio = 'CAMBIO ROL'
              }
              if(res.info[i].tipocambio == 4 ){
                res.info[i].tipocambio = 'CAMBIO PLACA Y RUTA'
              }
              if(res.info[i].tipocambio == 5 ){
                res.info[i].tipocambio = 'CAMBIO PLACA Y ROL'
              }
              if(res.info[i].tipocambio == 6 ){
                res.info[i].tipocambio = 'CAMBIO RUTA Y ROL'
              }
              if(res.info[i].tipocambio == 7 ){
                res.info[i].tipocambio = 'CAMBIO PLACA, RUTA Y ROL'
              }
            }

            if(res.info.length > 0)
              this.pg = true
            else
              this.pg = false

            this.allSG = res.info 
          break;

          case 5:
            this.hide()
            this.tblv = true
            for(let i = 0; i < res.info.length; i++) 
            {
              this.suma += res.info[i].totalefectivo << 0
            }

            if(res.info.length > 0)
              this.pg = true
            else
              this.pg = false

            this.allVRF = res.info 
          break;
       }
       
      }
      else{
        swal.fire("Aviso!", res.msg, "warning")
        this.spinner = false
        this.allCortes = null
        this.allPaquetes = null
        this.allRGD = null
        this.allVRF = null
        this.allSG = null
      }
    },
      error => {
        swal.fire("Aviso!", "Ocurrio un error al consultar novedades ", "warning")
        this.spinner = false
        this.allCortes = null
      })
  }

  onCloseModal() {
    this.modalService.dismissAll();
  }

  showLoder()
  {
    swal.fire({
      text: 'Por favor espere un momento ...',
      imageUrl: '/assets/animation/Eclipse-1s-200px.svg',
      imageWidth: 100
    })
  }

  closeLoader()
  {
    swal.close()
  }

  clear() {
    this.formR.reset()
    this.formRegister.get('finicial').setValue(null)
    this.formRegister.get('ffinal').setValue(null)
    this.formRegister.get('documento').setValue(null)
    this.hide()
  }
}



