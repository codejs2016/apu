import { Component, OnInit } from '@angular/core';
import { SemaforoService } from 'src/app/services/semaforo.service';
import swal from 'sweetalert2';
import { AbstractControl, FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ConfigService } from 'src/app/services/config-service';

@Component({
  selector: 'app-configptv',
  templateUrl: './configptv.component.html',
  styleUrls: ['./configptv.component.css']
})
export class ConfigptvComponent implements OnInit {

  public p: number = 1
  public puntos: any
  public msgError: any
  public ccostoslist: any;

  public formR: FormGroup
  public nombre: AbstractControl
  public territorio: AbstractControl
  public ccosto:  AbstractControl
  public tipo:  AbstractControl
  
  
  public showLoader = false
  public submitted = false
  public showError = false
  public showContentAll = false
  public showformTable = false
  public showform = false
  public showEditTalla = false
  public ptvs: any
  public opc = 0

  public listCtg: any
  public getDetalle : string
  public presenTation: any
  public cntTalla : any
  public allTallasTraslados: any
  public newTalla : any
  public tallasData = []

  constructor(
    private semaforoService: SemaforoService,
    private formBuilder: FormBuilder,
    private config: ConfigService,
    ) { 

      this.formR = this.formBuilder.group({
        _id: [''],
        nombre: ['', Validators.required ],
        territorio: ['', Validators.required ],
        ccosto: ['1078', Validators.required ],
        tipo: ['A', Validators.required ]
      })
      this.nombre = this.formR.controls['nombre']
      this.territorio = this.formR.controls['territorio']
      this.ccosto = this.formR.controls['ccosto']
      this.tipo = this.formR.controls['tipo']
  }

  ngOnInit(){
    this.ccostoslist = this.config.getConfig().ccostos
  }

  get f() {
    return this.formR.controls;
  }

  showLoder()
  {
    swal.fire({
      text: 'Por favor espere un momento ...',
      imageUrl: '/assets/animation/Eclipse-1s-200px.svg',
      imageWidth: 100,
    })
  }

  closeLoader()
  {
    swal.close()
  }


  get()
  {
    this.showLoder()
    this.semaforoService.get().subscribe( (res:any) =>{
      this.puntos = res
      this.showContentAll = true
      this.closeLoader()
    },
    error =>{
      this.closeLoader()
      swal.fire("Aviso","Ocurrio un error al consultar los puntos de ventas", "warning")
    })
  }

  showHide(){
    this.showformTable = false
    this.showform = true
  }

  search(event:any){
    this.show()
    this.semaforoService.search({ name: event.target.value }).subscribe( (res:any)=>{
       if(res.status == true){
         this.showform = false
         this.showformTable = true
         this.puntos = res.puntos
         this.hide()
       }
    }, error =>{
      this.msgError = "Ocurrio un error al consultar punto de venta"
      this.showError = true
      this.hide()
    })
   }

   show(){
     this.showLoader = true
   }

   hide(){
    this.showLoader = 
    false
   }

   validateNumber(event: any)
   { 
    event.target.value = (event.target.value + '').replace(/[^0-9]/g, '')
   }

   save()
   {
    this.submitted = true;
    if(this.formR.invalid)
     return false;
    
    const swalWithBootstrapButtons = swal.mixin({
      customClass: {
        confirmButton: 'btn btn-success',
        cancelButton: 'btn btn-danger'
      },
      buttonsStyling: false
    })
      
    swalWithBootstrapButtons.fire({
      text: 'Está seguro de registrar punto de venta?',
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Si',
      cancelButtonText: 'No',
      reverseButtons: true
    }).then((result) => {
    if (result.value)
    {
      this.showLoder()
      this.semaforoService.save(this.formR.value).subscribe( (res:any) =>{
       if(res.status){
         this.closeLoader()
         swal.fire("Aviso","Punto de Venta registrado satisfactoriamente", "success")
       }
      }, error =>{
        swal.fire("Aviso","Ocurrio un error al consultar los puntos de ventas", "warning")
      })
      this.formR.reset()
      this.submitted = false
     }
    })
   }

   detalle(item:any){
    for (let i = 0; i < this.puntos.length; i++) {
      this.puntos[i].btn = false;
      if(this.puntos[i].territorio == item.territorio)
      {
        this.puntos[i].status = false;
        this.puntos[i].btn = true;
      }
    }
   }

   getTipo(tipo:any, territorio:number){
      for (let i = 0; i < this.puntos.length; i++) {
        if(this.puntos[i].territorio == territorio)
        {
          this.puntos[i].tipo = tipo;
        }
    }
  }

  updatePunto(item:any){
   const swalWithBootstrapButtons = swal.mixin({
    customClass: {
      confirmButton: 'btn btn-success',
      cancelButton: 'btn btn-danger'
    },
    buttonsStyling: false
  })
    
  swalWithBootstrapButtons.fire({
    text: 'Está seguro de cambiar el tipo de recogida para este punto de venta?',
    type: 'warning',
    showCancelButton: true,
    confirmButtonColor: '#3085d6',
    cancelButtonColor: '#d33',
    confirmButtonText: 'Si',
    cancelButtonText: 'No',
    reverseButtons: true
  }).then((result) => {
  if (result.value)
  {
    this.showLoder()
    this.semaforoService.update(item).subscribe( (res:any) =>{
     if(res.status){
       this.closeLoader()
       swal.fire("Aviso","Punto de Venta actualizado satisfactoriamente", "success")
     }
    }, error =>{
      this.closeLoader()
      swal.fire("Aviso","Ocurrio un error al actualizar puntos de venta", "warning")
    })
   }
  })
 }

}
