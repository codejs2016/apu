import { Component, OnInit } from '@angular/core';
import { ConfigService } from 'src/app/services/config-service';
import { LoginService } from 'src/app/services/login.service';
import $ from "jquery";
import { AuthService } from 'src/app/services/auth.service';
import { DataService } from 'src/app/services/data.service';

@Component({
  selector: 'app-asesora',
  templateUrl: './asesora.component.html',
  styleUrls: ['./asesora.component.css']
})
export class AsesoraComponent implements OnInit {

  public listModules: any

  constructor(private config: ConfigService,
              private loginService: LoginService,
              public authService: AuthService,
              public dataService:DataService)
  {}
  
  ngOnInit(){
    this.getListModules()
  }
  
  getListModules()
  {
    this.listModules = this.config.getConfig().modulos
  }
  
}
