import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { LoginService } from '../services/login.service';
import { ConfigService } from '../services/config-service';
import { take, map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class AdminGuard implements CanActivate {

  constructor(private loginService: LoginService, 
    private router: Router,
    private configuration: ConfigService
){ }

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
    return this.loginService.isLoggedIn(sessionStorage.getItem('token'))
    .pipe(take(1))
    .pipe(map( (res:any) => {
    if(res.status==true){
      return true
    }
    else
     this.router.navigate(['login'])
  }));
  }
}
