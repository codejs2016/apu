import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { WebsocketService } from './services/websocket.service';
import { HttpClientModule } from '@angular/common/http';
import { SocketIoConfig, SocketIoModule } from 'ngx-socket-io';
import { configuration } from './configuration/configuration';
import { RegisterComponent } from './component/register/register.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CortesComponent } from './component/cortes/cortes.component';
import { PagosComponent } from './component/pagos/pagos.component';
import { ControlcajaComponent } from './component/controlcaja/controlcaja.component';
import { CortesService } from './services/cortes.service';
import { LoginComponent } from './component/login/login.component';
import { AsesoraComponent } from './component/asesora/asesora.component';
import { AdminComponent } from './component/admin/admin.component';
import { GuardasComponent } from './component/guardas/guardas.component';
import { ProgramacionComponent } from './component/programacion/programacion.component';
import { PtvsComponent } from './component/ptvs/ptvs.component';
import { RecogidasComponent } from './component/recogidas/recogidas.component';



import { ReccogidapuntoComponent } from './component/reccogidapunto/reccogidapunto.component';
import { EntregaefectivoComponent } from './component/entregaefectivo/entregaefectivo.component';
import { RtesoreriaComponent } from './component/rtesoreria/rtesoreria.component';
import { TesoreriaComponent } from './component/tesoreria/tesoreria.component';
import { SemaforoComponent } from './component/semaforo/semaforo.component';
import { ConfigptvComponent } from './component/configptv/configptv.component';
import { VehiculosComponent } from './component/vehiculos/vehiculos.component';
import { NgxPaginationModule } from 'ngx-pagination';
import { PlanillaComponent } from './component/planilla/planilla.component';
import { CentroefectivoComponent } from './component/centroefectivo/centroefectivo.component';
import { CentroefectivorecibeComponent } from './component/centroefectivorecibe/centroefectivorecibe.component';
import { RangosComponent } from './component/rangos/rangos.component';
import { InformeComponent } from './component/informe/informe.component';

const configSocket: SocketIoConfig = { url: configuration.bussinesServer.url , options: {} };

@NgModule({
  declarations: [
    AppComponent,
    RegisterComponent,
    CortesComponent,
    PagosComponent,
    ControlcajaComponent,
    LoginComponent,
    AsesoraComponent,
    AdminComponent,
    GuardasComponent,
    ProgramacionComponent,
    PtvsComponent,
    RecogidasComponent,
    ReccogidapuntoComponent,
    TesoreriaComponent,
    EntregaefectivoComponent,
    RtesoreriaComponent,
    SemaforoComponent,
    ConfigptvComponent,
    VehiculosComponent,
    PlanillaComponent,
    CentroefectivoComponent,
    CentroefectivorecibeComponent,
    RangosComponent,
    InformeComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    NgbModule,
    FormsModule,
    ReactiveFormsModule,
    NgxPaginationModule,
    SocketIoModule.forRoot(configSocket),
  ],
  providers: [
    WebsocketService,
    CortesService,
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
