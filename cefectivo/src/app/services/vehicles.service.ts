import { Injectable } from '@angular/core';
import { HttpClient } from "@angular/common/http";
import { ConfigService } from "./config-service";

@Injectable({
  providedIn: 'root'
})
export class VehiclesService {

  readonly URL_API = this.config.getConfig().bussinesServer.url;

  constructor(private http: HttpClient, private config: ConfigService) {}
  
  searchs(name: any) {
    return this.http.post(this.URL_API + "/api/vehiculo/search", name);
  }

  register(data: any) {
    return this.http.post(this.URL_API + "/api/vehiculo", data);
  }

  update(data: any) {
    return this.http.post(this.URL_API + "/api/vehiculo/update",data);
  }

  delete(data: any) {
    return this.http.post(this.URL_API + "/api/vehiculo/delete",data);
  }
}
