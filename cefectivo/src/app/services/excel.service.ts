import { Injectable } from '@angular/core';
import { Papa } from 'ngx-papaparse';
import * as moment from 'moment';

@Injectable({
  providedIn: 'root'
})
export class ExcelService {

  constructor(private papa: Papa) { }

  excelDownload(data: any, nameFile: string, type: number): void {
    const excelObject =  this.mapData(data, type)
    const csv = this.papa.unparse({ data: excelObject });
    
    const csvData = new Blob([csv], { type: 'text/csv;charset=utf-8;' });

    let csvURL = null;

    if (navigator.msSaveBlob) {
      csvURL = navigator.msSaveBlob(csvData, 'export.xlsx');
    } else {
      csvURL = window.URL.createObjectURL(csvData);
    }

    // For Browser
    const tempLink = document.createElement('a');
    tempLink.href = csvURL;
    tempLink.setAttribute('download', `${nameFile}.xlsx`)
    tempLink.click();
  }

  private mapData(data: any, type: number) {
    switch (type) {
        case 1:
            return data.map((item:any) => {
                return {
                  Corte: item.corte,
                  Fecha: moment(item.fecha).format('DD-MM-YYYY'),
                  Hora: item.hora,
                  Novedad: item.novedad,
                  "Numero de Bolsa": item.numerobolsa,
                  Observacion: item.observacion,
                  "Punto de Venta": item.puntoventa,
                  "$ Total Efectivo": item.totalefectivo,
                  "$ Verificación": item.valorverficacion
                }
            })
        break

        case 2:
            return data.map((item:any) => {
                return {
                  "Cantidad Tulas": item.cantidad,
                  Fecha: moment(item.fecha).format('DD-MM-YYYY'),
                  Hora: item.hora,
                  "Tipo Observación": item.tipo,
                  Nota: item.nota
                }
           })
        break;


        case 3:
          return data.map((item:any) => {
              return {
                "Punto": item.punto,
                Candado: item.candado,
                Tula: item.tula,
                Fecha: moment(item.fecha).format('DD-MM-YYYY'),
                Hora: item.hora,
                "Tipo Observación": item.novedad,
              }
         })
        break;

        case 4:
            return data.map((item:any) => {
                return {
                  "Documento Guarda": item.documento,
                  "Ruta": item.ruta,
                  "Placa": item.placa,
                  "bservación": item.obs,
                  Fecha: moment(item.fecha).format('DD-MM-YYYY'),
                  "Tipo de Cambio": item.tipocambio
                }
           })
        break;

        case 5:
           
            return data.map((item:any) => {
                return {
                  "$ 100.000": item.cienmil,
                  "$ 50.000": item.cincuentamil,
                  "$ 20.000": item.veintemil ,
                  "$ 10.000": item.diezmil,
                  "$ 5.000": item.cincomil,
                  "$ 2.000": item.dosmil,
                  "$ 1.000": item.mil,
                  "Monedas": item.totalmonedas,
                  "Total Efectivo": item.totalefectivo,
                  "$ Verificación": item.valorverficacion,
                  " # Bolsa": item.numerobolsa,
                  "Observación": item.observacion,
                  Fecha: moment(item.fecha).format('DD-MM-YYYY')
                }
           })
        break;
    
    }
  }
}

