import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ConfigService } from './config-service';

@Injectable({
  providedIn: 'root'
})
export class SemaforoService {

  readonly URL_API = this.config.getConfig().bussinesServer.url
  constructor(
    private http: HttpClient, 
    private config:ConfigService)
  {}
   
  get()
  {
    return this.http.get(this.URL_API+"/api/semaforo")
  }

  getSemaforo()
  {
    return this.http.get(this.URL_API+"/api/semaforo/semaforo")
  }

  getRangos()
  {
    return this.http.get(this.URL_API+"/api/semaforo/rangos")
  }

  updateRangos(data: any)
  {
    return this.http.post(this.URL_API+"/api/semaforo/rangos", data)
  }

  save(data: any)
  {
    return this.http.post(this.URL_API+"/api/semaforo",data)
  }

  search(data: any)
  {
    return this.http.post(this.URL_API+"/api/semaforo/search",data)
  }

  update(data: any)
  {
    return this.http.post(this.URL_API+"/api/semaforo/update",data)
  }
}


