import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class DataService {

  public storage: any;
  public cart: any;
  public products: any;
  public company: any;
  public tw: boolean;
  public cntsedes: any
  public disable = null
  public placa : any
  public cacheSession: any
  public cacheTokenSession: any
  public cacheSessionUser: any

  public sumachance = 0
  public sumagiro = 0
  public sumapagos = 0

  constructor() { }

   getSessionUser(){
    return this.cacheSessionUser = {
      token:sessionStorage.getItem('token'),
      loginuser:sessionStorage.getItem('loginuser'),
      territorio:sessionStorage.getItem('territorio'),
      username:sessionStorage.getItem('username'),
      tokensession:sessionStorage.getItem('tokensession'),
      role:sessionStorage.getItem('role')
    }
  }

  removeSessions()
  {
    sessionStorage.removeItem('token')
    sessionStorage.removeItem('loginuser')
    sessionStorage.removeItem('territorio')
    sessionStorage.removeItem('username')
    sessionStorage.removeItem('tokensession')
    sessionStorage.removeItem('role')
  }
}
