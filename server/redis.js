const Redis = require('ioredis');
const redis = new Redis();
 
const JSONCache = require('redis-json');
 
const jsonCache = new JSONCache(redis, {prefix: 'cache:'});
 
const user = {
  name: 'redis-json',
  age: 25,
  address: {
    doorNo: '12B',
    locality: 'pentagon',
    pincode: 123456
  },
  cars: ['BMW 520i', 'Audo A8']
}
 

 
jsonCache.set('123', user)
async function response(){
    let res = await  jsonCache.get('123')
    console.log(res)
}

response()