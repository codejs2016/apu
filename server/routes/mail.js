"use strict";
const path = require('path');
const nodemailer = require('nodemailer');
const smtpTransport = require('nodemailer-smtp-transport');
const config = require('../configuration');
const transporter = nodemailer.createTransport(smtpTransport({
  service: 'gmail',
  host: 'smtp.gmail.com',
  auth: {
    user: config.toMail,
    pass: config.passwordMail
  }
}));

exports.dobleAutenticacion = async function(data)
{
  const htmlCustom = `<div style="width:auto;height:auto; background:#4466f4; color:white; padding:30px; margin:15px 0;text-align: center;-webkit-transition: background-image 0.3s ease-out;-moz-transition: background-image 0.3s ease-out;-o-transition: background-image 0.3s ease-out;transition: background-image 0.3s ease-out;box-shadow: 0px 0px 43px 0px rgba(131, 131, 131, 0.23);"><img style="width:120px;" src="https://inverpacifico.com.co/wp-content/uploads/2013/10/logo.png"><p>ALERTA DE SEGURIDAD, ALGUIEN HA INTENTADO INICIAR SESIÓN EN OTRO DISPOSITIVO. <br>
  'Usuario (a): ${data.username} <br>
   Login User: ${data.loginuser} <br> 
   Territorio: ${data.territorio} <br>
   Rol: ${data.role} <br>'  
  </p><a style="text-decoration: none;font-size: 14px;padding: 5px 14px;border-radius: 30px;font-family: sans-serif;letter-spacing: 1px;border: none; text-transform: uppercase;color: #e9c05e;">SEGUIMIENTO REALIZADO POR TI</a></div><div style="margin-bottom:20px;width:100%;vertical-align: bottom;padding-bottom: 30px;background-color: #f7f7f7;"><img src="https://inverpacifico.com.co/wp-content/uploads/2013/10/logo.png" style="width:57px;"><br>Calle 3 Nº 5 - 40 · Buenventura Valle<br> Centro de ayuda · Política de privacidad · Términos y condiciones<br>  </div></div>`

  var mailOptions = {
   from: config.toMail,
   to: 'codebydevs@gmail.com',//config.fromEmail,
   subject: 'NOTIFICACIÓN DE DOBLE AUTENTICACIÓN',
   html: htmlCustom,
 };

 transporter.sendMail(mailOptions, function(error, info){
  if (error) {
    console.log(error);
  } else {
    console.log('Email sent: ' + info.response);
  }
 })
}

