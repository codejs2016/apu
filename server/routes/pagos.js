const express = require('express');
const router = express.Router();
const dbConfig = require('../keys');
const config = require('../configuration');
const oracledb = require('oracledb');

var data = []

router.get('/:id', async (req, res) => {
  let connection, sql, result, dateNow
  try {
      connection = await oracledb.getConnection(dbConfig);
      sql = `SELECT * FROM appweb.ce_pagoacumulado WHERE FECHA=TO_DATE(SYSDATE,'DD/MM/RRRR')
             AND CEDULA='${req.params.id}'`
      result = await connection.execute(sql);
      let info = []
      if (result.rows.length > 0) {
          for(let i = 0; i < result.rows.length; i++) {
              let objson = {}
              objson.serie = result.rows[i][1]
              objson.numero = result.rows[i][2]
              objson.valor = result.rows[i][3]
              objson.hora = result.rows[i][6]
              objson.estado = result.rows[i][7]
              info.push(objson)
          }
          res.json({ status: true,  info }); 
      }
      else
        res.json({ status: false, msg: config.notfoudPays })
  }
  catch (err) {
      res.status(500).json(err)
  } finally {
      if (connection) {
          try {
              await connection.close();
          }
          catch (err) {
              res.status(500).json(err)
          }
      }
  }
})

router.post('/', async (req, res) => {
    let connection, sql, result;
    try {
         connection = await oracledb.getConnection(dbConfig);
         sql = `SELECT dp.serie, dp.numero, dp.fecha, dp.totalpremio, dp.estadopremio, d.nombre estado, dp.fechaestado
         FROM gamble.detallepremios@chile dp, datos d WHERE dp.serie='${req.body.serie.toUpperCase()}' AND dp.numero='${req.body.numero}'
         AND dp.estadopremio = d.codigo AND dp.estadopremio = 
         (SELECT  e.dat_dto_codigo  FROM estadopremios@chile e
         WHERE e.prmfor_frm_numero = '${req.body.numero}' AND
         e.prmfor_frm_serie='${req.body.serie.toUpperCase()}' AND e.fechafinal IS NULL)`
         result = await connection.execute(sql);
        if (result.rows.length > 0) {
            let login = {};
            result.rows.forEach(function (lgRow) {
                login.serie = lgRow[0];
                login.numero = lgRow[1];
                login.fecha = lgRow[2];
                login.totalpremio = lgRow[3];
                login.estadopremio = lgRow[4];
                login.estado = lgRow[5];
                login.fechaestado = lgRow[6];
            });
            res.json({ status: true, data:login });
        } 
        else
          res.json({ status: false, msg: config.notfoudPays });
    }
    catch (err) {
        res.status(500).json(err)
    } finally {
        if (connection) {
            try {
                await connection.close();
            }
            catch (err) {
                res.status(500).json(err)
            }
        }
    }
})

router.post('/save', async (req, res) => 
{
  let connection, sql, dateNow, result, fecha = new Date(), hora
  try 
  {
    connection = await oracledb.getConnection(dbConfig);
    sql =  `SELECT * FROM  appweb.ce_pagoacumulado WHERE serie='${req.body.serie}' AND numero='${req.body.numero}'`
    result = await connection.execute(sql)
    if(result.rows.length > 0){
      res.json({ status: true, msg:config.duplicado }); 
    }
    else
    {
     sql = `SELECT TO_CHAR (SYSDATE, 'DD/MM/RRRR') "NOW" FROM DUAL`
     result = await connection.execute(sql);
     dateNow = result.rows[0]
     hora = fecha.getHours()+":"+fecha.getMinutes()+":"+fecha.getSeconds();
     result = await connection.execute(
        `INSERT INTO appweb.ce_pagoacumulado VALUES (:id_pagoacumulado,:cedula,:serie,:numero,:valor,:fecha,:hora,:estado)`,
         [ req.body.cedula, req.body.serie, parseInt(req.body.numero), req.body.totalpremio, 
          dateNow.toString(), null, hora, 'pagado'],
         { autoCommit: true } 
     );
     if(result.rowsAffected == 1){
      res.json({ status: true, msg:config.success });
     }
     else
      res.json({ status: false, msg:config.error });
    }
  } 
  catch (err) {
    res.status(500).json(err)
  } finally {
    if (connection) {
      try {
        await connection.close();
      } 
     catch (err) {
      res.status(500).json(err)
     }
  }
 }
})

module.exports = router;