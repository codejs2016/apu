const express = require('express');
const router = express.Router();
const dbConfig = require('../keys');
const config = require('../configuration');
const oracledb = require('oracledb');

router.post('/', async (req, res) => 
{
  let connection = await oracledb.getConnection(dbConfig);
  try 
  {    result = await connection.execute(`INSERT INTO 
        APPWEB.CE_SEGURIDADVEHICULO VALUES (
        :ID_SEGURIDADVEHICULO,
        :PLACA)`,
      [ null, req.body.placa ], { autoCommit: true } );
      if(result.rowsAffected == 1){
        res.json({ status: true, msg:config.success });
      }
  } 
  catch (err) {
      console.log(err)
    res.status(500).json(err)
  } finally {
    if (connection) {
      try {
        await connection.close();
      } 
     catch (err) {
        console.log(err)
      res.status(500).json(err)
     }
  }
 }
})


router.post('/update', async (req, res) => 
{
  console.log( "update ", req.body )
  let connection = await oracledb.getConnection(dbConfig);
  try 
  {   
      sql = `UPDATE APPWEB.CE_SEGURIDADVEHICULO SET
                PLACA=:placa
                WHERE ID_SEGURIDADVEHICULO=:id`;
      await connection.execute(sql, 
          { placa:req.body.placa, id:req.body.id},
          { autoCommit: true}); 
     res.json({ status: true, msg:config.success });
  } 
  catch (err) {
      console.log(err)
    res.status(500).json(err)
  } finally {
    if (connection) {
      try {
        await connection.close();
      } 
     catch (err) {
        console.log(err)
      res.status(500).json(err)
     }
  }
 }
})


router.post('/delete', async (req, res) => 
{
  let connection = await oracledb.getConnection(dbConfig)
   sql = `DELETE FROM APPWEB.CE_SEGURIDADVEHICULO
          WHERE ID_SEGURIDADVEHICULO=:id`;
   await connection.execute(sql, 
      { id:req.body.id},
      { autoCommit: true})
   res.json({ status: true })
})


module.exports = router;

