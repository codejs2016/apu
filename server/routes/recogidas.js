const express = require('express');
const router = express.Router();
const dbConfig = require('../keys');
const config = require('../configuration');
const oracledb = require('oracledb');
const Redis = require('ioredis');
const redis = new Redis();
const JSONCache = require('redis-json');
const jsonCache = new JSONCache(redis, { prefix: 'cache:' });


router.post('/', async (req, res) => 
{
  let connection, result, sql, info = []
  try 
  {
    connection = await oracledb.getConnection(dbConfig);
    sql = `SELECT sd.veedor, sd.candado, sd.tula, 
            t.nombre territorio, sn.descripcion TipoNovedad, sd.novedad, sd.fecha, sd.hora, sd.cantidad
            FROM appweb.ce_seguridaddescarga sd, appweb.ce_seguridadnovedad sn, gamble.territorios@chile t
            WHERE sd.fecha BETWEEN '${req.body.finicial}' AND '${req.body.ffinal}'
            AND sd.id_seguridadnovedad = sn.id_seguridadnovedad
            AND sd.id_territorio = t.codigo
            AND sd.recolector = '${req.body.id}'
            AND sd.id_territorio = sd.id_territorio`
    result = await connection.execute(sql);
    if (result.rows.length > 0) {
        for(let i = 0; i < result.rows.length; i++) {
             let objson = {}
             objson.veedor = result.rows[i][0]
             objson.candado = result.rows[i][1]
             objson.tula = result.rows[i][2]
             objson.territorio = result.rows[i][3]
             objson.tipoNovedad= result.rows[i][4]
             objson.novedad= result.rows[i][5]
             objson.fecha= result.rows[i][6]
             objson.hora= result.rows[i][7]
             objson.cantidad= result.rows[i][8]
             info.push(objson)
         }
         res.json({ status: true,  info });
         
     }else  
       res.json({ status: false });
  } 
  catch (err) {
    res.status(500).json(err)
  } finally {
    if (connection) {
      try {
        await connection.close();
      } 
     catch (err) {
      res.status(500).json(err)
     }
  }
 }
})

module.exports = router;