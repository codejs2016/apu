const express = require('express')
const router = express.Router()
const dbConfig = require('../keys')
const config = require('../configuration')
const oracledb = require('oracledb')

router.get('/:id', async (req, res) => {
    let connection, result
    try {
        connection = await oracledb.getConnection(dbConfig);
        result = await connection.execute(`
                SELECT SUM(sgt.tula) cantidad
                FROM APPWEB.CE_SEGURIDADDESCARGA sgt
                WHERE sgt.estado = 1 AND sgt.RECOLECTOR = '${req.params.id}'`)
            let info = []   
            if(result.rows.length > 0) {
                for (let i = 0; i < result.rows.length; i++) {
                    let objson = {}
                    objson.cantidad = result.rows[i][0]
                    info.push(objson)
               }
               res.json({ status: true, info })
            }
            else
             res.json({ status: false, msg: config.notfoud })
    }
    catch (err) {
        res.status(500).json(err)
    } finally {
        if (connection) {
            try {
                await connection.close();
            }
            catch (err) {
                res.status(500).json(err)
            }
        }
    }
})

router.get('/', async (req, res) => {
    let connection, sql, result;
    try {

        connection = await oracledb.getConnection(dbConfig);
        sql = `SELECT id_tesorerianovedad id, descripcion FROM APPWEB.ce_tesorerianovedad`
        result = await connection.execute(sql);
        let info = []
        if (result.rows.length > 0) {
            for (let i = 0; i < result.rows.length; i++) {
                let objson = {}
                objson.id = result.rows[i][0]
                objson.descripcion = result.rows[i][1]
                info.push(objson)
            }
            res.json({ status: true, info });
        }
        else
            res.json({ status: false, msg: config.notfoud });
    }
    catch (err) {
        res.status(500).json(err)
    } finally {
        if (connection) {
            try {
                await connection.close();
            }
            catch (err) {
                res.status(500).json(err)
            }
        }
    }
})

router.post('/', async (req, res) => {
    let connection, sql, result, dateNow, fecha = new Date(), hora, GUARDA, ID
    console.log( req.body  )
    try {
        connection = await oracledb.getConnection(dbConfig);
        hora = fecha.getHours() + ":" + fecha.getMinutes() + ":" + fecha.getSeconds();
        sql = `SELECT TO_CHAR (SYSDATE, 'DD/MM/RRRR') "NOW" FROM DUAL`
        result = await connection.execute(sql);
        dateNow = result.rows[0]
       
        for (let i = 0; i < req.body.length; i++) {
            result = await connection.execute(
                `INSERT INTO APPWEB.CE_TESORERIADESCARGANOV VALUES (
                    :id_tesoreriadescarganov,
                    :id_tesorerianovedad,
                    :id_territorio,
                    :tula,
                    :candado,
                    :recolector,
                    :rtesoreria,
                    :faltante,
                    :fecha,
                    :hora,
                    :entregado,
                    :cantidad,
                    :idrecogida)`,
                            [null,
                    parseInt(req.body[i].obs),
                    parseInt(req.body[i].id_territorio),
                    parseInt(req.body[i].tula),
                    parseInt(req.body[i].candado),
                    req.body[i].recolector,
                    req.body[i].rtesoreria,
                    parseInt(req.body[i].faltante),
                    new Date(),
                    hora,
                    1,
                    parseInt(req.body[i].cantidad),
                    parseInt(req.body[i].id)
                ],
                { autoCommit: true })
        }
        res.json({ status: true, msg: config.success });
    }
    catch (err) {
        res.status(500).json(err)
    } finally {
        if (connection) {
            try {
                await connection.close();
            }
            catch (err) {
                res.status(500).json(err)
            }
        }
    }
})

router.post('/cortes', async (req, res) => {
    let connection, sql, result, condicion
    try {
        connection = await oracledb.getConnection(dbConfig);

       
        if (parseInt(req.body.estado) > 0)
            condicion = `ce.recogido = ${req.body.estado}`

        if (req.body.finicial.toString().length > 0)
            condicion = `ce.fecha = TO_DATE('${req.body.finicial}', 'dd/mm/yyyy') AND ce.recogido = ${req.body.estado}`

        if (req.body.ffinal.toString().length > 0)
            condicion = `ce.fecha = TO_DATE('${req.body.ffinal}', 'dd/mm/yyyy') AND ce.recogido = ${req.body.estado}`

        if (req.body.finicial.toString().length > 0 && req.body.ffinal.toString().length)
            condicion = `ce.fecha BETWEEN TO_DATE('${req.body.finicial}', 'dd/mm/yyyy') AND  TO_DATE('${req.body.ffinal}','dd/mm/yyyy')  AND ce.recogido = ${req.body.estado}`

        sql = `SELECT ce.id_controlefectivo, ce.billete_cienmil,ce.billete_cincuenta, ce.billete_veinte, 
                ce.billete_diez, ce.billete_cinco, ce.billete_dos, ce.billete_mil, ce.total_moneda,
                ce.total_efectivo,
                ce.fecha, tip.nombre tipo, t.nombre puntoventa, o.nombre observacion,
                ce.hora, ce.valorverficacion, ce.recogido , ce.numerobolsa
                FROM appweb.ce_controlefectivo ce, appweb.ce_tipos tip,
                gamble.territorios@apuestas t, appweb.ce_observaciontipo o
                WHERE ${condicion}
                AND ce.cedula='${'CV'+req.body.documento.trim()}' 
                AND ce.id_tipos = tip.id_tipos
                AND ce.id_territorios = t.codigo
                AND ce.observacion_tipos = o.id_observaciontipo
                ORDER BY ce.hora ASC`
                result = await connection.execute(sql)
                let info = []
        if (result.rows.length > 0) {
            for (let i = 0; i < result.rows.length; i++) {
                let objson = {}
                objson.cienmil = result.rows[i][1]
                objson.cincuentamil = result.rows[i][2]
                objson.veintemil = result.rows[i][3]
                objson.diezmil = result.rows[i][4]
                objson.cincomil = result.rows[i][5]
                objson.dosmil = result.rows[i][6]
                objson.mil = result.rows[i][7]
                objson.totalmonedas = result.rows[i][8]
                objson.totalefectivo = result.rows[i][9]
                objson.fecha = result.rows[i][10]
                objson.tipo = result.rows[i][11]
                objson.puntoventa = result.rows[i][12]
                objson.observacion = result.rows[i][13]
                objson.hora = result.rows[i][14]
                objson.valorverficacion = result.rows[i][15]
                objson.idcontrolefectivo = result.rows[i][0]
                objson.recogido = result.rows[i][16]
                objson.numerobolsa = result.rows[i][17]
                info.push(objson)
            }
            res.json({ status: true, info });
        }
        else
            res.json({ status: false, msg: config.notfoud });

    }
    catch (err) {
        res.status(500).json(err)
    } finally {
        if (connection) {
            try {
                await connection.close();
            }
            catch (err) {
                res.status(500).json(err)
            }
        }
    }
})

router.post('/verificacion', async (req, res) => {
    let connection, result, sql, complemento = null
    try {
        req.body.valor = parseInt(req.body.valor.trim())
        connection = await oracledb.getConnection(dbConfig);
        sql = `UPDATE APPWEB.ce_controlefectivo 
                SET valorverficacion=:valor, 
                OBSVERIFICACION=:obsid
                WHERE id_controlefectivo=:id`

        result = await connection.execute(sql, {
            valor: req.body.valor,
            id: parseInt(req.body.id),
            obsid:req.body.obs
        }, { autoCommit: true })
        
        if(result.rowsAffected == 1) {
            res.json({ status: true });
        }
        else
            res.json({ status: false });
    }
    catch (err) {
        res.status(500).json(err)
    } finally {
        if (connection) {
            try {
                await connection.close();
            }
            catch (err) {
                res.status(500).json(err)
            }
        }
    }
})

router.post('/verificacionok', async (req, res) => {
    let connection, result, sql, complemento = null
    try {
        complemento = `${req.body.obs} ${req.body.nota}`
        connection = await oracledb.getConnection(dbConfig);
        sql = `UPDATE APPWEB.ce_controlefectivo 
        SET recogido=3,
        observacion_detalle2=:nota
        WHERE id_controlefectivo=:id`
        result = await connection.execute(sql, {
            id: parseInt(req.body.id),
            nota: complemento
        }, { autoCommit: true })
        
        if (result.rowsAffected == 1) {
            res.json({ status: true });
        }
        else
            res.json({ status: false });
    }
    catch (err) {
        res.status(500).json(err)
    } finally {
        if (connection) {
            try {
                await connection.close();
            }
            catch (err) {
                res.status(500).json(err)
            }
        }
    }
})

router.post('/entrega', async (req, res ) =>{
    let connection, sql, result, dateNow, fecha = new Date(), hora
    try {
        connection = await oracledb.getConnection(dbConfig);
        hora = fecha.getHours() + ":" + fecha.getMinutes() + ":" + fecha.getSeconds();
        sql = `SELECT TO_CHAR (SYSDATE, 'DD/MM/RRRR') "NOW" FROM DUAL`
        result = await connection.execute(sql);
        dateNow = result.rows[0]
        result = await connection.execute(
                `INSERT INTO APPWEB.CE_ENTREGA VALUES (
                    :id_entrega,
                    :cantidad,
                    :guarda,
                    :responsable,
                    :fecha,
                    :hora,
                    :estado,
                    :tipoobs,
                    :nota)`,
                    [null,
                    req.body.cantidad,
                    req.body.guarda,
                    req.body.responsable,
                    new Date(),
                    hora,
                    0,
                    req.body.descripcion,
                    req.body.nota
                ],
                { autoCommit: true })

        if(result.rowsAffected == 1){
            connection = await oracledb.getConnection(dbConfig)
            sql = `UPDATE APPWEB.CE_SEGURIDADDESCARGA SET
                ESTADO=2 
                WHERE RECOLECTOR=:id`;
            await connection.execute(sql,
                { id: req.body.guarda },
                { autoCommit: true })

            res.json({ status: true })  
        }
        else 
          res.json({ status: false })
    }
    catch (err) {
        res.status(500).json(err)
    } finally {
        if (connection) {
            try {
                await connection.close();
            }
            catch (err) {
                res.status(500).json(err)
            }
        }
    }
})


router.post('/novedades', async (req, res) => {
    let connection, sql, result, info = [], condicion = ''
    try {
        connection = await oracledb.getConnection(dbConfig);
        switch (req.body.estado) {
            case 1:
            
            condicion = `WHERE to_date(ce.fecha,'DD/MM/RRRR')=to_date(SYSDATE,'DD/MM/RRRR') AND ce.recogido = 3`

            if(req.body.documento != null )
              condicion = `WHERE ce.cedula = '${'CV'+req.body.documento}' AND ce.recogido = 3`

            if (req.body.finicial.toString().length > 0)
              condicion = `WHERE TO_DATE(ce.fecha) = '${req.body.finicial}' AND ce.recogido = 3`
    
            if (req.body.ffinal.toString().length > 0)
                condicion = `WHERE TO_DATE(ce.fecha) = '${req.body.ffinal}' AND ce.recogido = 3`
    
            if (req.body.finicial.toString().length > 0 && req.body.ffinal.toString().length)
                condicion = `WHERE ce.fecha BETWEEN TO_DATE('${req.body.finicial}', 'dd/mm/yyyy') AND  TO_DATE('${req.body.ffinal}','dd/mm/yyyy') 
                AND ce.recogido = 3`

            if (req.body.finicial.toString().length > 0 && req.body.ffinal.toString().length && req.body.documento != null ) 
                condicion = `WHERE ce.fecha BETWEEN TO_DATE('${req.body.finicial}', 'dd/mm/yyyy') AND  TO_DATE('${req.body.ffinal}','dd/mm/yyyy') 
                AND ce.recogido = 3 AND ce.cedula = '${'CV'+req.body.documento}'`
                
            sql = `SELECT ce.id_controlefectivo, 
                ce.total_efectivo,
                ce.fecha, t.nombre puntoventa,
                o.nombre observacion,
                ce.hora, ce.valorverficacion, ce.numerobolsa, ce.observacion_detalle1
                FROM appweb.ce_controlefectivo ce,
                gamble.territorios@apuestas t, appweb.ce_observaciontipo o
                ${condicion}
                AND ce.id_territorios = t.codigo
                AND ce.observacion_tipos = o.id_observaciontipo
                ORDER BY ce.hora ASC`
                
            result = await connection.execute(sql)
            
            if (result.rows.length > 0) {
                for (let i = 0; i < result.rows.length; i++) {
                    let objson = {}
                    objson.idcontrolefectivo = result.rows[i][0]
                    objson.totalefectivo = result.rows[i][1]
                    objson.fecha = result.rows[i][2]
                    objson.puntoventa = result.rows[i][3]
                    objson.observacion = result.rows[i][4]
                    objson.hora = result.rows[i][5]
                    objson.valorverficacion = result.rows[i][6]
                    objson.numerobolsa = result.rows[i][7]
                    objson.novedad = result.rows[i][8]
                    info.push(objson)
                }
                res.json({ status: true, info })
            } 
            else
              res.json({ status: false, msg: config.notfoud });
            break


            case 2:
            
            condicion = `WHERE to_date(fecha,'DD/MM/RRRR')=to_date(SYSDATE,'DD/MM/RRRR') AND estado = 0`
            if(req.body.documento != null )
                condicion = `WHERE guarda = '${req.body.documento}' AND estado = 0`
            
            if (req.body.finicial.toString().length > 0)
                condicion = `WHERE TO_DATE(fecha) = '${req.body.finicial}' AND estado = 0`
    
            if (req.body.ffinal.toString().length > 0)
                condicion = `WHERE TO_DATE(fecha) = '${req.body.ffinal}' AND estado = 0`
    
            if (req.body.finicial.toString().length > 0 && req.body.ffinal.toString().length)
                condicion = `WHERE fecha BETWEEN TO_DATE('${req.body.finicial}', 'dd/mm/yyyy') AND  TO_DATE('${req.body.ffinal}','dd/mm/yyyy') 
                AND estado = 0`

            if (req.body.finicial.toString().length > 0 && req.body.ffinal.toString().length && req.body.documento != null ){
                condicion = `WHERE fecha BETWEEN TO_DATE('${req.body.finicial}', 'dd/mm/yyyy') AND  TO_DATE('${req.body.ffinal}','dd/mm/yyyy') 
                     AND estado = 0 AND guarda = '${req.body.documento}'`
            }
                
            sql = `SELECT cantidad,fecha,hora,tipoobs,nota 
                    FROM appweb.ce_entrega ${condicion}
                    ORDER BY fecha ASC` 
                    console.log( "sql ", sql)
            result = await connection.execute(sql)
            
            if (result.rows.length > 0) {
                for (let i = 0; i < result.rows.length; i++) {
                    let objson = {}
                    objson.cantidad = result.rows[i][0]
                    objson.fecha = result.rows[i][1]
                    objson.hora = result.rows[i][2]
                    objson.tipo = result.rows[i][3]
                    objson.nota = result.rows[i][4]
                    info.push(objson)
                }
                res.json({ status: true, info })
            } 
            else
              res.json({ status: false, msg: config.notfoud });
            break

            case 3:

            condicion = `WHERE to_date(sgt.FECHA,'DD/MM/RRRR')=to_date(SYSDATE,'DD/MM/RRRR') AND sgt.estado = 2`
            
            if(req.body.documento != null )
                condicion = `WHERE sgt.RECOLECTOR = '${req.body.documento}' AND sgt.estado = 2`

            if (req.body.finicial.toString().length > 0)
                condicion = `WHERE TO_DATE( sgt.FECHA) = '${req.body.finicial}' AND sgt.estado = 2`
    
            if (req.body.ffinal.toString().length > 0)
                condicion = `WHERE TO_DATE( sgt.FECHA) = '${req.body.ffinal}' AND sgt.estado = 2`
    
            if (req.body.finicial.toString().length > 0 && req.body.ffinal.toString().length)
                condicion = `WHERE sgt.FECHA BETWEEN TO_DATE('${req.body.finicial}', 'dd/mm/yyyy') AND TO_DATE('${req.body.ffinal}','dd/mm/yyyy') AND sgt.estado = 2`
    
            if (req.body.finicial.toString().length > 0 && req.body.ffinal.toString().length && req.body.documento != null ){
                condicion = `WHERE sgt.FECHA BETWEEN TO_DATE('${req.body.finicial}', 'dd/mm/yyyy') AND  TO_DATE('${req.body.ffinal}','dd/mm/yyyy') 
                   AND sgt.estado = 2 AND sgt.RECOLECTOR = '${req.body.documento}'`
            }

            sql = `SELECT sgt.ID_SEGURIDADDESCARGA,sgt.id_territorio,t.NOMBRE,
                   sgt.TULA, sgt.CANDADO, sgt.CANTIDAD, sgt.novedad,
                    sgt.RECOLECTOR, sgt.FECHA, sgt.hora
                    FROM APPWEB.CE_SEGURIDADDESCARGA sgt, gamble.territorios@chile t
                    ${condicion} AND
                    sgt.id_territorio = t.codigo AND sgt.id_territorio IN
                    (
                        SELECT u.trtrio_codigo FROM gamble.ubicacionnegocios@chile
                        u WHERE u.trtrio_codigo_compuesto_de = u.trtrio_codigo_compuesto_de
                    )
                    GROUP BY sgt.ID_SEGURIDADDESCARGA,sgt.ID_TERRITORIO,
                    t.nombre,sgt.TULA,sgt.CANDADO,sgt.CANTIDAD,sgt.novedad,sgt.RECOLECTOR, sgt.FECHA, sgt.hora
                    ORDER BY t.nombre` 

                    console.log(" 3 ", sql )
            result = await connection.execute(sql)
            
            if (result.rows.length > 0) {
                for (let i = 0; i < result.rows.length; i++) {
                    let objson = {}
                    objson.punto = result.rows[i][2]
                    objson.tula = result.rows[i][3]
                    objson.candado = result.rows[i][4]
                    objson.cantidad = result.rows[i][5]
                    objson.novedad = result.rows[i][6]
                    objson.recolector = result.rows[i][7]
                    objson.fecha = result.rows[i][8]
                    objson.hora = result.rows[i][9]
                    info.push(objson)
                }
                res.json({ status: true, info })
            } 
            else
              res.json({ status: false, msg: config.notfoud });
            break


           case 4:
                
                condicion = `WHERE to_date(FECHA,'DD/MM/RRRR')=to_date(SYSDATE,'DD/MM/RRRR')`

                if(req.body.documento != null )
                 condicion = `WHERE documento = '${req.body.documento}'`

                 
                if (req.body.finicial.toString().length > 0)
                    condicion = `WHERE TO_DATE(fecha) = '${req.body.finicial}'`
        
                if (req.body.ffinal.toString().length > 0)
                    condicion = `WHERE TO_DATE(fecha) = '${req.body.ffinal}'`
        
                if (req.body.finicial.toString().length > 0 && req.body.ffinal.toString().length)
                    condicion = `WHERE fecha BETWEEN TO_DATE('${req.body.finicial}', 'dd/mm/yyyy') AND TO_DATE('${req.body.ffinal}','dd/mm/yyyy')`
        
                if (req.body.finicial.toString().length > 0 && req.body.ffinal.toString().length && req.body.documento != null ){
                    condicion = `WHERE FECHA BETWEEN TO_DATE('${req.body.finicial}', 'dd/mm/yyyy') AND  TO_DATE('${req.body.ffinal}','dd/mm/yyyy') 
                        AND documento = '${req.body.documento}'`
                }

                sql = `
                    SELECT c.documento, s.ruta, v.placa, c.obs, c.fecha, c.tipcambio
                    FROM APPWEB.CE_SEGURIDADAGENDACAMBIOS c
                    LEFT JOIN APPWEB.CE_SEGURIDADVEHICULO v ON v.id_seguridadvehiculo = c.placa
                    LEFT JOIN APPWEB.CE_SEGURIDADRUTA s ON s.id_seguridadruta = c.ruta
                    ${condicion}
                    ORDER BY c.fecha ASC` 

                result = await connection.execute(sql)
                
                if (result.rows.length > 0) {
                    for (let i = 0; i < result.rows.length; i++) {
                        let objson = {}
                        objson.documento = result.rows[i][0]
                        objson.ruta = result.rows[i][1]
                        objson.placa = result.rows[i][2]
                        objson.obs = result.rows[i][3]
                        objson.fecha = result.rows[i][4]
                        objson.tipocambio = result.rows[i][5]
                        info.push(objson)
                    }
                    res.json({ status: true, info })
                } 
                else
                  res.json({ status: false, msg: config.notfoud });
                break

                case 5:
                   
                    condicion = `WHERE to_date(FECHA,'DD/MM/RRRR')=to_date(SYSDATE,'DD/MM/RRRR') AND  ce.recogido = 3`
                    
                    if(req.body.documento != null )
                        condicion = `WHERE ce.cedula='${'CV'+req.body.documento}' AND ce.recogido = 3`

                        
                    if (req.body.finicial.toString().length > 0)
                        condicion = `WHERE TO_DATE(ce.fecha) = '${req.body.finicial}' AND ce.recogido = 3`
        
                    if (req.body.ffinal.toString().length > 0)
                        condicion = `WHERE TO_DATE(ce.fecha) = '${req.body.ffinal}' AND ce.recogido = 3`
            
                    if (req.body.finicial.toString().length > 0 && req.body.ffinal.toString().length)
                        condicion = `WHERE ce.fecha BETWEEN TO_DATE('${req.body.finicial}', 'dd/mm/yyyy') 
                        AND TO_DATE('${req.body.ffinal}','dd/mm/yyyy') AND ce.recogido =  3`

                    if (req.body.finicial.toString().length > 0 && req.body.ffinal.toString().length && req.body.documento != null ){
                        condicion = `WHERE ce.FECHA BETWEEN TO_DATE('${req.body.finicial}', 'dd/mm/yyyy') AND  TO_DATE('${req.body.ffinal}','dd/mm/yyyy') 
                            AND ce.cedula = '${'CV'+req.body.documento}'`
                    }
            
                    sql = `SELECT ce.id_controlefectivo, ce.billete_cienmil,ce.billete_cincuenta, ce.billete_veinte, 
                            ce.billete_diez, ce.billete_cinco, ce.billete_dos, ce.billete_mil, ce.total_moneda,
                            ce.total_efectivo,
                            ce.fecha, tip.nombre tipo, t.nombre puntoventa, o.nombre observacion,
                            ce.hora, ce.valorverficacion, ce.recogido , ce.numerobolsa
                            FROM appweb.ce_controlefectivo ce, appweb.ce_tipos tip,
                            gamble.territorios@apuestas t, appweb.ce_observaciontipo o
                            ${condicion}
                            AND ce.id_tipos = tip.id_tipos
                            AND ce.id_territorios = t.codigo
                            AND ce.observacion_tipos = o.id_observaciontipo
                            ORDER BY ce.hora ASC`
                           
                    result = await connection.execute(sql)
                    
                    if (result.rows.length > 0) {
                        for (let i = 0; i < result.rows.length; i++) {
                            let objson = {}
                            objson.cienmil = result.rows[i][1]
                            objson.cincuentamil = result.rows[i][2]
                            objson.veintemil = result.rows[i][3]
                            objson.diezmil = result.rows[i][4]
                            objson.cincomil = result.rows[i][5]
                            objson.dosmil = result.rows[i][6]
                            objson.mil = result.rows[i][7]
                            objson.totalmonedas = result.rows[i][8]
                            objson.totalefectivo = result.rows[i][9]
                            objson.fecha = result.rows[i][10]
                            objson.tipo = result.rows[i][11]
                            objson.puntoventa = result.rows[i][12]
                            objson.observacion = result.rows[i][13]
                            objson.hora = result.rows[i][14]
                            objson.valorverficacion = result.rows[i][15]
                            objson.idcontrolefectivo = result.rows[i][0]
                            objson.recogido = result.rows[i][16]
                            objson.numerobolsa = result.rows[i][17]
                            info.push(objson)
                        }
                        res.json({ status: true, info });
                    }
                    else
                        res.json({ status: false, msg: config.notfoud });
                    break;
        }

    }
    catch (err) {
        res.status(500).json(err)
        console.log("err 1 ",err )
    } finally {
        if (connection) {
            try {
                await connection.close();
            }
            catch (err) {
                res.status(500).json(err)
                console.log("err 2 ",err )
            }
        }
    }
})
module.exports = router;