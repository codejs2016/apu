const express = require('express');
const router = express.Router();
const dbConfig = require('../keys');
const config = require('../configuration');
const oracledb = require('oracledb');
const Redis = require('ioredis');
const redis = new Redis();
const JSONCache = require('redis-json');
const jsonCache = new JSONCache(redis, {prefix: 'cache:'});
const mail = require('../routes/mail')

router.get('/', async (req, res) => 
{
  let connection, sql, result
  try 
  {
    connection = await oracledb.getConnection(dbConfig);
    sql = `SELECT p.documento, p.nombres, p.apellido1, p.apellido2 FROM GAMBLE.personas p,
           appweb.ce_permisos per WHERE per.cedula = 'CP' || CAST(p.documento AS VARCHAR(12))
           AND per.cargo = 9 ORDER BY p.nombres`
    result = await connection.execute(sql);
    let info = []
    if(result.rows.length > 0){
       for(let i = 0; i < result.rows.length; i++) {
          let data = {}
          data.documento = result.rows[i][0]
          data.nombres = result.rows[i][1]
          data.apellido1 = result.rows[i][2]
          data.apellido2 = result.rows[i][3]
          info.push(data)
       }
      res.json({ status: true, info });
    }
    else
     res.json({ status: false, msg:config.notfoudGR });
  } 
  catch (err) {
     res.status(500).json(err)
  } finally {
    if (connection) {
      try {
        await connection.close();
      } 
     catch (err) {
      res.status(500).json(err)
     }
  }
 }
})


router.get('/vehiculos', async (req, res) => 
{
  let connection, sql, result
  try 
  {
    connection = await oracledb.getConnection(dbConfig);
    sql = `SELECT sv.id_seguridadvehiculo, sv.placa FROM appweb.ce_seguridadvehiculo sv`
    result = await connection.execute(sql);
    let info = []
    if(result.rows.length > 0){
       for(let i = 0; i < result.rows.length; i++) {
          let data = {}
          data.idvehiculo = result.rows[i][0]
          data.placa = result.rows[i][1]
          info.push(data)
       }
      res.json({ status: true, info });
    }
    else
     res.json({ status: false, msg:config.notfouduser });
  } 
  catch (err) {
    res.status(500).json(err)
  } finally {
    if (connection) {
      try {
        await connection.close();
      } 
     catch (err) {
      res.status(500).json(err)
     }
  }
 }
})

router.get('/rutas', async (req, res) => 
{
  let connection, sql, result
  try 
  {
    connection = await oracledb.getConnection(dbConfig);
    sql = `SELECT * FROM appweb.ce_seguridadruta`
    result = await connection.execute(sql);
    let info = []
    if(result.rows.length > 0){
       for(let i = 0; i < result.rows.length; i++){
          let data = {}
          data.idseguridadruta = result.rows[i][0]
          data.ruta = result.rows[i][1]
          info.push(data)
       }
       res.json({ status: true, info });
    }
    else
     res.json({ status: false, msg:config.notfouduser });
  } 
  catch (err) {
    res.status(500).json(err)
  } finally {
    if (connection) {
      try {
        await connection.close();
      } 
     catch (err) {
      res.status(500).json(err)
     }
  }
 }
})

router.get('/programacion', async (req, res) => 
{
  let connection, sql, result;
  try 
  {
    connection = await oracledb.getConnection(dbConfig);
    sql = `SELECT sa.id_seguridadagenda,  sa.documento, m.nombres ||' '|| m.apellido1 AS nombre, 
           sv.placa, sr.ruta, sa.fecha, sa.hora
           FROM appweb.ce_seguridadagenda sa, appweb.ce_seguridadvehiculo sv, appweb.ce_seguridadruta sr, gamble.personas m
           WHERE sa.id_seguridadvehiculo = sv.id_seguridadvehiculo
           AND sa.id_seguridadruta = sr.id_seguridadruta
           AND sa.documento = m.documento`
    result = await connection.execute(sql);
    if(result.rows.length > 0){
     resposenJson(result.rows)
     jsonCache.set('keyProgramacion', dataJson);
     const data = await jsonCache.get('keyProgramacion');
     res.json({ status: true, data });
    }
    else
     res.json({ status: false, msg:config.notfouduser });
  } 
  catch (err) {
    res.status(500).json(err)
  } finally {
    if (connection) {
      try {
        await connection.close();
      } 
     catch (err) {
      res.status(500).json(err)
     }
  }
 }
})

router.post('/saveprogramacion', async (req, res) => 
{
  let connection, result, fecha = new Date(), hora, sql, vfi, vff, splfi, splff
  hora = fecha.getHours()+":"+fecha.getMinutes()+":"+fecha.getSeconds();
  try 
  {
    connection = await oracledb.getConnection(dbConfig);
    sql = `SELECT * FROM APPWEB.CE_SEGURIDADCALENDARIO
           WHERE FECHAINICIAL = '${req.body.finicial}'
           AND FECHAFINAL = '${req.body.ffinal}' `
   
    result = await connection.execute(sql);
    if(result.rows.length  > 0){
      res.json({ status: 'existe', msg:"Ya existe una programación registrada para esas fechas" });
    }
    else
    {
        result = await connection.execute(`INSERT INTO 
        APPWEB.CE_SEGURIDADCALENDARIO VALUES (
        :ID_SEGURIDADCALENDARIO, 
        :PROGRAMACION, 
        :FECHAINICIAL, 
        :FECHAFINAL, 
        :HORA,
        :ESTADO)`,
      [ null, 1, req.body.finicial, req.body.ffinal, hora, 0], { autoCommit: true } );
      if(result.rowsAffected == 1){
        res.json({ status: true, msg:config.success });
      }
    }
  } 
  catch (err) {
    res.status(500).json(err)
  } finally {
    if (connection) {
      try {
        await connection.close();
      } 
     catch (err) {
      res.status(500).json(err)
     }
  }
 }
})

router.post('/validate', async (req, res) => 
{
  let connection, sql, result, loginuser ;
  try 
  {
    connection = await oracledb.getConnection(dbConfig);
    sql = `SELECT s.loginusr, gambledbv10_2.decrypt(s.PASSWORD) AS clave,
           m.nombres ||' '|| m.apellido1 AS Nombre
           FROM gamble.usuarios@apuestas s, gamble.personas@apuestas m
           WHERE  s.loginusr  ='${req.body.user}'
           and gambledbv10_2.decrypt(s.PASSWORD)='${req.body.password}' 
           and s.loginusr = 'CP' || CAST(m.documento AS VARCHAR(12))
           GROUP BY s.loginusr, s.PASSWORD, m.nombres, m.apellido1`
    result = await connection.execute(sql)
    if(result.rows.length > 0){
      loginuser = req.body.user.replace("CP","");
      sql = `SELECT * FROM APPWEB.CE_SEGURIDADAGENDA 
             WHERE IDGUARDA=${parseInt(loginuser)} AND ESTADO = 1 AND ROL = 1`;
      result = await connection.execute(sql)
      if(result.rows.length > 0){
        res.json({ status: true, permiso:"Si tiente un agenda activa", data:result.rows });
      }else{
        res.json({ msg:"No tienes una agenda asignada"});
      }
    }
    else
     res.json({ status: false, msg:config.validateguarda });
  } 
  catch (err) {
    res.status(500).json(err)
  } finally {
    if (connection) {
      try {
        await connection.close();
      } 
     catch (err) {
      res.status(500).json(err)
     }
  }
 }
})

module.exports = router;

