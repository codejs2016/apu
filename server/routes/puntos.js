const express = require('express');
const router = express.Router();
const dbConfig = require('../keys');
const config = require('../configuration');
const oracledb = require('oracledb');

router.get('/:id', async (req, res) => 
{
  let connection, sql, result
  try 
  {
    
    connection = await oracledb.getConnection(dbConfig);
    sql = `SELECT ce.id_territorios, t.nombre
           FROM appweb.ce_controlefectivo ce, gamble.territorios@apuestas t
           WHERE ce.fecha BETWEEN TRUNC(SYSDATE - 25) AND TRUNC(SYSDATE)
           AND ce.id_territorios = t.codigo
           AND ce.id_territorios IN 
           (
            SELECT u.trtrio_codigo  
            FROM gamble.ubicacionnegocios@apuestas u
            WHERE u.trtrio_codigo_compuesto_de ='${req.params.id}'
           )
           GROUP BY ce.id_territorios, t.nombre
           ORDER BY t.nombre`
    result = await connection.execute(sql)
    let info = []
    if (result.rows.length > 0) {
        for(let i = 0; i < result.rows.length; i++) {
            let objson = {}
            objson.id = result.rows[i][0]
            objson.nombre = result.rows[i][1]
            info.push(objson)
        }
        res.json({ status: true, msg:config.success, info });
    } 
    else
     res.json({ status: false, msg:config.error });
  } 
  catch (err) {
    res.status(500).json(err)
  } finally {
    if (connection) {
      try {
        await connection.close();
      } 
     catch (err) {
      res.status(500).json(err)
     }
  }
 }
})


router.get('/info/:id', async (req, res) => 
{
  let connection, result, sql, info = []
  try 
  {
    connection = await oracledb.getConnection(dbConfig);
    sql = `SELECT count(*) cantidad
           FROM appweb.ce_controlefectivo ce, gamble.territorios@apuestas t
           WHERE ce.id_territorios = t.codigo
           AND ce.id_territorios = ${req.params.id} 
           AND ce.recogido = 0`
    result = await connection.execute(sql);
    if(result.rows.length > 0) {
        res.json({ status: true, msg:config.successQ, cantidad : result.rows[0][0] });
     }else  
       res.json({ status: false, msg:config.errorRC });
  } 
  catch (err) {
    res.status(500).json(err)
  } finally {
    if (connection) {
      try {
        await connection.close();
      } 
     catch (err) {
      res.status(500).json(err)
     }
  }
 }
})

module.exports = router;

