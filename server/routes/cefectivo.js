const express = require('express');
const router = express.Router();
const dbConfig = require('../keys');
const config = require('../configuration');
const oracledb = require('oracledb');

router.post('/', async (req, res) => {
    let connection, sql, result
    try {
        connection = await oracledb.getConnection(dbConfig)
        sql = `SELECT sgt.ID_SEGURIDADDESCARGA,sgt.id_territorio,t.NOMBRE, 
        sgt.TULA, sgt.CANDADO, sgt.CANTIDAD, sgt.novedad,
        sgt.RECOLECTOR
        FROM APPWEB.CE_SEGURIDADDESCARGA sgt, gamble.territorios@chile t
        WHERE to_date( sgt.FECHA,'DD/MM/RRRR') =to_date(SYSDATE,'DD/MM/RRRR')
        AND sgt.estado = 2
        AND sgt.RECOLECTOR IN(${req.body.guardas.toString()}) AND 
        sgt.id_territorio = t.codigo AND sgt.id_territorio IN
        (
        SELECT u.trtrio_codigo FROM gamble.ubicacionnegocios@chile
        u WHERE u.trtrio_codigo_compuesto_de = u.trtrio_codigo_compuesto_de
        )
        GROUP BY sgt.ID_SEGURIDADDESCARGA,sgt.ID_TERRITORIO,
        t.nombre,sgt.TULA,sgt.CANDADO,sgt.CANTIDAD,sgt.novedad,sgt.RECOLECTOR
        ORDER BY t.nombre`
        result = await connection.execute(sql)
        let info = []
        if (result.rows.length > 0) {
            for (let i = 0; i < result.rows.length; i++) {
                let objson = {}
                objson.id = result.rows[i][0]
                objson.id_territorio = result.rows[i][1]
                objson.nombre = result.rows[i][2]
                objson.tula = result.rows[i][3]
                objson.candado = result.rows[i][4]
                objson.cantidad = result.rows[i][5]
                objson.novedad = result.rows[i][6]
                objson.recolector = result.rows[i][7]
                info.push(objson)
            }
            res.json({ status: true, info });
        }
        else
            res.json({ status: false, msg:"¡No se encontraron entrega de tulas registradas en el sistema!"});
    }
    catch (err) {
        res.status(500).json(err)
    } finally {
        if (connection) {
            try {
                await connection.close();
            }
            catch (err) {
                res.status(500).json(err)
            }
        }
    }
})

router.get('/entregas', async (req, res) => {
    let connection, sql, result
    try {
        connection = await oracledb.getConnection(dbConfig);
        sql = `SELECT guarda  FROM APPWEB.CE_ENTREGA WHERE ESTADO = 0`
        result = await connection.execute(sql);
        let info = []
        if (result.rows.length > 0) {
            for (let i = 0; i < result.rows.length; i++) {
                let objson = {}
                objson.guarda = result.rows[i][0]
                info.push(objson)
            }
            res.json({ status: true, info });
        }
        else
            res.json({ status: false, msg: config.notfoud });
    }
    catch (err) {
        res.status(500).json(err)
    } finally {
        if (connection) {
            try {
                await connection.close();
            }
            catch (err) {
                res.status(500).json(err)
            }
        }
    }
})


module.exports = router;