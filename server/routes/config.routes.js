const express = require('express')
const router =  express.Router()

const configCtrl = require('../controller/config.controller')

router.get('/', configCtrl.getPuntos)
router.post('/', configCtrl.create)
router.post('/search', configCtrl.search)
router.post('/update', configCtrl.update)
router.get('/rangos', configCtrl.getRangos)
router.post('/rangos', configCtrl.updateRango)
router.get('/semaforo', configCtrl.semaforo)

module.exports = router