const express = require('express');
const router = express.Router();
const dbConfig = require('../keys');
const config = require('../configuration');
const oracledb = require('oracledb');


router.get('/days', async (req, res) => 
{
  let connection, sql, result
  connection = await oracledb.getConnection(dbConfig);
  sql = `
  SELECT id_seguridadcalendario,
    (to_date(FECHAFINAL, 'DD-MM-YYYY') 
        - to_date(FECHAINICIAL, 'DD-MM-YYYY')) days
    FROM appweb.CE_SEGURIDADCALENDARIO
    WHERE PROGRAMACION = 1 AND ESTADO IN(0,1)`
  result = await connection.execute(sql);
  let info = []
  if(result.rows.length > 0){
    for(let i = 0; i < result.rows.length; i++) {
       let data = {}
       data.id = result.rows[i][0]
       data.days = result.rows[i][1]
       info.push(data)
    }
    res.json({ status: true, info });
 }
 else
  res.json({ status: false, msg:config.notfouduser });
})

router.get('/', async (req, res) => 
{
  let connection, sql, result
  try 
  {
    connection = await oracledb.getConnection(dbConfig);
    sql = `SELECT * FROM APPWEB.CE_SEGURIDADCALENDARIO WHERE PROGRAMACION = 1 AND ESTADO IN(0,1)`
    result = await connection.execute(sql);
    let info = []
    if(result.rows.length > 0){
       for(let i = 0; i < result.rows.length; i++) {
          let data = {}
          data.id = result.rows[i][0]
          data.finicial = result.rows[i][2]
          data.ffinal = result.rows[i][3]
          data.hora = result.rows[i][4]
          data.estado = result.rows[i][5]
          info.push(data)
       }
       res.json({ status: true, info });
    }
    else
     res.json({ status: false, msg:config.notfouduser });
  } 
  catch (err) {
     res.status(500).json(err)
  } finally {
    if (connection) {
      try {
        await connection.close();
      } 
     catch (err) {
      res.status(500).json(err)
     }
  }
 }
})

router.post('/', async (req, res) => 
{
  let connection, result, dateNow, fecha = new Date(), hora, sql
  try 
  {
    connection = await oracledb.getConnection(dbConfig);
    hora = fecha.getHours()+":"+fecha.getMinutes()+":"+fecha.getSeconds();
    sql = `SELECT TO_DATE(SYSDATE, 'DD/MM/RRRR') FROM DUAL`
    result = await connection.execute(sql);
    dateNow = result.rows[0][0]
    for (let i = 0; i < req.body.data.length; i++) 
    {
     result = await connection.execute(`
      INSERT INTO APPWEB.CE_SEGURIDADAGENDA VALUES 
      ( :ID_SEGURIDADAGENDA, 
       :ID_SEGURIDADVEHICULO, 
       :ID_SEGURIDADRUTA, 
       :FECHA, 
       :HORA,
       :IDGUARDA, 
       :IDAGENDA,
       :ESTADO,
       :ROL)`,
     [ null, 
       req.body.data[i].idplaca, 
       req.body.data[i].idruta, 
       dateNow,
       hora, 
       req.body.data[i].documento, 
       req.body.data[i].idAgenda, 
       1,
       req.body.data[i].rol
      ], { autoCommit: true } )

      await connection.execute(`
        INSERT INTO APPWEB.CE_SEGURIDADAGENDACAMBIOS VALUES 
        (
        :IDAGENDA, 
        :DOCUMENTO, 
        :PLACA, 
        :RUTA, 
        :OBS,
        :FECHA,
        :TIPOCAMBIO)`,
      [ null, 
        req.body.data[i].idAgenda,
        req.body.data[i].documento, 
        req.body.data[i].idplaca, 
        req.body.data[i].idruta, 
        dateNow,
        req.body.data[i].tipocambio
        ], { autoCommit: true } )
    }
    if(result.rowsAffected == 1){

     sql = `UPDATE APPWEB.CE_SEGURIDADCALENDARIO SET
            ESTADO=:estado
            WHERE ID_SEGURIDADCALENDARIO=:idagenda`;
     connection.execute(sql, { estado: 1, idagenda:req.body.data[0].idAgenda  }, { autoCommit: true})
     res.json({ status: true });
    }
    else  
      res.json({ status: false });
  } 
  catch (err) {
    res.status(500).json(err)
  } finally {
    if (connection) {
      try {
        await connection.close();
      } 
     catch (err) {
      res.status(500).json(err)
     }
  }
 }
})


router.post('/update/guarda', async (req, res) => 
{
  console.log( req.body )
  let connection, result, sql, condition
  try 
  {
    connection = await oracledb.getConnection(dbConfig)
    sql = `SELECT TO_DATE(SYSDATE, 'DD/MM/RRRR') FROM DUAL`
    result = await connection.execute(sql);
    dateNow = result.rows[0][0]

    if(req.body.idruta > 0){
      sql = `UPDATE APPWEB.CE_SEGURIDADAGENDA SET
      ID_SEGURIDADRUTA=:idruta
      WHERE IDGUARDA=:documento 
      AND IDAGENDA=:idagenda`;
      result = await connection.execute(sql, {
      idruta: parseInt(req.body.idruta),
      documento: parseInt(req.body.documento),
      idagenda: parseInt(req.body.idagenda)
      }, { autoCommit: true})
    }
     
    if(req.body.idplaca > 0)
    {
      sql = `UPDATE APPWEB.CE_SEGURIDADAGENDA SET
      ID_SEGURIDADVEHICULO=:idplaca
      WHERE IDGUARDA=:documento 
      AND IDAGENDA=:idagenda`;
      result = await connection.execute(sql, {
      idplaca: parseInt(req.body.idplaca),
      documento: parseInt(req.body.documento),
      idagenda: parseInt(req.body.idagenda)
      }, { autoCommit: true})
    }

    if(req.body.rol > 0)
    {
        sql = `UPDATE APPWEB.CE_SEGURIDADAGENDA SET
        ROL=:rol
        WHERE IDGUARDA=:documento 
        AND IDAGENDA=:idagenda`;
        result = await connection.execute(sql, {
        rol: parseInt(req.body.rol),
        documento: parseInt(req.body.documento),
        idagenda: parseInt(req.body.idagenda)
        }, { autoCommit: true})
    }
    
    if(result.rowsAffected > 0){
         await connection.execute(`
            INSERT INTO APPWEB.CE_SEGURIDADAGENDACAMBIOS VALUES 
            (
            :IDAGENDA, 
            :DOCUMENTO, 
            :PLACA, 
            :RUTA, 
            :OBS,
            :FECHA,
            :TIPOCAMBIO)`,
            [ req.body.idagenda,
            req.body.documento, 
            parseInt(req.body.idplaca) == 0 ? 0: parseInt(req.body.idplaca), 
            parseInt(req.body.idruta) == 0 ? 0: parseInt(req.body.idruta),
            req.body.obs,
            dateNow,
            req.body.tipocambio
            ], { autoCommit: true } )
          }
         res.json({ status: true });
  } 
  catch (err) {
    res.status(500).json(err)
  } finally {
    if (connection){
      try {
        await connection.close();
      } 
     catch (err) {
      res.status(500).json(err)
     }
  }
 }
})

router.post('/deleteagenda', async (req, res) => 
{
  let connection, result, sql
  try 
  {
    connection = await oracledb.getConnection(dbConfig);
    sql = `DELETE 
           FROM APPWEB.CE_SEGURIDADCALENDARIO 
           WHERE ID_SEGURIDADCALENDARIO=:idagenda`;
    result = await connection.execute(sql, {
      idagenda: parseInt(req.body.idagenda)
     }, { autoCommit: true})
    res.json({ status: true });
  } 
  catch (err) {
    res.status(500).json(err)
  } finally {
    if (connection) {
      try {
        await connection.close();
      } 
     catch (err) {
      res.status(500).json(err)
     }
  }
 }
})

router.post('/delete/guarda', async (req, res) => 
{
    let connection, sql
    connection = await oracledb.getConnection(dbConfig)
    sql = `DELETE FROM APPWEB.CE_SEGURIDADAGENDA
         WHERE IDGUARDA=:documento 
         AND IDAGENDA=:idagenda`;
    await connection.execute(sql, {
    documento: parseInt(req.body.documento),
    idagenda: parseInt(req.body.idagenda)
    }, { autoCommit: true})
    res.json({ status: true })
})

router.get('/recurso/:id', async (req, res) => 
{
  let connection, sql, result
  try 
  {
    connection = await oracledb.getConnection(dbConfig);
    sql = `SELECT p.documento, p.nombres || ' ' || p.apellido1 || ' ' || p.apellido2  guarda,
            sv.placa, sr.ruta, sv.id_seguridadvehiculo, sr.id_seguridadruta, sa.rol, sa.estado
            FROM  GAMBLE.personas p
            JOIN  appweb.ce_permisos per ON per.cargo = 9 
            JOIN  appweb.CE_SEGURIDADAGENDA sa ON sa.idguarda = p.documento
            LEFT JOIN  appweb.CE_SEGURIDADVEHICULO sv  ON sv.id_seguridadvehiculo = sa.id_seguridadvehiculo
            LEFT JOIN  appweb.CE_SEGURIDADRUTA sr ON sr.id_seguridadruta = sa.id_seguridadruta
            WHERE per.cedula = 'CP' || CAST(p.documento AS VARCHAR(12)) AND sa.estado = 1
            AND sa.idagenda = ${req.params.id} ORDER BY p.nombres`
    result = await connection.execute(sql);
    let info = []
    if(result.rows.length > 0){
       for(let i = 0; i < result.rows.length; i++) {
          let data = {}
          data.documento = result.rows[i][0]
          data.guarda = result.rows[i][1]
          data.placa = result.rows[i][2]
          data.ruta = result.rows[i][3]
          data.idvehiculo = result.rows[i][4]
          data.idruta = result.rows[i][5]
          data.idrol = result.rows[i][6]
          data.estado = result.rows[i][7]
          info.push(data)
       }
      res.json({ status: true, info });
    }
    else
     res.json({ status: false });
  } 
  catch (err) {
     res.status(500).json(err)
  } finally {
    if (connection) {
      try {
        await connection.close();
      } 
     catch (err) {
      res.status(500).json(err)
     }
  }
 }
})

router.post('/status', async (req, res ) =>{
  let connection, sql, status
  connection = await oracledb.getConnection(dbConfig)
  if(req.body.st)
    status =  1
  else
    status = 0

  sql = `UPDATE APPWEB.CE_SEGURIDADAGENDA SET 
           ESTADO=:status    
           WHERE IDGUARDA=:id 
           AND IDAGENDA=:idagenda`;
  await connection.execute(sql, {
    status:status,
    id: req.body.id,
    idagenda: req.body.idagenda
  },{ autoCommit: true})
  res.json({ status: true })
})

module.exports = router;

