const express = require('express');
const router = express.Router();
const dbConfig = require('../keys');
const config = require('../configuration');
const oracledb = require('oracledb');
const Redis = require('ioredis');
const redis = new Redis();
const JSONCache = require('redis-json');
const jsonCache = new JSONCache(redis, {prefix: 'cache:'});
const jwt = require('jsonwebtoken');


const { mongoose } = require("../database");
const AuthUser = require("../mongo/auth");
const mail = require('../routes/mail')


router.post('/', async (req, res) => 
{
  let connection, loginuser, sql, result, resultRole, data = {}
  try 
  {
     loginuser = req.body.username.substring(0,2);
     connection = await oracledb.getConnection(dbConfig);
     switch (loginuser) {
         case config.cv:
         case config.CV:  
         if(config.cv == 'cv')
          req.body.username = req.body.username.replace("cv", "CV")

         sql = `SELECT  s.loginusr,gambledbv10_2.decrypt(s.PASSWORD) AS clave,
          (
           SELECT h.ubcneg_trtrio_codigo
           FROM gamble.horariopersonas@apuestas h
           WHERE h.contvta_prs_documento = m.documento AND h.tipodia = 'N' and FECHAFINAL is null
           group by h.ubcneg_trtrio_codigo
          ) territorio,
          m.nombres
          || ' '
          || m.apellido1 AS Usuario, m.documento
           FROM gamble.usuarios@apuestas s,gamble.personas@apuestas m
           WHERE
           s.loginusr   = '${req.body.username}'
           AND gambledbv10_2.decrypt(s.PASSWORD) = '${req.body.password}'
           AND
           s.loginusr = 'CV' || CAST(m.documento AS VARCHAR(12))`
          result = await connection.execute(sql);
          if(result.rows.length > 0){
            for(let i = 0; i < result.rows.length; i++) {
              data.loginuser = result.rows[i][0]
              data.territorio = result.rows[i][2]
              data.username = result.rows[i][3]
              data.documento = result.rows[i][5]
              data.tokensession = req.sessionID
              data.role = config.roleasesora
           }
           return jwt.sign({ data }, '74unnbbr*?&/··,!?¿_*SASDAS@', { expiresIn: '1h' }, (err, token) =>
           {
             res.json({ token, status: true,  data} );
           })
          }
          else
           res.json({ status: false, msg:config.notfouduser });
        break;

         case config.cp:
         case config.CP:
          sql = `SELECT  s.loginusr,gambledbv10_2.decrypt(s.PASSWORD) AS clave,
          (
           SELECT h.ubcneg_trtrio_codigo
           FROM gamble.horariopersonas@apuestas h
           WHERE h.contvta_prs_documento = m.documento AND h.tipodia = 'N' and FECHAFINAL is null
           group by h.ubcneg_trtrio_codigo
          ) territorio,
          m.nombres
          || ' '
          || m.apellido1 AS Usuario, m.documento
           FROM gamble.usuarios@apuestas s,gamble.personas@apuestas m
           WHERE
           s.loginusr   = '${req.body.username}'
           AND gambledbv10_2.decrypt(s.PASSWORD) = '${req.body.password}'
           AND
           s.loginusr = 'CP' || CAST(m.documento AS VARCHAR(12))`
          result = await connection.execute(sql);
          if(result.rows.length > 0){
           for(let i = 0; i < result.rows.length; i++) 
           {

            data.loginuser = result.rows[i][0]
            data.username = result.rows[i][3]
            data.documento = result.rows[i][4]
            data.tokensession = req.sessionID
           }
           
           sql = `SELECT per.cargo, c.descripcion FROM appweb.ce_permisos per, appweb.cargos c 
                  WHERE  per.cedula = '${req.body.username}' AND per.cargo = c.codigo 
                  AND per.AUTORIZACION = 1`
           resultRole = await connection.execute(sql);
           if(resultRole.rows.length > 0){
            for(let i = 0; i < resultRole.rows.length; i++) {
              data.role = resultRole.rows[i][1]
            }
            
          } 
          return jwt.sign({ data }, '74unnbbr*?&/··,!?¿_*SASDAS@', { expiresIn: '1h' }, (err, token) =>
          {
            res.json({ token, status: true,  data} );
          });
        }
        else
           res.json({ status: false, msg:config.notfouduser });
         break;
     }
  } 
  catch (err) {
    console.log(err);
     res.status(500).json(err)
  } finally {
    if (connection) {
      try {
        await connection.close();
      } 
     catch (err) {
      console.log(err);
      res.status(500).json(err)
     }
  }
 }
})

router.get('/logout/:id', async (req, res) => {
  const { id } = req.params
  await pool.query("DELETE FROM sig_control_session WHERE session_secret = '"+id+"' ");
  req.session.destroy()
  res.status(200).send({ status : true })
})

router.get('/isLoggedIn/:token', async (req, res) => {
  const { token } = req.params
  jwt.verify(token, '74unnbbr*?&/··,!?¿_*SASDAS@', (err, authData) =>{
    if(err){
      res.status(200).send({ status : false, msg: config.notisloged })
      //res.sendStatus(403);
    }
    else{
        res.json({
            status : true,
            msg: config.haveisloged, 
            authData
        });
    }
  });
  
})

router.get('/removeLoggedIn/:token', async (req, res) => {
  const { token } = req.params
  const keylogin = await jsonCache.get('keylogin')
  if(token ===  keylogin.token){
    res.status(200).send({ status : true, msg: config.deleteKey }) 
  }else
    res.status(200).send({ status : false, msg: config.notisloged })
})

async function saveLoged(user){
  jsonCache.set('keylogin', user)
  let res = await  jsonCache.get('keylogin')
}

function generateToken(data){
  return jwt.sign({ data }, '74unnbbr*?&/··,!?¿_*SASDAS@', { expiresIn: '30s' }, (err, token) =>{
      res.json( token );
  });
}

function verifiyTwoSession(user){
 const userAuth = new AuthUser({
    user: user
  });
  userAuth.save();
}

function verifiyTwoSessionExist(data){
  AuthUser.findOne( { user: data.loginuser }, function(err, user)
  {
    console.log( typeof user )
    console.log( user )
    if(typeof(user) === 'object'){
      if(user){
        console.log("todo ok ...");
        mail.dobleAutenticacion(data);
        return 1;
      }
      else{
        verifiyTwoSession(data.loginuser);
      }
    }
  })  
}
module.exports = router;