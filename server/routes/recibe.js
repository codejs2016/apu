const express = require('express');
const router = express.Router();
const dbConfig = require('../keys');
const config = require('../configuration');
const oracledb = require('oracledb');
const initSendEmail = require("./mail");

router.get('/:id', async (req, res) => {
  let connection, result, sql, dataTransport = []
  try {
    connection = await oracledb.getConnection(dbConfig);
    sql = `SELECT SUM(total_efectivo) FROM 
            APPWEB.ce_controlefectivo WHERE ID_TIPOS=2 AND 
            TO_DATE(FECHA,'DD/MM/RRRR')=TO_DATE(SYSDATE,'DD/MM/RRRR')
            AND CEDULA='${req.params.id}' AND RECOGIDO = 0`;

            //console.log(sql)
    result = await connection.execute(sql);
    if (result.rows.length > 0) {
      dataTransport.push({ giros: result.rows })
    }

    sql = `SELECT SUM(total_efectivo) FROM 
            APPWEB.ce_controlefectivo WHERE ID_TIPOS=1 AND 
            TO_DATE(FECHA,'DD/MM/RRRR')=TO_DATE(SYSDATE,'DD/MM/RRRR')
            AND CEDULA='${req.params.id}' AND RECOGIDO = 0`;

           // console.log(sql)
    result = await connection.execute(sql);
    if (result.rows.length > 0) {
      dataTransport.push({ chance: result.rows })
    }

    sql = `SELECT SUM(valor) FROM 
           APPWEB.ce_pagoacumulado
           WHERE estado='pagado' 
           AND TO_DATE(FECHA,'DD/MM/RRRR')=TO_DATE(SYSDATE,'DD/MM/RRRR')
           AND CEDULA='${req.params.id}'`

           //console.log(sql)
    result = await connection.execute(sql);
    
    if (result.rows.length > 0) {
      dataTransport.push({ pagos: result.rows })
    } 
    
    //console.log(dataTransport)
    
    res.json({ status: true,  info:dataTransport });
  }
  catch (err) {
    res.json(err)
  } finally {
    if (connection) {
      try {
        await connection.close();
      }
      catch (err) {
        res.json(err)
      }
    }
  }
})

module.exports = router;