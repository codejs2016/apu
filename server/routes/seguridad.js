const express = require('express');
const router = express.Router();
const dbConfig = require('../keys');
const config = require('../configuration');
const oracledb = require('oracledb');
const Redis = require('ioredis');
const redis = new Redis();
const JSONCache = require('redis-json');
const jsonCache = new JSONCache(redis, {prefix: 'cache:'});
var dataJsonGuarda = []
var dataJsonVehiculo = []
var dataJsonRuta = []

router.get('/', async (req, res) => 
{
  let connection, sql, result;
  try 
  {
    connection = await oracledb.getConnection(dbConfig);
    sql = `SELECT sn.id_seguridadnovedad, sn.descripcion FROM appweb.ce_seguridadnovedad sn`
    result = await connection.execute(sql)
    let info = []
    if (result.rows.length > 0) {
        for(let i = 0; i < result.rows.length; i++) {
            let objson = {}
            objson.id = result.rows[i][0]
            objson.descripcion = result.rows[i][1]
            info.push(objson)
        }
        res.json({ status: true, msg:config.success, info });
    } 
    else
     res.json({ status: false, msg:config.error });
  } 
  catch (err) {
    res.status(500).json(err)
  } finally {
    if (connection) {
      try {
        await connection.close();
      } 
     catch (err) {
      res.status(500).json(err)
     }
  }
 }
})


module.exports = router;

