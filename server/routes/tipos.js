const express = require('express');
const router = express.Router();
const dbConfig = require('../keys');
const oracledb = require('oracledb');


router.get('/', async (req, res) => {
  let connection, result, sql, dataTransport = []
  try {
    connection = await oracledb.getConnection(dbConfig);
    sql = `SELECT * FROM appweb.ce_tipos`;
    result = await connection.execute(sql);
    let info = []
    if (result.rows.length > 0) {
      for(let i = 0; i < result.rows.length; i++) {
          let objson = {}
          objson.id = result.rows[i][0]
          objson.nombre = result.rows[i][1]
          info.push(objson)
      }
      res.json({ status: true,  info });
    } 
   

  }
  catch (err) {
    res.json(err)
  } finally {
    if (connection) {
      try {
        await connection.close();
      }
      catch (err) {
        res.json(err)
      }
    }
  }
})


router.get('/obs', async (req, res) => {
  let connection, result, sql, dataTransport = []
  try {
    connection = await oracledb.getConnection(dbConfig);
    sql = `SELECT * FROM appweb.ce_observaciontipo`;
    result = await connection.execute(sql);
    let info = []
    if (result.rows.length > 0) {
     for(let i = 0; i < result.rows.length; i++) {
        let objson = {}
        objson.id = result.rows[i][0]
        objson.nombre = result.rows[i][1]
        info.push(objson)
      }
      res.json({ status: true,  info });
    } 
  }
  catch (err) {
    res.json(err)
  } finally {
    if (connection) {
      try {
        await connection.close();
      }
      catch (err) {
        res.json(err)
      }
    }
  }
})


module.exports = router;