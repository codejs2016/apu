const express = require('express');
const router = express.Router();
const dbConfig = require('../keys');
const config = require('../configuration');
const oracledb = require('oracledb');
const Redis = require('ioredis');
const redis = new Redis();
const JSONCache = require('redis-json');
const jsonCache = new JSONCache(redis, { prefix: 'cache:' });


router.get('/:id', async (req, res) => {
    let connection, sql, result;
    try {
        connection = await oracledb.getConnection(dbConfig);
        sql = `SELECT ce.id_controlefectivo, ce.billete_cienmil,ce.billete_cincuenta, ce.billete_veinte, 
               ce.billete_diez, ce.billete_cinco, ce.billete_dos, ce.billete_mil, ce.total_moneda, ce.total_efectivo,
               ce.fecha, tip.nombre tipo, t.nombre puntoventa, o.nombre observacion, ce.hora, ce.numerobolsa
               FROM appweb.ce_controlefectivo ce, appweb.ce_tipos tip, gamble.territorios@apuestas t, appweb.ce_observaciontipo o
               WHERE to_date(ce.fecha,'DD/MM/RRRR')=to_date(SYSDATE,'DD/MM/RRRR') 
               AND ce.cedula='${req.params.id}' 
               AND ce.id_tipos = tip.id_tipos
               AND ce.id_territorios = t.codigo
               AND ce.observacion_tipos = o.id_observaciontipo
               ORDER BY ce.hora ASC`
        result = await connection.execute(sql);
        let info = []
        if (result.rows.length > 0) {
            for(let i = 0; i < result.rows.length; i++) {
                let objson = {}
                objson.cienmil = result.rows[i][1]
                objson.cincuentamil = result.rows[i][2]
                objson.veintemil = result.rows[i][3]
                objson.diezmil= result.rows[i][4]
                objson.cincomil = result.rows[i][5]
                objson.dosmil = result.rows[i][6]
                objson.mil = result.rows[i][7]
                objson.totalmonedas = result.rows[i][8]
                objson.totalefectivo = result.rows[i][9]
                objson.fecha = result.rows[i][10]
                objson.tipo = result.rows[i][11]
                objson.puntoventa = result.rows[i][12]
                objson.observacion = result.rows[i][13]
                objson.hora = result.rows[i][14]
                objson.numerobolsa = result.rows[i][15]
                info.push(objson)
            }
            res.json({ status: true,  info });
        } 
        else
          res.json({ status: false, msg: config.notfoud });
    }
    catch (err) {
        res.status(500).json(err)
    } finally {
        if (connection) {
            try {
                await connection.close();
            }
            catch (err) {
                res.status(500).json(err)
            }
        }
    }
})

module.exports = router;