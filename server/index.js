
const express = require('express')
const morgan = require('morgan')
const session = require('express-session')
const redis = require('redis');
const redisStore = require('connect-redis')(session)
const client = redis.createClient()
const bodyParser = require('body-parser')
const cors = require('cors')
const helmet = require('helmet')
const moment = require('moment')
const app = express()
const http = require('http').Server(app)
const io = require('socket.io')(http)

let noventa = null
let setentaycinco = null

const { mongoose } = require("./database");
const CortesModel = require("./mongo/cortes");
const ConfigModel = require("./mongo/config");
const ConfigRangos = require("./mongo/rangos");

const initData = require("./mongo/initData")
const configEnv = require("./config");
const SSH = require('simple-ssh');
const fs = require('fs');
const clientScp = require('scp2');
const Rsync = require('rsync');

const dbConfig = require('./keys');
const config = require('./configuration');
const configApp = require('./cortes-config');
const oracledb = require('oracledb');

var dataTransport = []
var dataTransportMonitoreo = []

//configApp.getPuntos() //primero registra los puntos.
//configApp.syncPuntos() //segundo se lanza para syncronizar

app.use(
  session({
    secret: "74unnbbr*?&/··,!?¿_*SASDAS@",
    store: new redisStore({ host: 'localhost', port: 6379, client: client, ttl: 260 }),
    saveUninitialized: true,
    resave: false,
    cookie: { maxAge: 60 * 60 * 1000 }
  })
);

app.all('/*', function (req, res, next) {
  res.header('Access-Control-Allow-Origin', '*');
  res.header('Access-Control-Allow-Headers',
    'Content-Type,X-Requested-With,cache-control,pragma');
  next();
});

//app.use(express.json({ limit: "50mb" }))
app.use(express.urlencoded({ limit: "50mb", extended: true }))
app.use(helmet())

io.on('connection', (socket) => {
  console.log("usuario conectado " + socket.id);

  var address = socket.handshake.address;
  console.log('New connection from ' + address);
  /*console.log('socket.client.conn.remoteAddress', socket.client.conn.remoteAddress);
    console.log('socket.request.connection.remoteAddress', socket.request.connection.remoteAddress);
    console.log('socket.handshake.address', socket.handshake.address);*/
	
	
  socket.on("disconnect", () => {
    console.log("usuario desconectado " + socket.id);
  });

  socket.on("efectivo", data => {
    saveEfectivo(data,address)
    setTimeout(() => {
      io.sockets.emit('semaforo')
    },3000)
  })

  socket.on("printcorte", data => {
    reeprint(data,address)
  })

  socket.on("printcorteCE", data => {
    reeprintCE(data,address)
  })

  socket.on("printerPagoPremio", data => {
    printerPagoPremio(data,address)
  })

  socket.on("printerPlanilla", data => {
    printerPlanilla(data,address)
  })

  socket.on("getSums", data => {
    getTransaciones(data.iduser)
    setTimeout(() => {
      io.sockets.emit('getSums', { idUser: data.iduser, dataTransport })
      dataTransport = []
    }, 1000);
  })

  socket.on("notificar", data => {
    saveRecogida(data)
    setTimeout(() => {
      io.sockets.emit('semaforo')
    }, 3000)
  })

  socket.on("rguarda", data => {
    dataTransportMonitoreo = [] 
    getRecogidasByGuarda(data)
    setInterval(() => {
      io.sockets.emit('rguarda', { id: data, dataTransportMonitoreo })
    }, 3000);
  })

  socket.on("monitoreozonas", data => {
    getMonitoreoByZonas()
  })

  socket.on("semaforo", data => {
    io.sockets.emit('semaforo')
  })


  socket.on("getObsGD", async (data) => {
   
    let connection, sql, result
    connection = await oracledb.getConnection(dbConfig);
    sql = `SELECT * FROM  APPWEB.CE_SEGURIDADAGENDACAMBIOS
           WHERE IDAGENDA = ${data.idagenda} AND DOCUMENTO = ${data.documento}`
    result = await connection.execute(sql)
    if (result.rows.length > 0) {
        let info = []
        for(let i = 0; i < result.rows.length; i++) {
            let objson = {}
            objson.idagenda = result.rows[i][0]
            objson.documento = result.rows[i][1]
            objson.idplaca = result.rows[i][2]
            objson.idruta = result.rows[i][3]
            objson.obs = result.rows[i][4]
            objson.fecha= result.rows[i][5]
            objson.tipocambio= result.rows[i][6]
            info.push(objson)
        }
        setTimeout(() => {
         io.sockets.emit('getObsGD', { data: info, documento: data.documento })
        }, 1000);
    }else
      io.sockets.emit('getObsGD', { data: [], documento: data.documento })
   })

   socket.on("enable", async (data) => {
      let ID = [...new Set(data)]
      let connection, sql
        connection = await oracledb.getConnection(dbConfig);
        ID.forEach(i => {
            sql = `UPDATE APPWEB.CE_SEGURIDADCALENDARIO 
                 SET ESTADO=2 
                 WHERE id_seguridadcalendario=:idagenda`;

            connection.execute(sql, 
            { idagenda: i },
            { autoCommit: true })
        })
   }) 
})

app.use(morgan('dev'))
app.use(cors())
app.use(express.json({ limit: "50mb" }));
app.use(bodyParser.urlencoded({ extended: false }))
app.use(bodyParser.json())

app.use('/api/cortes', require('./routes/cortes'))
app.use('/api/login', require('./routes/login'))
app.use('/api/guardas', require('./routes/guardas'))
app.use('/api/recibe', require('./routes/recibe'))
app.use('/api/pagos', require('./routes/pagos'))
app.use('/api/tesoreria', require('./routes/tesoreria'))
app.use('/api/tipos', require('./routes/tipos'))
app.use('/api/puntos', require('./routes/puntos'))
app.use('/api/seguridad', require('./routes/seguridad'))
app.use('/api/programacion', require('./routes/programacion'))
app.use('/api/recogidas', require('./routes/recogidas'))
app.use('/api/semaforo', require('./routes/config.routes'))
app.use('/api/vehiculo', require('./routes/vehiculos'))
app.use('/api/cefectivo', require('./routes/cefectivo'))


http.listen(configEnv.port, () => {
  console.log("Server on port ", configEnv.port)
});


async function saveEfectivo(data,address) {
  let connection, sql, novedad, dateNow,  result, nmCorte = 1
  try {
    connection = await oracledb.getConnection(dbConfig);
    sql = `SELECT TO_CHAR (SYSDATE, 'DD/MM/RRRR') "NOW" FROM DUAL`
    result = await connection.execute(sql);
    dateNow = result.rows[0]
    
    if(data.obsnovedad != null)
      novedad = data.obsnovedad
    
    let fecha = new Date();
    let hora = fecha.getHours()+":"+fecha.getMinutes()+":"+fecha.getSeconds();
    let fechaPrinter = ""
    fechaPrinter = dateNow
    result = await connection.execute(`INSERT INTO APPWEB.CE_CONTROLEFECTIVO VALUES (
      :id_controlefectivo,:cedula,
      :numero_corte, :billete_cincuenta,
      :billete_veinte, :billete_diez,
      :billete_cinco, :billete_dos,
      :billete_mil, :total_moneda,
      :total_efectivo, 
      :fecha,
      :id_tipos, :id_territorios,
      :observacion_tipos, :observacion_detalle1,
      :observacion_detalle2,:hora,:billete_cienmil,
      :recogido,
      :valorverficacion,
      :obsverificacion,
      :numerobolsa)`,
    [ null, data.iduser, 0, 
      parseInt(data.cincuentamil),parseInt(data.veintemil), 
      parseInt(data.diezmil), parseInt(data.cincomil),
      parseInt(data.dosmil), parseInt(data.mil), 
      parseInt(data.tolamonedas),parseInt(data.totalefectivo), 
      fecha, parseInt(data.relacion), 
      parseInt(data.territorio), data.observacion,
      novedad, novedad , hora, parseInt(data.ciemil), 0, 
      parseInt(data.totalefectivo), 0 , data.numerobolsa], 
      { autoCommit: true } );
      if(result.rowsAffected == 1)
      {
        saveMongoDB(data.territorio,data.totalefectivo)
        printerCorte(data,fechaPrinter,hora,nmCorte,address)
      }
  }
  catch (err) {
  } finally {
    if (connection) {
      try {
        await connection.close();
      }
      catch (err) {
      }
    }
  }
}

function saveMongoDB(punto,total)
{
  ConfigModel.findOne( { territorio: punto }, (err, response)=>{
    if(response){
        CortesModel.findOne( { punto: response.territorio },(err, ptv)=>{
          if(ptv){
            ptv.totalcortes =  ptv.totalcortes + total 
            ptv.save()
          }
          else
          {
            const corte = new CortesModel({
              punto: punto,
              totalcortes: total
            });
            corte.save()
          }
        })
    }
    //semaforo()
  })
}

async function getConfigRangos(){
  const rangos = await ConfigRangos.find()
  topeB = rangos[0].topeB
  topeA = rangos[0].topeA
  noventa = rangos[0].noventa
  setentaycinco = rangos[0].setentaycinco
}

async function semaforo()
{
  let porcentaje = 0
  const puntos = await  CortesModel.find()
  if(puntos.length > 0)
  {
     //puntos almacenados en la configuracion
     const congfigPuntos = await ConfigModel.find()
     for(let k = 0; k < congfigPuntos.length; k++)
     {
       //console.log(" puntos ", puntos )
      for (let i = 0; i < puntos.length; i++)
      {
        if(puntos[i].punto == congfigPuntos[k].territorio){
         if(congfigPuntos[k].tipo == config.tipoB)
          {
            

             porcentaje = parseInt((puntos[i].totalcortes*100)/config.topeB)
             if(porcentaje >= noventa)
             {
              congfigPuntos[k].recogida = 'red';
             }

             if(porcentaje <= setentaycinco)
             {
              congfigPuntos[k].recogida = 'yellow';
             }
          }
          else
          {
              porcentaje = (puntos[i].totalcortes * 100 )/config.topeA
              if(porcentaje >= noventa)
              {
                congfigPuntos[k].recogida = 'red';
              }
              else if(porcentaje >= setentaycinco || porcentaje < noventa)
              {
                congfigPuntos[k].recogida = 'yellow';
              }
          }
        }
      }
     }
     io.sockets.emit('semaforo', { congfigPuntos })
  }
}

async function removeMongoDB(punto)
{
  const congfigPuntos = await ConfigModel.find()
  await CortesModel.deleteOne({ punto:punto })
}

async function saveRecogida(data)
{
  let connection, sql, result, dateNow
  try {
    connection = await oracledb.getConnection(dbConfig);
    let observacion = null
    if (data.observacion != null)
      observacion = data.observacion

    let fecha = new Date();
    let hora = fecha.getHours()+":"+fecha.getMinutes()+":"+fecha.getSeconds();
    result = await connection.execute(
        `INSERT INTO appweb.ce_seguridaddescarga VALUES (
        :ID_SEGURIDADDESCARGA,
        :RECOLECTOR,
        :VEEDOR,
        :CANDADO,
        :TULA,
        :ID_TERRITORIO,
        :ID_SEGURIDADNOVEDAD,
        :NOVEDAD,
        :FECHA,
        :HORA,
        :CANTIDAD,
        :ESTADO)`,
      [ null, 
        data.recogida.recolector, 
        data.recogida.veedor, 
        data.recogida.candado,
        data.recogida.tula, 
        parseInt(data.territorio), 
        parseInt(data.recogida.novedad),
        observacion,
        fecha, 
        hora, 
        parseInt(data.recogida.cantidad),
        1
      ],
      { autoCommit: true })

      if(result.rowsAffected == 1)
      {
        connection = await oracledb.getConnection(dbConfig)
        sql = `UPDATE APPWEB.CE_CONTROLEFECTIVO SET
                RECOGIDO=1
                WHERE CEDULA=:cedula 
                AND RECOGIDO = 0 
                AND TO_DATE(FECHA,'DD/MM/RRRR')=TO_DATE(SYSDATE,'DD/MM/RRRR')`
        await connection.execute(sql, 
          { cedula:data.recogida.veedor },
          { autoCommit: true})
          
        removeMongoDB(data.territorio)
       
      }
  }
  catch (err) {
    console.log(1,err);
  } finally {
    if (connection) {
      
      try {
        await connection.close();
      }
      catch (err) {
        console.log(1,err);
      }
    }
 }
  
}

async function getTransaciones(cedula) {
  let connection, result, sql, countchance, countgiro, countpagos, ventagamble;
  try {
    connection = await oracledb.getConnection(dbConfig);
    sql = `SELECT count(*) FROM APPWEB.ce_controlefectivo WHERE ID_TIPOS=2 AND TO_DATE(FECHA,'DD/MM/RRRR')=TO_DATE(SYSDATE,'DD/MM/RRRR') AND CEDULA='${cedula}'`
    result = await connection.execute(sql);
    if (result.rows.length > 0) {
      countgiro = result.rows
    }

    sql = `SELECT SUM(total_efectivo) FROM 
           APPWEB.ce_controlefectivo WHERE ID_TIPOS=2  AND TO_DATE(FECHA,'DD/MM/RRRR')=TO_DATE(SYSDATE,'DD/MM/RRRR')
           AND CEDULA='${cedula}'`
    result = await connection.execute(sql);
    if (result.rows.length > 0) {
      dataTransport.push({ giros: result.rows, countgiro: countgiro })
    }
    else
      dataTransport.push({ giros: 0 })

    sql = `SELECT count(*) FROM APPWEB.ce_controlefectivo WHERE ID_TIPOS=1 AND TO_DATE(FECHA,'DD/MM/RRRR')=TO_DATE(SYSDATE,'DD/MM/RRRR') AND CEDULA='${cedula}'`
    result = await connection.execute(sql);
    if (result.rows.length > 0) {
      countchance = result.rows
    }

    sql = `SELECT SUM(total_efectivo) FROM 
           APPWEB.ce_controlefectivo WHERE ID_TIPOS=1  ANDTO_DATE(FECHA,'DD/MM/RRRR')=TO_DATE(SYSDATE,'DD/MM/RRRR')
           AND CEDULA='${cedula}'`
    result = await connection.execute(sql);
    if (result.rows.length > 0) {
      dataTransport.push({ chance: result.rows, countchance: countchance })
    }
    else
      dataTransport.push({ chance: 0 })

    sql = `SELECT count(*) FROM 
    APPWEB.ce_pagoacumulado WHERE CEDULA='${cedula}' AND TO_DATE(FECHA,'DD/MM/RRRR')=TO_DATE(SYSDATE,'DD/MM/RRRR') AND estado='pagado'`
    result = await connection.execute(sql);
    if (result.rows.length > 0) {
      countpagos = result.rows
    }

    sql = `SELECT SUM(valor) FROM 
    APPWEB.ce_pagoacumulado WHERE CEDULA='${cedula}' AND TO_DATE(FECHA,'DD/MM/RRRR')=TO_DATE(SYSDATE,'DD/MM/RRRR') AND estado='pagado'`
    result = await connection.execute(sql);
    if (result.rows.length > 0) {
      dataTransport.push({ pagos: result.rows, countpagos: countpagos })
    }
    else
      dataTransport.push({ pagos: 0 })

    sql =`SELECT  SUM(ventabruta) from  v_totalventasnegocio@chile 
           WHERE TO_DATE(FECHA,'DD/MM/RRRR') = TO_DATE(SYSDATE,'DD/MM/RRRR') and persona ='${cedula}'`
    result = await connection.execute(sql);
    if (result.rows.length > 0) {
      ventagamble = result.rows
      dataTransport.push({ ventagamble: ventagamble })
    }

  }
  catch (err) {
    
  } finally {
    if (connection) {
      try {
        await connection.close();
      }
      catch (err) {
      }
    }
  }
}

async function getMonitoreoByZonas() {
  let connection, result = null, sql

  dataTransportMonitoreo.pop()
  try { 
    connection = await oracledb.getConnection(dbConfig);
    sql = `SELECT sum(ce.total_efectivo), ce.id_territorios, t.nombre, ce.recogido
            FROM appweb.ce_controlefectivo ce, gamble.territorios t
            WHERE ce.recogido = 1
            AND ce.id_territorios = t.codigo
            AND ce.id_territorios IN 
            (
            SELECT u.trtrio_codigo  
            FROM gamble.ubicacionnegocios u
            WHERE u.trtrio_codigo_compuesto_de = 1087
            )
            GROUP BY ce.id_territorios, ce.id_territorios, t.nombre, ce.recogido
            ORDER BY sum(ce.total_efectivo) DESC`
    result = await connection.execute(sql);
    if(result.rows.length > 0) {
       let info = []
       for(let i = 0; i < result.rows.length; i++) {
          let data = {}
          data.totalefectivo = result.rows[i][0]
          data.idterritorio = result.rows[i][1]
          data.nombre = result.rows[i][2]
          data.recogido = result.rows[i][3]
          info.push(data)
      }
     dataTransportMonitoreo.push({ zona:1087, puntos: info })
    }

    sql = `SELECT sum(ce.total_efectivo), ce.id_territorios, t.nombre, ce.recogido
          FROM appweb.ce_controlefectivo ce, gamble.territorios t
          WHERE ce.recogido = 1
          AND ce.id_territorios = t.codigo
          AND ce.id_territorios IN (
          SELECT u.trtrio_codigo  
          FROM gamble.ubicacionnegocios u
          WHERE u.trtrio_codigo_compuesto_de = 1125
          )
          GROUP BY ce.id_territorios, ce.id_territorios, t.nombre, ce.recogido
          ORDER BY sum(ce.total_efectivo) DESC`
    result = await connection.execute(sql);
    if(result.rows.length > 0) {
      let info = []
      for(let i = 0; i < result.rows.length; i++) {
         let data = {}
         data.totalefectivo = result.rows[i][0]
         data.idterritorio = result.rows[i][1]
         data.nombre = result.rows[i][2]
         data.recogido = result.rows[i][3]
         info.push(data)
     } 
     dataTransportMonitoreo.push({ zona:1125, puntos: info })
    }

    sql = `SELECT sum(ce.total_efectivo), ce.id_territorios, t.nombre, ce.recogido
            FROM appweb.ce_controlefectivo ce, gamble.territorios t
            WHERE ce.recogido = 1
            AND ce.id_territorios = t.codigo
            AND ce.id_territorios IN (
            SELECT u.trtrio_codigo  
            FROM gamble.ubicacionnegocios u
            WHERE u.trtrio_codigo_compuesto_de = 1152
            )
            GROUP BY ce.id_territorios, ce.id_territorios, t.nombre, ce.recogido
            ORDER BY sum(ce.total_efectivo) DESC`
    result = await connection.execute(sql);
    if(result.rows.length > 0) {
      let info = []
      for(let i = 0; i < result.rows.length; i++) {
         let data = {}
         data.totalefectivo = result.rows[i][0]
         data.idterritorio = result.rows[i][1]
         data.nombre = result.rows[i][2]
         data.recogido = result.rows[i][3]
         info.push(data)
     }
     dataTransportMonitoreo.push({ zona:1152, puntos: info })
    }

    if(dataTransportMonitoreo.length>0){
      io.sockets.emit('monitoreozonas', { dataTransportMonitoreo })
    }

  }
  catch (err) {
    
  } finally {
    if (connection) {
      try {
        await connection.close();
      }
      catch (err) {
      }
    }
  }
}


async function getMonitoreoByZonas() {
  let connection, result = null, sql

  dataTransportMonitoreo.pop()
  try {
    connection = await oracledb.getConnection(dbConfig);
    sql = `SELECT sum(ce.total_efectivo), ce.id_territorios, t.nombre, ce.recogido
            FROM appweb.ce_controlefectivo ce, gamble.territorios t
            WHERE ce.recogido IN(1,2)
            AND ce.id_territorios = t.codigo
            AND ce.id_territorios IN 
            (
            SELECT u.trtrio_codigo  
            FROM gamble.ubicacionnegocios u
            WHERE u.trtrio_codigo_compuesto_de = 1087
            )
            GROUP BY ce.id_territorios, ce.id_territorios, t.nombre, ce.recogido
            ORDER BY sum(ce.total_efectivo) DESC`
    result = await connection.execute(sql);
    if(result.rows.length > 0) {
       let info = []
       for(let i = 0; i < result.rows.length; i++) {
          let data = {}
          data.totalefectivo = result.rows[i][0]
          data.idterritorio = result.rows[i][1]
          data.nombre = result.rows[i][2]
          data.recogido = result.rows[i][3]
          info.push(data)
      }
     dataTransportMonitoreo.push({ zona:1087, puntos: info })
    }

    sql = `SELECT sum(ce.total_efectivo), ce.id_territorios, t.nombre, ce.recogido
          FROM appweb.ce_controlefectivo ce, gamble.territorios t
          WHERE ce.recogido IN(1,2)
          AND ce.id_territorios = t.codigo
          AND ce.id_territorios IN (
          SELECT u.trtrio_codigo  
          FROM gamble.ubicacionnegocios u
          WHERE u.trtrio_codigo_compuesto_de = 1125
          )
          GROUP BY ce.id_territorios, ce.id_territorios, t.nombre, ce.recogido
          ORDER BY sum(ce.total_efectivo) DESC`
    result = await connection.execute(sql);
    if(result.rows.length > 0) {
      let info = []
      for(let i = 0; i < result.rows.length; i++) {
         let data = {}
         data.totalefectivo = result.rows[i][0]
         data.idterritorio = result.rows[i][1]
         data.nombre = result.rows[i][2]
         data.recogido = result.rows[i][3]
         info.push(data)
     }
     dataTransportMonitoreo.push({ zona:1125, puntos: info })
    }

    sql = `SELECT sum(ce.total_efectivo), ce.id_territorios, t.nombre, ce.recogido
            FROM appweb.ce_controlefectivo ce, gamble.territorios t
            WHERE ce.recogido IN(1,2)
            AND ce.id_territorios = t.codigo
            AND ce.id_territorios IN (
            SELECT u.trtrio_codigo  
            FROM gamble.ubicacionnegocios u
            WHERE u.trtrio_codigo_compuesto_de = 1152
            )
            GROUP BY ce.id_territorios, ce.id_territorios, t.nombre, ce.recogido
            ORDER BY sum(ce.total_efectivo) DESC`
    result = await connection.execute(sql);
    if(result.rows.length > 0) {
      let info = []
      for(let i = 0; i < result.rows.length; i++) {
         let data = {}
         data.totalefectivo = result.rows[i][0]
         data.idterritorio = result.rows[i][1]
         data.nombre = result.rows[i][2]
         data.recogido = result.rows[i][3]
         info.push(data)
     }
     dataTransportMonitoreo.push({ zona:1152, puntos: info })
    }

  
    if(dataTransportMonitoreo.length>0){
      io.sockets.emit('monitoreozonas', { dataTransportMonitoreo })
    }

  }
  catch (err) {
    
  } finally {
    if (connection) {
      try {
        await connection.close();
      }
      catch (err) {
      }
    }
  }
}

async function getRecogidasByGuarda(id)
{
  let connection, sql, result,  info = []
    try{
       
        connection = await oracledb.getConnection(dbConfig);
        sql = `SELECT sgt.RECOLECTOR,sgt.id_territorio,t.NOMBRE, sgt.TULA, sgt.CANDADO, sgt.CANTIDAD, sgt.novedad
              FROM APPWEB.CE_SEGURIDADDESCARGA sgt, gamble.territorios t 
              WHERE sgt.FECHA =to_date(SYSDATE,'DD/MM/RRRR')
              and sgt.RECOLECTOR = '${"CP"+id}' AND sgt.id_territorio = t.codigo AND sgt.id_territorio IN 
              (
                SELECT u.trtrio_codigo FROM gamble.ubicacionnegocios 
                u WHERE u.trtrio_codigo_compuesto_de = u.trtrio_codigo_compuesto_de 
             )
             GROUP BY sgt.RECOLECTOR,sgt.ID_TERRITORIO,t.nombre,sgt.TULA,sgt.CANDADO,sgt.CANTIDAD,sgt.novedad ORDER BY t.nombre`
        result = await connection.execute(sql);
        if (result.rows.length > 0) {
            for(let i = 0; i < result.rows.length; i++) {
                let objson = {}
                objson.recolector = result.rows[i][0]
                objson.id_territorio = result.rows[i][1]
                objson.nombre = result.rows[i][2]
                objson.tula = result.rows[i][3]
                objson.candado = result.rows[i][4]
                objson.cantidad= result.rows[i][5]
                objson.novedad= result.rows[i][6]
                objson.observacion= result.rows[i][7]
                info.push(objson)
            }
            dataTransportMonitoreo = info
        } 
    }
    catch (err) {
    } finally {
        if (connection) {
            try {
                await connection.close();
            }
            catch (err) {
            }
        }
    }
}



async function printerCorte(data, fechaPrinter, hora, nmCorte, address)
{

    fechaPrinter = fechaPrinter.toString()
    var printer = ''

    if(data.relacion == 1){
        data.relacion = "CHANCE"
    }else
      data.relacion = "GIRO"

    printer += '      INVERSIONES DEL PACIFICO S.A. \n';
    printer += '  \t      NIT: 835000235-3  \n';
    printer += '  \t   CONTROL DE EFECTIVO \n';
    printer += '     FECHA: '+fechaPrinter+' HORA: '+hora+'\n';
    printer += '  \t       '+data.iduser+' \n';
    printer += '  \t TIPO DE RELACION: '+data.relacion+'\n';
    printer +='\nBilletes $ 100.000: '+data.ciemil+'';
    printer +='\nBilletes $ 50.000:  '+data.cincuentamil+'';
    printer +='\nBilletes $ 20.000:  '+data.veintemil+'';
    printer +='\nBilletes $ 10.000:  '+data.diezmil+'';
    printer +='\nBilletes $ 5.000:   '+data.cincomil+'';
    printer +='\nBilletes $ 2.000:   '+data.dosmil+'';
    printer +='\nBilletes $ 1.000:   '+data.mil+'';
    printer +='\nTotal Monedas :     '+formatNumber(data.tolamonedas,"$")+'';
    printer +='\nTotal Efectivo :    '+formatNumber(data.totalefectivo,"$")+'';
    printer +='\nNumero de Bolsa :   '+data.numerobolsa+'';
    printer +='\n\n';
    printer +='\t      ORIGINAL';
    printer +='\n    Todos Los Derechos Reservados';
    printer +='\n\n';
    printer +='\n\n';
    printer +='\n\n';
    printer +='\n\n';

	
    address = address.replace("::ffff:","")
    console.log(" ip ", address )
    fs.writeFile(`corte${data.iduser}.txt`,printer,function (err) {
      if (err) throw err;
    });
    setTimeout(() => {
      clientScp.scp(`corte${data.iduser}.txt`, {
        host: address,
        username: 'gamble',
        password: 'gamble',
        path: '/home/gamble'
    }, function(err) {
       if(err){
          console.log('There has been some error!!!');
          console.log(err);
       }else{
        const ssh = new SSH({
          host: address,
          user: 'gamble',
          pass: 'gamble'
        });
      
        ssh.exec(`lpr /home/gamble/corte${data.iduser}.txt`, {
            out: function(stdout) {
          }
        }).start()
          console.log('succeeded copying the file to remote server');   
       }
    });
    }, 2000);  
    clientScp.close()
}

async function reeprintCE(data,address){
  
  data.item.fecha = moment(data.item.fecha).format('DD/MM/YYYY') 
  var printerCE = ''
  printerCE += '      INVERSIONES DEL PACIFICO S.A. \n';
  printerCE += '  \t      NIT: 835000235-3  \n';
  printerCE += '  \t   CONTROL DE EFECTIVO \n';
  printerCE += '     FECHA: '+data.item.fecha+' HORA: '+data.item.hora+' \n';
  printerCE += '  \t       '+data.item.iduser+' \n';
  printerCE += '  \t TIPO DE RELACION: '+data.item.tipo+'\n';
  printerCE +='\nNumero de Corte '+data.corte+'';
  printerCE +='\nBilletes $ 100.000: '+data.item.cienmil+'';
  printerCE +='\nBilletes $ 50.000:  '+data.item.cincuentamil+'';
  printerCE +='\nBilletes $ 20.000:  '+data.item.veintemil+'';
  printerCE +='\nBilletes $ 10.000:  '+data.item.diezmil+'';
  printerCE +='\nBilletes $ 5.000:   '+data.item.cincomil+'';
  printerCE +='\nBilletes $ 2.000:   '+data.item.dosmil+'';
  printerCE +='\nBilletes $ 1.000:   '+data.item.mil+'';
  printerCE +='\nTotal Monedas :     '+formatNumber(data.item.totalmonedas,"$")+'';
  printerCE +='\nTotal Efectivo :    '+formatNumber(data.item.totalefectivo,"$")+'';
  printerCE +='\nNumero de Bolsa :   '+data.item.numerobolsa+'';
  printerCE +='\n';
  printerCE +='\n    Todos Los Derechos Reservados';
  printerCE +='\t\t    REIMPRESION';
  printerCE +='\n\n';
  printerCE +='\n\n';
  printerCE +='\n\n';
  printerCE +='\n\n';
  printerCE +='\n\n';

  const escpos = require('escpos');
  escpos.USB = require('escpos-usb');
  const device  = new escpos.USB();
  const options = { encoding: "GB18030" }
  const printer = new escpos.Printer(device, options)
  
  device.open(function(error){
      printer
     .size(0)
     .encode('utf8')
     .text(printerCE)
     .cut()
     .close()
  })
}


async function reeprint(data,address){
  
  data.item.fecha = moment(data.item.fecha).format('DD/MM/YYYY') 
  var printer = ''
  printer += '      INVERSIONES DEL PACIFICO S.A. \n';
  printer += '  \t      NIT: 835000235-3  \n';
  printer += '  \t   CONTROL DE EFECTIVO \n';
  printer += '     FECHA: '+data.item.fecha+' HORA: '+data.item.hora+' \n';
  printer += '  \t       '+data.item.iduser+' \n';
  printer += '  \t TIPO DE RELACION: '+data.item.tipo+'\n';
  printer +='\nNumero de Corte '+data.corte+'';
  printer +='\nBilletes $ 100.000: '+data.item.cienmil+'';
  printer +='\nBilletes $ 50.000:  '+data.item.cincuentamil+'';
  printer +='\nBilletes $ 20.000:  '+data.item.veintemil+'';
  printer +='\nBilletes $ 10.000:  '+data.item.diezmil+'';
  printer +='\nBilletes $ 5.000:   '+data.item.cincomil+'';
  printer +='\nBilletes $ 2.000:   '+data.item.dosmil+'';
  printer +='\nBilletes $ 1.000:   '+data.item.mil+'';
  printer +='\nTotal Monedas :     '+formatNumber(data.item.totalmonedas,"$")+'';
  printer +='\nTotal Efectivo :    '+formatNumber(data.item.totalefectivo,"$")+'';
  printer +='\nNumero de Bolsa :   '+data.item.numerobolsa+'';
  printer +='\n';
  printer +='\n    Todos Los Derechos Reservados';
  printer +='\t\t    REIMPRESION';
  printer +='\n\n';
  printer +='\n\n';
  printer +='\n\n';
  printer +='\n\n';
  printer +='\n\n';

  address = address.replace("::ffff:","")
  fs.writeFile(`rprinter${data.item.iduser}.txt`,printer,function (err) {
    if (err) throw err;
  });
 
setTimeout(() => {
  clientScp.scp(`rprinter${data.item.iduser}.txt`, {
    host: address,
    username: 'gamble',
    password: 'gamble',
    path: '/home/gamble'
  }, function(err) {
   if(err){
      console.log('There has been some er41ror!!!');
      console.log(err);
   }else{

    const ssh = new SSH({
      host: address,
      user: 'gamble',
      pass: 'gamble'
    });
  
    ssh.exec(`lpr /home/gamble/rprinter${data.item.iduser}.txt`, {
        out: function(stdout) {
      }
    }).start()
      console.log('succeeded copying the file to remote server');   
   }
 });
}, 1000)

clientScp.close()
   
}


async function printerPagoPremio(data,address)
{
  
    let fecha = new Date()
    let hora = fecha.getHours()+":"+fecha.getMinutes()+":"+fecha.getSeconds();
    let fechaPrinter = fecha.getDay()+"/"+fecha.getMonth()+"/"+fecha.getFullYear().toString()
    
    var printer = ''
    printer += '      INVERSIONES DEL PACIFICO S.A. \n';
    printer += '  \t      NIT: 835000235-3  \n';
    printer += '  \t   CONTROL DE EFECTIVO \n';
    printer += '     FECHA: '+fechaPrinter+' HORA: '+hora+'\n';
    printer += '  \t PAGO DE PREMIO: \n';
    printer +='\nREALIZADO POR : '+data.cedula+'';
    printer +='\nSERIE:  '+data.serie+'';
    printer +='\nNUMERO:  '+data.numero+'';
    printer +='\nTOTAL PREMIO $:  '+formatNumber(data.totalpremio,"$")+'';
    printer +='\n';
    printer +='\n    Todos Los Derechos Reservados';
    printer +='\n\n';

	
    address = address.replace("::ffff:","")
    
    fs.writeFile(`pagopremio${data.cedula}.txt`,printer,function (err) {
      if (err) throw err;
    });
    //setTimeout(() => {
      clientScp.scp(`pagopremio${data.cedula}.txt`, {
        host: address,
        username: 'gamble',
        password: 'gamble',
        path: '/home/gamble'
    }, function(err) {
       if(err){
          console.log('There has been some error!!!');
          console.log(err);
       }else{
        const ssh = new SSH({
          host: address,
          user: 'gamble',
          pass: 'gamble'
        });
        
        ssh.exec(`lpr /home/gamble/pagopremio${data.cedula}.txt`, {
            out: function(stdout) {
          }
        }).start()
          console.log('succeeded copying the file to remote server');   
       }
    });
   // }, 100);  
    clientScp.close()
}

async function printerPlanilla(data,address)
{ 
    let fecha = new Date()
    let hora = fecha.getHours()+":"+fecha.getMinutes()+":"+fecha.getSeconds();
    let fechaPrinter = moment(fecha).format('DD/MM/YYYY') 

    var printer = ''
    printer += '      INVERSIONES DEL PACIFICO S.A. \n';
    printer += '  \t      NIT: 835000235-3  \n';
    printer += '  \t   CONTROL DE EFECTIVO \n';
    printer += '     FECHA: '+fechaPrinter+' HORA: '+hora+' \n';
    printer += '  \t       '+data.iduser+' \n';
    printer +='\n';
    printer += '\t   PLANILLA DE REGISTROS \n';
    printer +='\n # Cortes: '+data.cortes+'';
    printer +='\n Total Cortes: '+formatNumber(data.totalcortes,"$")+'';
    printer +='\n # Premios:  '+data.premios+'';
    printer +='\n Total Premios:  '+formatNumber(data.totalpremios,"$")+'';
    printer +='\n';
    printer +='\n    Todos Los Derechos Reservados';
    printer +='\n\n';
    printer +='\n\n';
    printer +='\n\n';
    printer +='\n\n';
   
    address = address.replace("::ffff:","")
    fs.writeFile(`planilla${data.iduser}.txt`,printer,function (err) {
      if (err) throw err;
    });
    setTimeout(() => {
      clientScp.scp(`planilla${data.iduser}.txt`, {
        host: address,
        username: 'gamble',
        password: 'gamble',
        path: '/home/gamble'
    }, function(err) {
       if(err){
          console.log('There has been some error!!!');
          console.log(err);
       }else{
        const ssh = new SSH({
          host: address,
          user: 'gamble',
          pass: 'gamble'
        });
      
        ssh.exec(`lpr /home/gamble/planilla${data.iduser}.txt`, {
            out: function(stdout) {
          }
        }).start()
          console.log('succeeded copying the file to remote server');   
       }
    });
    }, 100); 
    clientScp.close()
}

function formatNumber(num, prefix) {
    prefix = prefix || '';
    num += '';
    var splitStr = num.split('.');
    var splitLeft = splitStr[0];
    var splitRight = splitStr.length > 1 ? '.' + splitStr[1] : '';
    var regx = /(\d+)(\d{3})/;
    while (regx.test(splitLeft)) {
      splitLeft = splitLeft.replace(regx, '$1' + '.' + '$2');
    }
    return prefix + splitLeft + splitRight;
 }
