const mongoose = require('mongoose')

const config = require('./config')

 mongoose
 .connect (config.Urldb, {
 useUnifiedTopology: true,
 useNewUrlParser: true,
 })
 .then (() => console.log ('DB Connected!'))
 .catch (err => {
 console.log(`DB Connection Error: ${err.message}`);
 });

module.exports = mongoose
