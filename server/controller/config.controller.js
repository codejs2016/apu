const configCtrl = {};

const mongoose = require("mongoose");

const Config = require("../mongo/config");
const Cortes = require("../mongo/cortes");
const ConfigRangos = require("../mongo/rangos");
const configTopes = require('../configuration');
let noventa = null
let setentaycinco = null


configCtrl.semaforo = async (req, res) => {
  let cgM = await Config.find()
   if(cgM.length > 0){
    let cortes = await  Cortes.find()
    for (let p = 0; p < cgM.length; p++) {
      cgM[p].valor = 0
      for (let k = 0; k < cortes.length; k++) {
        if(cgM[p].territorio == cortes[k].punto)
        {
            cgM[p].valor = cortes[k].totalcortes
            if(cgM[p].tipo == configTopes.tipoB)
            {
              cgM[p].porcentaje = parseInt((cortes[k].totalcortes*100)/configTopes.topeB)
              if(cgM[p].porcentaje >= configTopes.noventa)
              {
                  cgM[p].recogida = 'red'
              }

              if(cgM[p].porcentaje <= configTopes.setentaycinco)
              {
                cgM[p].recogida = 'yellow'
              }
            }
            else{
              cgM[p].porcentaje =  (cortes[k].totalcortes * 100 )/configTopes.topeA
              if(cgM[p].porcentaje >= configTopes.noventa)
              {
                cgM[p].recogida = 'red'
              }
              else if(cgM[p].porcentaje >= configTopes.setentaycinco || cgM[p].porcentaje < configTopes.noventa)
              {
                cgM[p].recogida = 'yellow'
              }
            }
          }
       }
    }//close fors 
   }
   res.json(cgM)
};


configCtrl.getPuntos = async (req, res) => {
  const ptvs = await Config.find().limit(5);
  res.json(ptvs);
};

configCtrl.search = async (req, res) => {
  const puntos = await Config.find({
    nombre: { $regex: ".*" + req.body.name.toUpperCase() + ".*" }
  });
  if (puntos) {
    res.status(200).send({ puntos, status: true });
  } else {
    res.json({
      status: false
    });
  }
};

configCtrl.create = async (req, res) => {
  const ptv = new Config({
    territorio: req.body.territorio,
    nombre: req.body.nombre,
    ccosto: req.body.ccosto,
    tipo: req.body.tipo
  });
  await ptv.save();
  res.json({
    status: true
  });
};

configCtrl.update = async (req, res)=>{
  await Config.findOne( { territorio: req.body.territorio }, function(err, ptv)
  {
      ptv.tipo =  req.body.tipo;
      ptv.save();
  })
  res.json({
    status: true
  });
};


configCtrl.updateRango = async (req, res)=>{
 await ConfigRangos.findOneAndUpdate({ _id: req.body.id }, 
  { 
    topeB : parseInt(req.body.topeB),
	  topeA : parseInt(req.body.topeA),
	  noventa : parseInt(req.body.noventa),
	  setentaycinco : parseInt(req.body.setentaycinco),
  })

  res.json({
    status: true
  });
};

configCtrl.getRangos = async (req, res) => {
  const rangos = await ConfigRangos.find();
  res.json(rangos);
};

module.exports = configCtrl;
