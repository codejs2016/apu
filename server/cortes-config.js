const cron = require('node-cron')
const notifier = require('node-notifier')
const path = require('path')
const ConfigModel = require("./mongo/config");
const PuntosModel = require("./mongo/ptv");
const dbConfig = require('./keys');
const config = require('./configuration');
const oracledb = require('oracledb');
const { mongoose } = require("./database");

async function notificationCorte()
{
   cron.schedule('* * * * *', () => {
    notifier.notify({
        title: 'Soporte TI',
        message: 'Por favor registrar efectivo',
        icon: path.join(__dirname, 'logo.png'), 
        sound: true, 
        wait: true 
       });
   });
}


async function syncPuntos()
{
 //cron.schedule('* * * * *', () => {
 console.log("sincronizando syncPuntos...");

  let connection, sql, result
  try 
  {
    connection = await oracledb.getConnection(dbConfig);
    sql = `SELECT t.codigo as territorio,nombre,
            TO_CHAR( u.FECHAFINAL, 'YYYY/MM/DD') FECHAFINAL,
            U.TRTRIO_CODIGO_COMPUESTO_DE ccosto
            FROM GAMBLE.territorios@chile t,GAMBLE.ubicacionnegocios@chile u
            WHERE t.codigo = u.trtrio_codigo
            AND u.trtrio_codigo_compuesto_de in(1087,1125,1152,1221,1250,1193)
            AND t.codigo not in(1902,1213,1607,1603)
            order by u.trtrio_codigo_compuesto_de,t.nombre`
    result = await connection.execute(sql);
    let info = []
    if(result.rows.length > 0)
    {
       for(let i = 0; i < result.rows.length; i++) 
       {
          let data = {}
          data.territorio = result.rows[i][0]
          data.nombre = result.rows[i][1]
          data.fechafinal = result.rows[i][2]
          data.ccosto = result.rows[i][3]
          info.push(data)
       }

     

       if(info.length > 0)
       {
         ConfigModel.find({ status:true}, function(err, configPuntos){
            if(configPuntos)
            {
               for(let k = 0; k < configPuntos.length; k++)
               {
                  for (let j = 0; j < info.length; j++)
                  {
                     if(parseInt(configPuntos[k].territorio) == parseInt(info[j].territorio) )
                     {
                       if(info[j].fechafinal != null){
                           console.log("actualizando puntos..");
                           configPuntos[k].nombre = info[j].nombre
                           configPuntos[k].fechafinal = info[j].fechafinal
                           var conditions = { territorio: info[j].territorio }
                           , update = { $inc: { nombre:info[j].nombre,fechafinal: info[j].fechafinal,  }}
                           , options = { multi: true };
                           PuntosModel.update(conditions, update, options);
                        }
                        else
                        {
                           const puntos = new PuntosModel({
                              territorio:info[j].territorio,
                              nombre:info[j].nombre,
                              ccosto:info[j].ccosto,
                           });
                           puntos.save()
                        }
                     }
                  }
               }
            }
         });
       }
    }
  } 
  catch (err) {
     console.log(err);
  } finally {
    if (connection) {
      try {
         await connection.close();
      } 
     catch (err) {
      console.log(err);
     }
  }
  }
 //});
}


async function deletePuntos()
{
   await ConfigModel.deleteMany({})
}

async function getPuntos()
{
 console.log("registrando puntos iniciando...");
 let connection, sql, result
  try 
  {
    connection = await oracledb.getConnection(dbConfig);
    sql = `SELECT t.codigo as territorio,nombre,U.TRTRIO_CODIGO_COMPUESTO_DE ccosto
            FROM GAMBLE.territorios@chile t,GAMBLE.ubicacionnegocios@chile u
            WHERE t.codigo = u.trtrio_codigo
            AND u.trtrio_codigo_compuesto_de in(1087,1125,1152,1221,1250,1193)
            AND u.FECHAFINAL is null
            AND t.codigo not in(1902,1213,1607,1603)
            order by u.trtrio_codigo_compuesto_de,t.nombre`
    result = await connection.execute(sql);
    let info = []
    if(result.rows.length > 0){
       for(let i = 0; i < result.rows.length; i++) {
          let data = {}
          data.territorio = result.rows[i][0]
          data.nombre = result.rows[i][1]
          data.ccosto = result.rows[i][2]
          info.push(data)
      }

      if(info.length > 0){
         for (let j = 0; j < info.length; j++) {
            const syncptv = new ConfigModel({
               territorio:info[j].territorio,
               nombre:info[j].nombre,
               ccosto:info[j].ccosto,
               tipo: 'A',
             });
             syncptv.save();    
         }
      }
      console.log("tarea programada terminada...");
      
    }
  } 
  catch (err) {
     console.log(err);
  } finally {
    if (connection) {
      try {
         await connection.close();
      } 
     catch (err) {
      console.log(err);
     }
  }
  }
}

module.exports = 
{
    notificationCorte , 
    syncPuntos,
    getPuntos,
    deletePuntos
}


// module.exports = {
//     title:'Soporte TI',
//     message:'Por favor registrar efectivo',
//     second:'*', //0-59
//     minute:'*',//0-59,
//     hour:'*',//0-23,
//     day:'',// 1-31,
//     month:'',// 1-12 (or names)
//     dayofweek:''//	0-7 (or names, 0 or 7 are sunday)
// };

  

