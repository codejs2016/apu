const mongoose = require('mongoose')

const cortesSchema = new mongoose.Schema
({
    punto: Number,
    totalcortes: Number,
    status: { type:Boolean, default: true },
    fecha:{ type: Date, default: Date.now() }
})
module.exports = mongoose.model('cortes', cortesSchema )
