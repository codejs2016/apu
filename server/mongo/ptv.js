const mongoose = require('mongoose')

const puntosSchema = new mongoose.Schema
({
    territorio:Number,
    nombre:String,
    ccosto:Number,
    status: { type:Boolean, default: true }
})
module.exports = mongoose.model('puntos', puntosSchema )
