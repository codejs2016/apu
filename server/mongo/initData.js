var mongoose = require("mongoose");
const configModel = require("../mongo/config");
const configRangos = require("../mongo/rangos");

exports.createConfig = function()
{  
    var config = new configModel({
      territorio: 1660,
      nombre:"ALCALDIA",
      ccosto:1087,
      tipo:'A',
    }) 
    config.save();
}

exports.createRango = function()
{  
    var config = new configRangos({
      topeB:2000000,
      topeA:1000000,
      noventa:90,
      setentaycinco:75
    }) 
    config.save();
}