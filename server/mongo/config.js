const mongoose = require('mongoose')

const configSchema = new mongoose.Schema
({
    territorio:Number,
    nombre:String,
    ccosto:Number,
    porcentaje:Number,
    ccosto:Number,
    recogida:String,
    tipo: String,
    recogida: { type:String, default: '' },
    fechafinal: String,
    valor: Number,
    status: { type:Boolean, default: true }
})
module.exports = mongoose.model('config', configSchema )
