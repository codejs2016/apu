import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { RegisterComponent } from './component/register/register.component';
import { CortesComponent } from './component/cortes/cortes.component';
import { PagosComponent } from './component/pagos/pagos.component';
import { ControlcajaComponent } from './component/controlcaja/controlcaja.component';
import { LoginComponent } from './component/login/login.component';
import { AsesoraComponent } from './component/asesora/asesora.component';
import { AdminComponent } from './component/admin/admin.component';
import { ProgramacionComponent } from './component/programacion/programacion.component';
import { PtvsComponent } from './component/ptvs/ptvs.component';
import { RecogidasComponent } from './component/recogidas/recogidas.component';
import { ReccogidapuntoComponent } from './component/reccogidapunto/reccogidapunto.component';
import { TesoreriaComponent } from './component/tesoreria/tesoreria.component';
import { EntregaefectivoComponent } from './component/entregaefectivo/entregaefectivo.component';
import { RtesoreriaComponent } from './component/rtesoreria/rtesoreria.component';
import { AuthGuard } from './guards/auth.guard';
import { SemaforoComponent } from './component/semaforo/semaforo.component';
import { ConfigptvComponent } from './component/configptv/configptv.component';
import { VehiculosComponent } from './component/vehiculos/vehiculos.component';
import { PlanillaComponent } from './component/planilla/planilla.component';
import { CentroefectivoComponent } from './component/centroefectivo/centroefectivo.component';
import { CentroefectivorecibeComponent } from './component/centroefectivorecibe/centroefectivorecibe.component';
import { InformeComponent } from './component/informe/informe.component';
import { RangosComponent } from './component/rangos/rangos.component';

const routes: Routes = [
  { 
    path: 'asesora', component: AsesoraComponent,canActivate: [ AuthGuard ], 
    children: [
    { path: 'register', component: RegisterComponent },
    { path: 'cortes', component: CortesComponent  },
    { path: 'pagos', component: PagosComponent  },
    { path: 'caja', component: ControlcajaComponent },
    { path: 'planilla', component: PlanillaComponent  },
    { path: 'recogidapunto', component: ReccogidapuntoComponent  }
    ]
  },
  { 
    path: 'novedades', component: InformeComponent,  
    //canActivate: [ AuthGuard ]
  },
  { path: 'login', component: LoginComponent },
  { path: 'monitoreo', component: AdminComponent, canActivate: [ AuthGuard ],
    children: [
    { path: 'calendario', component: ProgramacionComponent  },
    { path: 'recogidas', component: RecogidasComponent  },
    { path: 'semaforo', component: SemaforoComponent  },
    { path: 'vehiculos', component: VehiculosComponent  }
  ]
  },
  { path: 'tesoreria', component: TesoreriaComponent, 
   canActivate: [ AuthGuard ], 
    children: [
    { path: 'entrega', component: EntregaefectivoComponent  },
    { path: 'semaforo', component: SemaforoComponent  },
    { path: 'parametrizar', component: ConfigptvComponent  },
    { path: 'rangos', component: RangosComponent  },
    { path: 'novedades', component: InformeComponent  }
   ]
  },
  { path: 'centroefectivo', component: CentroefectivoComponent, canActivate: [ AuthGuard ], 
  children: [
    { path: 'recepcion', component: CentroefectivorecibeComponent  },
    { path: 'verificacion', component: RtesoreriaComponent },
    { path: 'semaforo', component: SemaforoComponent  }
  ]
  },
  {
    path: '**',
    redirectTo: 'login'
  }
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { useHash: true }),
    CommonModule
  ],
  exports:[
    RouterModule,
  ],
  declarations: []
})
export class AppRoutingModule { }
