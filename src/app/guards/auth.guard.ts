import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router, CanActivateChild } from '@angular/router';
import { Observable } from 'rxjs';

import { map, take } from  'rxjs/operators'
import { LoginService } from '../services/login.service';
import { ConfigService } from '../services/config-service';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements  CanActivate {

  constructor(private loginService: LoginService, 
    private router: Router,
    private configuration: ConfigService
    ){ }
    
  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean | import("@angular/router").UrlTree | Observable<boolean | import("@angular/router").UrlTree> | Promise<boolean | import("@angular/router").UrlTree> {
    return this.loginService.isLoggedIn(sessionStorage.getItem('token'))
    .pipe(take(1))
    .pipe(map( (res:any) => {
     if(res.status){
        return true
     }
     else{
       this.logout()
       return false
     }
  }))
  }

  logout()
  {
    sessionStorage.removeItem('token')
    sessionStorage.removeItem('loginuser')
    sessionStorage.removeItem('territorio')
    sessionStorage.removeItem('username')
    sessionStorage.removeItem('tokensession')
    sessionStorage.removeItem('role')
    this.router.navigate(['login'])
  }
  
}
