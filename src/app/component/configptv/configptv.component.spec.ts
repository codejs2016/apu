import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ConfigptvComponent } from './configptv.component';

describe('ConfigptvComponent', () => {
  let component: ConfigptvComponent;
  let fixture: ComponentFixture<ConfigptvComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ConfigptvComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ConfigptvComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
