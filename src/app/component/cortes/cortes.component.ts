import { Component, OnInit } from '@angular/core';
import { Socket } from 'ngx-socket-io';
import { WebsocketService } from 'src/app/services/websocket.service';
import { CortesService } from 'src/app/services/cortes.service';
import swal from 'sweetalert2';
import { DataService } from 'src/app/services/data.service';

@Component({
  selector: 'app-cortes',
  templateUrl: './cortes.component.html',
  styleUrls: ['./cortes.component.css']
})
export class CortesComponent implements OnInit {

  public allCortes : any 
  public suma = 0
  public spinner = false
  public dateNow = new Date()
  public p: number = 1

  constructor(
    private websocketService: WebsocketService,
    private cortesService: CortesService,
    private dataService: DataService) { 
  }

  ngOnInit() 
  {
    this.getCortesById(this.dataService.getSessionUser().loginuser) 
  }

  showloader(){
    this.spinner = true
  }

  hideloader(){
    this.spinner = false
  }

  getCortesById(id: string)
  {
      this.showloader()
      this.cortesService.getCorteById(id).subscribe( (res:any) => {
        if(res.status)
        {
          for(let i = 0; i < res.info.length; i++) 
          {
            res.info[i].corte  = 0
            res.info[i].status = true
            res.info[i].corte += i+1 
            this.suma += res.info[i].totalefectivo << 0

          }
          this.allCortes = res.info
        }
        this.hideloader()
      }, error => {
        swal.fire("Aviso","Ocurrio un error al consultar cortes", "warning")
        this.hideloader()
     })
  }

  showLoder()
  {
    swal.fire({
      text: 'Por favor espere un momento ...',
      imageUrl: '/assets/animation/Eclipse-1s-200px.svg',
      imageWidth: 100
    })
  }

  closeLoader()
  {
    swal.close()
  }

  setStatus(){
    this.allCortes.forEach((i:any) => { i.status = !i.status })
  }

  printer(item:any, index:number)
  {
    item.totalmonedas =  parseInt(item.totalmonedas)
    item.corte = index
    const swalWithBootstrapButtons = swal.mixin({
      customClass: {
        confirmButton: 'btn btn-success',
        cancelButton: 'btn btn-danger'
      },
      buttonsStyling: false
    })
      
    swalWithBootstrapButtons.fire({
      title: 'Está seguro de imprimir relación?',
      text:'Al confirmar se imprimira el recibo de este corte, recuerde cambiar a papeleria blanca.',
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Si',
      cancelButtonText: 'No',
      reverseButtons: true
    }).then((result) => {
    if (result.value)
    {
      this.setStatus()
      this.showLoder()
      item.iduser = sessionStorage.getItem('loginuser')
      let data = {
        corte:index,
        item:item
      }
      this.websocketService.printerCorte(data)

      setTimeout(() => {
          this.closeLoader()
          this.setStatus()
      }, 3000);
     }
    })
  }

}
