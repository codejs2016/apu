import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReccogidapuntoComponent } from './reccogidapunto.component';

describe('ReccogidapuntoComponent', () => {
  let component: ReccogidapuntoComponent;
  let fixture: ComponentFixture<ReccogidapuntoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReccogidapuntoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReccogidapuntoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
