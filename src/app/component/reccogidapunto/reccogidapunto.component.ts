import { Component, OnInit } from '@angular/core';
import { FormGroup, AbstractControl, FormBuilder, Validators } from '@angular/forms';
import { GuardasService } from 'src/app/services/guardas.service';
import swal from 'sweetalert2';
import { ConfigService } from 'src/app/services/config-service';
import { NgbModal, ModalDismissReasons, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { WebsocketService } from 'src/app/services/websocket.service';
import { Socket } from 'ngx-socket-io';
import { PuntosService } from 'src/app/services/puntos.service';


@Component({
  selector: 'app-reccogidapunto',
  templateUrl: './reccogidapunto.component.html',
  styleUrls: ['./reccogidapunto.component.css']
})
export class ReccogidapuntoComponent implements OnInit {

  public formValidateGuarda: FormGroup
  public formZona: FormGroup
  public formSaveRecogida: FormGroup
  public user: AbstractControl 
  public password : AbstractControl
  public zona: AbstractControl 
  public ptv: AbstractControl 

  public cantidad: AbstractControl 
  public novedad: AbstractControl 
  public observacion: AbstractControl 
  public candado: AbstractControl 
  public tula: AbstractControl 
  public veedor: AbstractControl 

  public submitted = false
  public showlogin = true
  public showParasms = false
  public showPoints = false
  public showTool = false
  public showNoveda = false
  public spinner = false
  public showFormRG = false
  public cntTulas = 0
  public zonas: any
  public puntosVentas = []
  public closeResult: string
  public modalRef: NgbModalRef
  public obsSg : any
  
  constructor(private modalService: NgbModal,
              private formBuilder: FormBuilder,
              private config: ConfigService,
              private guardaService: GuardasService,
              private webSockeService: WebsocketService,
              private puntosServices: PuntosService,
              private socket: Socket) { 
    this.formValidateGuarda = this.formBuilder.group({
      user: ['', Validators.required ],
      password: ['', Validators.required ],
    })

    this.user = this.formValidateGuarda.controls['user']
    this.password = this.formValidateGuarda.controls['password']

    this.formZona = this.formBuilder.group({
      zona: ['', Validators.required ],
      ptv: ['', Validators.required ]
    })
    this.zona = this.formZona.controls['zona']
    this.ptv = this.formZona.controls['ptv']

    this.onSuccess()
  }

  initFormRecogida(){
    
    this.formSaveRecogida = this.formBuilder.group({
      cantidad: [ this.cntTulas, Validators.required ],
      novedad: [0, Validators.required ],
      observacion: ['' ],
      candado: ['', Validators.required ],
      tula: ['', Validators.required ],
      veedor: [ sessionStorage.getItem('loginuser') ,Validators.required],
      recolector: [''],
      idterritorio: ['']
    })

    this.cantidad = this.formSaveRecogida.controls['cantidad']
    this.novedad = this.formSaveRecogida.controls['novedad']
    this.observacion = this.formSaveRecogida.controls['observacion']
    this.candado = this.formSaveRecogida.controls['candado']
    this.tula = this.formSaveRecogida.controls['tula']
    this.veedor = this.formSaveRecogida.controls['veedor']
  }

  ngOnInit() {
   this.zonas = this.getZonas()
  }

  show(){
    this.spinner = true
  }

  hide(){
    this.spinner = false
  }


  get f() {
    return this.formValidateGuarda.controls;
  }

  get fz() {
    return this.formZona.controls;
  }

  get fr() {
    return this.formSaveRecogida.controls;
  }

  searchPointByZona(idzona:any){
   this.showPoints = true
   this.show()
   this.puntosServices.getZonas(idzona).subscribe( (res:any) =>{
     if(res.status){
       sessionStorage.setItem('zonarecogida',idzona)
       for(let i = 0; i < res.info.length; i++) {
         if(res.info[i].id == parseInt(sessionStorage.getItem('territorio')))
         {
          res.info[i].status = true
         }
         else
          res.info[i].status = false
       }
       this.puntosVentas = res.info
       this.hide()
     }else
     {
      this.hide()
      swal.fire("No se encontraron puntos de ventas para esta zona")
     }
   }, error =>{
     swal.fire("","Ocurrio un error al consultar puntos de ventas","warning")
     this.hide()
   })
  }

  getObservacionesSeguridad(){
    this.puntosServices.getObservacionesSeguridad().subscribe( (res:any) =>{
      if(res.status){
        this.obsSg = res.info
        this.hide()
      }
    }, error =>{
      swal.fire("","Ocurrio un error al consultar observaciones","warning")
      this.hide()
    })
  }

  save(){
    this.submitted = true
    if(this.formZona.invalid)
      return false
  }

  validate(){
    this.submitted = true
    if(this.formValidateGuarda.invalid)
     return false
    
    this.show()
    this.guardaService.validateGuarda(this.formValidateGuarda.value).subscribe( (res:any)=>{
       if(res.status){
          this.showlogin = false
          sessionStorage.setItem('recolector',res.data[0][5])
          this.viewdata(sessionStorage.getItem('territorio'))
          this.showFormRG = true
          this.hide()
       }
       else{
         swal.fire("Aviso ",res.msg, "warning")
         this.hide()
        }
      }, error =>{
         swal.fire("","Ocurrio un error al validar usuario ","warning")
         this.hide()
     })
  }

  getZonas(){
    return this.config.getConfig().zonas
  }

  viewdata(id:any)
  {
    this.show()
      this.puntosServices.getCantidadTulas(parseInt(id)).subscribe( (res:any)=>{
         if(res.status){
           if(res.cantidad > 0){
            this.getObservacionesSeguridad()
            this.showFormRG = true
            this.cntTulas = res.cantidad
            this.initFormRecogida() 
           }else{
            swal.fire("Aviso","No se encuentrarón registros de efectivo en el punto de venta","warning")
            this.showFormRG = false
           }
           this.hide()
        }
        else
         this.hide()
  
      }, error =>{
        this.hide()
        swal.fire("Aviso","Ocurrio un error al consultar recogidad del punto de venta","warning")
      })
  }

  showLoder()
  {
    swal.fire({
      text: 'Por favor espere un momento ...',
      imageUrl: '/assets/animation/Eclipse-1s-200px.svg',
      imageWidth: 100
    })
  }

  closeLoader()
  {
    swal.close()
  }
  
  saveRecogida(){
    this.submitted = true
    if(this.formSaveRecogida.invalid)
     return false

     const swalWithBootstrapButtons = swal.mixin({
       customClass: {
         confirmButton: 'btn btn-success',
         cancelButton: 'btn btn-danger'
       },
       buttonsStyling: false
     })
       
     swalWithBootstrapButtons.fire({
       title: 'Está seguro de registrar recogida ?',
       type: 'warning',
       showCancelButton: true,
       confirmButtonColor: '#3085d6',
       cancelButtonColor: '#d33',
       confirmButtonText: 'Si',
       cancelButtonText: 'No',
       reverseButtons: true
     }).then((result) => {
     if (result.value)
     {
       this.showLoder()
       this.formSaveRecogida.get('idterritorio').setValue(sessionStorage.getItem('territorio'))
       this.formSaveRecogida.get('recolector').setValue(sessionStorage.getItem('recolector'))

       let info ={
        zona: sessionStorage.getItem('zonarecogida'),
        territorio:parseInt(sessionStorage.getItem('territorio')),
        recogida:this.formSaveRecogida.value
       }
       this.webSockeService.sendRecogida(info)
       this.submitted = false
       this.showFormRG = false
       this.formSaveRecogida.reset()
       this.closeLoader()
       swal.fire("Registro","Punto Recogido satisfactoriamente", "success")
      }
     },error =>{
      this.closeLoader()
      swal.fire("Aviso","Ocurrio un error al registrar recogida ","warning")
    })
  }

  onSuccess(){
    this.socket.on("notifica", (data:any)=>{
      console.log(  data );
      this.puntosVentas = []
      
    })
  }

  getNovedad(obs:any){
    if(obs == 0){
      this.showNoveda = false
      this.formSaveRecogida.get('observacion').setValue(null);
    }
    else
     this.showNoveda = true
  }

  validateNumber(event: any, opc:number)
  { 
    if(event.target.value.length > 0){
      event.target.value = (event.target.value + '').replace(/[^0-9]/g, '')
      if(opc == 1)
      {
        if(event.target.value == 0 || event.target.value < 0 ){
          swal.fire("Aviso","Por favor ingresar un número de candado valido", "warning")
        }
        else{
          if(event.target.value > 600 ){
            swal.fire("Aviso","Número de candado no permitido", "warning")
            event.target.value = ""
          }
        }
      }
      else
      {
        if(event.target.value == 0 || event.target.value < 0 ){
          swal.fire("Aviso","Por favor ingresar un número de tula valido ", "warning")
        }
        else
        {
          if(event.target.value > 600 ){
            swal.fire("Aviso","Número de tula no permitido", "warning")
            event.target.value = ""
          }
        }
      }
    }
  }  
}
