import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EntregaefectivoComponent } from './entregaefectivo.component';

describe('EntregaefectivoComponent', () => {
  let component: EntregaefectivoComponent;
  let fixture: ComponentFixture<EntregaefectivoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EntregaefectivoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EntregaefectivoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
