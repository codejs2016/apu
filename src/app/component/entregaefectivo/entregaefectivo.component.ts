import { Component, OnInit } from '@angular/core';
import { FormGroup, AbstractControl, FormBuilder, Validators } from '@angular/forms';
import { GuardasService } from 'src/app/services/guardas.service';
import { TesoreriaService } from 'src/app/services/tesoreria.service';
import swal from 'sweetalert2';
import { WebsocketService } from 'src/app/services/websocket.service';
import { Socket } from 'ngx-socket-io';
import { DataService } from 'src/app/services/data.service';

@Component({
  selector: 'app-entregaefectivo',
  templateUrl: './entregaefectivo.component.html',
  styleUrls: ['./entregaefectivo.component.css']
})
export class EntregaefectivoComponent implements OnInit {

  public guardas: any
  public recogidas : number
  public observacion: any
  public descripcion : string
  public nota : string
  public spinner = false
  public showRecogida = false
  public submitted = false
  public status = false
  public obs = null
  public count = 0
  public restante = 0
  public obstula = 1
  public obscandado = 1
  public idrecogida = 0
  public faltante: number
  
  public guarda : any
  public entregas: any

  public closeResult: string

  public formRegister: FormGroup
  public formRecogida: FormGroup
  public formValidate: FormGroup
  public id: AbstractControl
  public idplaca: AbstractControl
  public idruta: AbstractControl

  
  public idnovedad: AbstractControl
  public password: AbstractControl

  constructor( 
    private formBuilder: FormBuilder,
    private guardasService: GuardasService,
    private tesoreriaService: TesoreriaService,
    private webSocketService: WebsocketService,
    private dataService: DataService,
    private socket: Socket) {

    this.formRegister = this.formBuilder.group({
      id: ['0', [ Validators.required ] ],
      idplaca: ['', [Validators.required ] ],
      idruta: ['', [Validators.required ] ]
    })

    this.id = this.formRegister.controls['id']
    this.idplaca = this.formRegister.controls['idplaca']
    this.idruta = this.formRegister.controls['idruta']

    this.formRecogida = this.formBuilder.group({
      idnovedad: [1, [ Validators.required ] ]
    })
    this.idnovedad = this.formRecogida.controls['idnovedad']

    this.formValidate = this.formBuilder.group({
      password: ['', [ Validators.required ] ]
    })
    this.password = this.formValidate.controls['password']

    this.onSuccessRecogidaByGuarda()
   }

  ngOnInit() {
    this.getAllGuardas()
    this.getObservacion()
  }

  showLoder()
  {
    swal.fire({
      text: 'Por favor espere un momento ...',
      imageUrl: '/assets/animation/Eclipse-1s-200px.svg',
      imageWidth: 100
    })
  }

  closeLoader()
  {
    swal.close()
  }

  setStatus(desc: string){
    if(desc =='SIN NOVEDAD'){
      this.status = false
      this.nota = ''
    }
    else
     this.status = true
  }
  
  get f() {
    return this.formRegister.controls;
  }

  get fr() {
    return this.formRecogida.controls;
  }

  get fv() {
    return this.formValidate.controls;
  }

  getAllGuardas(){
    this.show()
    this.guardasService.get().subscribe( (res:any) =>{
      if(res.status){
        this.guardas =  res.info
        this.hide()
      }
    }, error =>{
      swal.fire("", "Erro al consultar guardas", "warning")
      this.hide()
   })
  }

  show(){
    this.spinner = true
  }

  hide(){
    this.spinner = false
  }

  getObservacion(){
    this.tesoreriaService.getObservacion().subscribe( (res:any) =>{
      if(res.status){
        this.observacion = res.info
      }
    }, error =>{
      swal.fire("", "Erro al consultar observaciones de tesesoreria", "warning")
   })
  }

  onSuccessRecogidaByGuarda() 
  {
    this.socket.on('rguarda', (data: any) => {
      this.showRecogida = true
      this.recogidas = data.dataTransportMonitoreo
      this.count = data.dataTransportMonitoreo.length
      this.hide()
    })
  }

  searchByGuarda(event: number){
     if(event > 0){
      this.guarda = event
      this.show()
      this.tesoreriaService.getRecogidas(event).subscribe( (res:any)=>{
       if(res.status){
        this.showRecogida = true
        if(res.info[0].cantidad > 0)
         this.recogidas = res.info[0].cantidad
        else
         this.recogidas = 0
         
        this.hide()
      }
      else{
        swal.fire("",res.msg, "warning") 
        this.showRecogida = false
        this.hide()
      }
    }, error =>{
       swal.fire("", "Error al consultar recogidas", "warning")
       this.hide()
       this.spinner = false
    })
   }
  }
  
 
 save(){
    this.showLoder()
    const swalWithBootstrapButtons = swal.mixin({
        customClass: {
          confirmButton: 'btn btn-success',
          cancelButton: 'btn btn-danger'
        },
        buttonsStyling: false
      })
        
      swalWithBootstrapButtons.fire({
        title: 'Está seguro de registrar entrega de paquetes?',
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Si',
        cancelButtonText: 'No',
        reverseButtons: true
      }).then((result) => {
      if (result.value)
      {
        if(this.recogidas > 0){
          let dataSend = {
            cantidad: this.recogidas,
            guarda : this.guarda,
            responsable: this.dataService.getSessionUser().loginuser,
            descripcion: this.descripcion,
            nota: this.nota
          }
          this.tesoreriaService.entrega(dataSend).subscribe( (res:any)=>{
            if(res.status){
              swal.fire("Aviso","Entrega de paquetes registrada satisfactoriamente", "success")
              this.recogidas = null
              this.showRecogida = false
              setTimeout(() => {
                this.closeLoader()
              }, 1000);
             }
          },error =>{
             swal.fire("", "Error al registrar entrega de paquetes", "warning")
             this.recogidas = null
             this.hide()
             this.closeLoader()
           })
        }
       }
      })
  }

 
  validateNumber(event: any)
  { 
    event.target.value = (event.target.value + '').replace(/[^0-9]/g, '')
    this.faltante = parseInt( event.target.value )
  }
}
