import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PtvsComponent } from './ptvs.component';

describe('PtvsComponent', () => {
  let component: PtvsComponent;
  let fixture: ComponentFixture<PtvsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PtvsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PtvsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
