import { Component, OnInit } from '@angular/core';
import { ConfigService } from 'src/app/services/config-service';
import { AuthService } from 'src/app/services/auth.service';

@Component({
  selector: 'app-tesoreria',
  templateUrl: './tesoreria.component.html',
  styleUrls: ['./tesoreria.component.css']
})
export class TesoreriaComponent implements OnInit {

  public listModules: any
  
  constructor(private config: ConfigService,
              public authService:AuthService) { }


  ngOnInit() {
    this.getListModules()
  }

  getListModules()
  {
    this.listModules = this.config.getConfig().modulostesoreria
  }

}
