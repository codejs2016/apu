import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CentroefectivoComponent } from './centroefectivo.component';

describe('CentroefectivoComponent', () => {
  let component: CentroefectivoComponent;
  let fixture: ComponentFixture<CentroefectivoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CentroefectivoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CentroefectivoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
