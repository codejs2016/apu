import { Component, OnInit } from '@angular/core';
import { ConfigService } from 'src/app/services/config-service';
import { AuthService } from 'src/app/services/auth.service';

@Component({
  selector: 'app-centroefectivo',
  templateUrl: './centroefectivo.component.html',
  styleUrls: ['./centroefectivo.component.css']
})
export class CentroefectivoComponent implements OnInit {

  public listModules: any
  
  constructor(private config: ConfigService,
             public authService:AuthService) { }


  ngOnInit() {
    this.getListModules()
  }

  getListModules()
  {
    this.listModules = this.config.getConfig().moduloscentroefectivo
  }
}
