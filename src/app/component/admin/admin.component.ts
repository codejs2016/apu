import { Component, OnInit } from '@angular/core';
import { ConfigService } from 'src/app/services/config-service';
import { AuthService } from 'src/app/services/auth.service';

@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.css']
})
export class AdminComponent implements OnInit {

  public listModules: any
  constructor(
    private config: ConfigService,
    public authService: AuthService) { }

  ngOnInit() {
    this.getListModules()
  }

  getListModules()
  {
    this.listModules = this.config.getConfig().modulosmonitoreo
  }
  
}
