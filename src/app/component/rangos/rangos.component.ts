import { Component, OnInit } from '@angular/core';
import { AbstractControl, FormGroup, FormBuilder, Validators } from '@angular/forms';
import swal from 'sweetalert2';
import { SemaforoService } from 'src/app/services/semaforo.service';
  
@Component({
  selector: 'app-rangos',
  templateUrl: './rangos.component.html',
  styleUrls: ['./rangos.component.css']
})
export class RangosComponent implements OnInit {

  public form: FormGroup
  public topeB: AbstractControl 
  public topeA: AbstractControl 
  public noventa: AbstractControl 
  public setentaycinco: AbstractControl 
  public submitted = false
  public spinner = false
  public show = true
  public rangos: any
  public p: number = 1
  
  constructor(
    private formBuilder: FormBuilder,
    private semaforoService: SemaforoService)
    {
    this.form = this.formBuilder.group({
      id:[],
      topeB: ['', Validators.required ],
      topeA: ['', Validators.required ],
      noventa: ['', Validators.required ],
      setentaycinco: ['', Validators.required ]
    })
    this.topeB = this.form.controls['topeB']
    this.topeA = this.form.controls['topeA']
    this.noventa = this.form.controls['noventa']
    this.setentaycinco = this.form.controls['setentaycinco']
  }

  ngOnInit() {
    this.getRangos()
  }


  get f() {
    return this.form.controls;
  }

  showSpinner(){
    this.spinner = true
  }

  hideSpinner(){
    this.spinner = false
  }

  getRangos(){
    this.showSpinner()
    this.semaforoService.getRangos().subscribe((res:any)=>{
       this.rangos = res
       this.hideSpinner()
    }, 
    error =>{
       this.hideSpinner()
       swal.fire("Ocurrio un error al consultar rangos", "warning")
    })
  }

  save(){
    this.submitted = true
    if(this.form.invalid)
     return false

     this.update()
  }

  
  update(){
    this.semaforoService.updateRangos(this.form.value).subscribe( (res:any) =>{
      if(res.status){
       this.form.reset()
       this.submitted = false
       swal.fire("Registro","Rango actualizado satisfactoriamente", "success")
       this.getRangos()
      }
    }, 
    error =>{
      this.hideSpinner()
       swal.fire("Registro","Ocurrio un error al actualizar rangos", "error")
    })
  }

  validateNumber(event:any)
  { 
    event.target.value = (event.target.value + '').replace(/[^0-9]/g, '')
  }

  edit(item: any){
    this.form.get('id').setValue(item._id)
    this.form.get('topeB').setValue(item.topeB)
    this.form.get('topeA').setValue(item.topeA)
    this.form.get('noventa').setValue(item.noventa)
    this.form.get('setentaycinco').setValue(item.setentaycinco)
  }
}
