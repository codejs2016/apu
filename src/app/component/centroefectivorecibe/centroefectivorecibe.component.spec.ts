import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CentroefectivorecibeComponent } from './centroefectivorecibe.component';

describe('CentroefectivorecibeComponent', () => {
  let component: CentroefectivorecibeComponent;
  let fixture: ComponentFixture<CentroefectivorecibeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CentroefectivorecibeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CentroefectivorecibeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
