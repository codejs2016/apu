import { Component, OnInit } from '@angular/core';
import { LoginService } from 'src/app/services/login.service';
import { Router } from "@angular/router";
import swal from 'sweetalert2';
import { ConfigService } from 'src/app/services/config-service';
import $ from "jquery";
import { FormGroup, AbstractControl, FormBuilder, Validators } from '@angular/forms';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  public dateNow = new Date()
  public loading = false
  public submitted = false
  public loginForm: FormGroup
  public username: AbstractControl
  public password: AbstractControl
  
  constructor(private loginService: LoginService, 
              private router: Router,
              private config: ConfigService,
              private formBuilder: FormBuilder) {


      this.loginForm = this.formBuilder.group({
        username: ['', Validators.required],
        password: ['', Validators.required]
      });
  
      this.username = this.loginForm.controls.username
      this.password = this.loginForm.controls.password
   }

  ngOnInit() {
    sessionStorage.removeItem('name')
    sessionStorage.removeItem('iduser')
    sessionStorage.removeItem('token')
  }

  get f() {
    return this.loginForm.controls
  }

  loginUser()
  {
    this.submitted = true;
    if (this.loginForm.invalid) {
      return false
    }
    
    this.loading =  true
    this.loginService.loginUser(this.loginForm.value).subscribe( (res: any) => {
        if(res.status)
        {
          switch(res.data.role)
          {
                case this.config.getConfig().typerol.asesora:
                  this.persisSession(res)
                  this.redirect('asesora')
                break;

                case this.config.getConfig().typerol.jefeseguridad:
                  this.persisSession(res)
                  this.redirect('monitoreo')
                break;

                case this.config.getConfig().typerol.jefetesoreria:
                case this.config.getConfig().typerol.auxtesoreria:
                  this.persisSession(res)
                  this.redirect('tesoreria')
                break;

                case this.config.getConfig().typerol.cajera:
                  this.persisSession(res)
                  this.redirect('centroefectivo')
                break;
         }

        }else{
          swal.fire("Inicio de sesión ","El usuario ingresado no existe", "warning")
          this.loading = false
        }
      },
      error => {
        swal.fire("Inicio de sesión ","Error de comunicación con el servidor", "warning")
        this.loading = false
      })
  }
 

  persisSession(res: any){
    sessionStorage.setItem('token',res.token)
    sessionStorage.setItem('loginuser',res.data.loginuser)
    sessionStorage.setItem('territorio',res.data.territorio)
    sessionStorage.setItem('username',res.data.username)
    sessionStorage.setItem('tokensession',res.data.tokensession)
  }
  
  redirect(role:string){
    switch (role) {
        case 'asesora':
          this.router.navigate(['asesora'])
        break;

        case 'centroefectivo':
          this.router.navigate(['centroefectivo'])
        break;

        case 'monitoreo':
          this.router.navigate(['monitoreo'])
        break;

        case 'tesoreria':
          this.router.navigate(['tesoreria'])
        break;
    
      default:
        break;
    }
  }

  loader() 
  {
		setTimeout(function() { 
			if($('#idloader').length > 0) {
				$('#idloader').removeClass('show');
			}
		}, 600);
	};
	
 
}
