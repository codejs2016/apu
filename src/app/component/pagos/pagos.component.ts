import { Component, OnInit } from '@angular/core';
import swal from 'sweetalert2';
import { PagoService } from 'src/app/services/pago.service';
import { FormGroup, AbstractControl, Validators, FormBuilder } from '@angular/forms';
import { DataService } from 'src/app/services/data.service';
import { ConfigService } from 'src/app/services/config-service';
import { WebsocketService } from 'src/app/services/websocket.service';

@Component({
  selector: 'app-pagos',
  templateUrl: './pagos.component.html',
  styleUrls: ['./pagos.component.css']
})
export class PagosComponent implements OnInit {

  public pagos: any
  public pagoTmp: any
  public formPagos: FormGroup
  public serie: AbstractControl 
  public numero : AbstractControl 
  public submitted = false
  public showTable = false
  public showLoader = true
  public showpay = false
  public valor = null
  public iduser = null
  public showgrid = true
  public showform = false
  public listPagos:any
  
  constructor(private pagoService: PagoService,  
    private dataService: DataService, 
    public configService: ConfigService,
    private websocketService: WebsocketService,
    private formBuilder: FormBuilder)
    { 
    this.formPagos = this.formBuilder.group({
      serie: ['', Validators.required],
      numero: ['', Validators.required],
    })

    this.serie = this.formPagos.controls['serie']
    this.numero = this.formPagos.controls['numero']
  }

  ngOnInit() {
    this.getPagos()
  }

  get f() {
    return this.formPagos.controls;
  }

  show(){
    this.showLoader = true
  }

  hide(){
    this.showLoader = false
  }

  add(){
    this.showTable = true
    this.pagos =  this.pagoTmp
  }

  clear(){
    this.pagos = null
    this.pagoTmp = []
    this.showTable = false
    this.submitted = false
    this.showpay = false
    this.formPagos.reset()
  }

  getPagos(){
    this.show()
    this.pagoService.getPagos(this.dataService.getSessionUser().loginuser).subscribe( (res:any)=>{
      if(res.status == true){
        this.listPagos  = res.info 
      }else 
       swal.fire("Aviso ",res.msg, "warning")
    },
    error => {
      swal.fire("Aviso ","Ocurrio un error al consultar pagos", "warning")
      this.hide()
    })
    this.hide()
  }

  save(){
    this.pagos.cedula = this.dataService.getSessionUser().loginuser;
    const swalWithBootstrapButtons = swal.mixin({
      customClass: {
        confirmButton: 'btn btn-success',
        cancelButton: 'btn btn-danger'
      },
      buttonsStyling: false
    })
      
    swalWithBootstrapButtons.fire({
      title: 'Está seguro de registrar pago ?',
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Si',
      cancelButtonText: 'No',
      reverseButtons: true
    }).then((result) => {
    if (result.value)
    {
     this.show()
     if(this.pagos.cedula != null){
        this.pagoService.save(this.pagos).subscribe( (res:any)=>{
        if(res.status == true){
          this.showgrid  = true
          this.showform = false 
          this.showTable = false
          this.formPagos.reset()
          swal.fire("Aviso ",res.msg, "success")
          this.getPagos()
          this.showformData()
          //this.websocketService.printerPagoPremio(this.pagos)
          this.pagos = null
          this.showTable = false
          this.showpay = false
        }
        else
         swal.fire("Aviso ",res.msg, "warning")

        this.hide()
      },
      error => {
        swal.fire("Aviso","Ocurrio un error al registrar pago", "warning")
        this.hide()
      }) 
    }
     }
    })
  }
  

  validatePago(){
    this.submitted = true
    if(this.formPagos.invalid)
      return false
    
    this.show()
    this.pagoService.searchs(this.formPagos.value).subscribe( (res:any)=>{
       if(res.status){
        if(res.data.totalpremio < this.configService.getConfig().valorpagopremio)
         {
          if(res.data.estadopremio == 1 || res.data.estadopremio == 62){
            this.pagoTmp = res.data
            this.valor = res.data.totalpremio
            this.showpay = true
           }
           else{
             swal.fire("Aviso ","No es premio valido", "warning")
             this.showpay = false
           }
         }
         else
         {
           swal.fire("Aviso","Este pago se debe cancelar por la oficina principal", "warning")
         }
       }
       else
       {  swal.fire("Aviso ",res.msg, "warning")
          this.showpay = false
       }
       this.hide()
    },
    error => {
      swal.fire("Aviso","Ocurrio un error al validar pago", "warning")
      this.hide()
   }) 
  }

  validateNumber(event: any)
  { 
    event.target.value = (event.target.value + '').replace(/[^0-9]/g, '')
  }

  delete()
  {
    console.log("hola");
     this.pagos = null
      /*swal.fire({
      text: 'Está Seguro que de eliminar número?',
      type: 'warning',
      showCancelButton: true,
      cancelButtonText: 'Cancelar',
      confirmButtonText: 'Sí'
    }).then((result) => {
      if (result.value) 
        this.pagos = []
      })*/
  }


  showformData() {
    this.showform = !this.showform
    this.showgrid = !this.showgrid
  }
}
