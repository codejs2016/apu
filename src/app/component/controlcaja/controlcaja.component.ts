import { Component, OnInit } from '@angular/core';
import { CortesService } from 'src/app/services/cortes.service';
import { WebsocketService } from 'src/app/services/websocket.service';
import { Socket } from 'ngx-socket-io';
import { DataService } from 'src/app/services/data.service';

@Component({
  selector: 'app-controlcaja',
  templateUrl: './controlcaja.component.html',
  styleUrls: ['./controlcaja.component.css']
})
export class ControlcajaComponent implements OnInit {

  public allCortes : any 
  public suma = 0
  public sumachance = 0
  public sumagiro = 0
  public sumapagos = 0
  public totalEngtrega = 0
  public ventaGamble = 0
  public saldo = 0
  public countEfectivo = 0
  public countGiros = 0
  public countPagos = 0
  public valorPagos = 0
  public relaciones : number
  public idUserApp = null
  public spinner = false
  public alert : string

  constructor(private cortesService: CortesService,
    private websocketService: WebsocketService,
    private dataService: DataService,
    private socket: Socket) {
      this.getSums()
     }

  ngOnInit() {
    this.idUserApp = this.dataService.getSessionUser().loginuser
    //this.getCortesById(this.dataService.getSessionUser().loginuser)
    this.onSuccessSums()
  }

  onSuccessSums() 
  {
    this.websocketService.getSums({ iduser:this.idUserApp })
  }

  getSums(){
    this.socket.on('getSums', (data: any) => {
       if(data.idUser == this.idUserApp){
         this.sumagiro = parseInt(data.dataTransport[0].giros)
         this.countGiros = parseInt(data.dataTransport[0].countgiro)

         this.sumachance = parseInt(data.dataTransport[1].chance)
         this.countEfectivo = parseInt(data.dataTransport[1].countchance)
         
         this.sumapagos = parseInt(data.dataTransport[2].pagos)
         this.countPagos = parseInt(data.dataTransport[2].countpagos)
         
         this.totalEngtrega = this.sumagiro + this.sumachance + this.sumapagos
         this.ventaGamble = parseInt(data.dataTransport[3].ventagamble)

         this.saldo = this.ventaGamble - this.totalEngtrega

         if(this.saldo >= 30000){
            this.alert = "Comunicarse con el departamento de cartera..."
         }

         if(this.saldo >= 15000 && this.saldo<30000){
           this.alert = "Esta Proxima a cartera..."
         }

         if(this.saldo > 0 && this.saldo < 15000){
          this.alert = "Esta presentando diferencia..."
         }

         if(this.saldo == 0){
          this.alert = "Esta a paz y salvo..."
         }

         if(this.saldo < 0){
          this.alert = "Saldo a favor..."
         }
       }
    })
  }

  getCortesById(id: any)
  {
      this.showloader()
      this.cortesService.getCorteById(id).subscribe( (res:any) => {
        console.log("res ", res );
        if(res.status == true)
        {
          this.allCortes = res.info
          for (let i = 0; i < res.info.length; i++) 
          {
            /*if(res.info[i].tipo == "GIROS"){
              this.countGiros += res.info[i].tipo << 0
            }

            if(res.info[i].tipo == "CHANCE"){
              this.countEfectivo += res.info[i].tipo << 0
            }*/
            this.suma += res.info[i].totalefectivo << 0
          }
        }
        this.hideloader()
      })
  }

  showloader(){
    this.spinner = true
  }

  hideloader(){
    this.spinner = false
  }


}
