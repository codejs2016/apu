import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { WebsocketService } from 'src/app/services/websocket.service';
import { Socket } from 'ngx-socket-io';
import swal from 'sweetalert2';
import { SemaforoService } from 'src/app/services/semaforo.service';

@Component({
  selector: 'app-semaforo',
  templateUrl: './semaforo.component.html',
  styleUrls: ['./semaforo.component.css']
})
export class SemaforoComponent implements OnInit {

  public puntos: any 
  public pred = []
  public pyellow = []
  public pgreen = []

  constructor(
    private websocketService: WebsocketService,
    private semaforoService: SemaforoService,
    private socket: Socket,
    private ref: ChangeDetectorRef
    ) { 
      this.onSuccess()
    }

  ngOnInit()
  {
    this.getSemaforo()
  }

  getSemaforo(){
    this.showLoder()
    this.pred = []
    this.pyellow = []
    this.pgreen = []
    this.semaforoService.getSemaforo().subscribe( (res:any) =>{
       res.forEach((i:any) => {
         switch(i.ccosto) 
         {
           case 1087:
            i.ccosto = 'Zona Norte'
           break;
           case 1151:
            i.ccosto = 'Zona Centro'
           break;
           case 1152:
            i.ccosto = 'Zona Sur'
           break;
         }

         switch(i.recogida) 
         {
           case 'red':
            this.pred.push(i)
           break;
           case 'yellow':
            this.pyellow.push(i)
           break;
           
           default:
            this.pgreen.push(i)
           break;
        }
       })
       this.closeLoader()
    },
     error => {
      this.closeLoader()
      swal.fire("Aviso","Ocurrio un error al generar semaforo", "warning")
    })
  }
  getStatus()
  {
    this.showLoder();
    this.websocketService.sendSemaforo();
    this.closeLoader();
  }


  showLoder()
  {
    swal.fire({
      text: 'Por favor espere un momento ...',
      imageUrl: '/assets/animation/Eclipse-1s-200px.svg',
      imageWidth: 100,
    })
  }

  closeLoader()
  {
    swal.close()
  }

  onSuccess() 
  {
    this.socket.on('semaforo', (data: any) => {
      this.getSemaforo()  
    })
  }

}
