export const configuration = {
   bussinesServer:{
    //url:'http://192.168.41.15:4000'
    url:'http://172.16.20.10:4000' 
   },
   initComponent:{
    login:'Entrar',
    register:'Registrar',
    cefectivo:'Control de Efectivo',
    },
    loginComponent:{
     titlte:'Control de Efectivo',
     placeholderUser:'Usuario',
     placeholderPassword:'Contraseña',
     textLogin:'Iniciar sesión' 
    },
    resetComponent:{
      titlte:'Control de Efectivo',
      placeholderUser:'Usuario',
      textreset:'Recuperar contraseña' 
     },
     todo:{
      name:'Todo'
     },
    registerComponent:{
      placeholderDocument:'Número de documento',
      placeholderType:'Tipo de Documento',
      placeholderName:'Nombres',
      placeholderSurName:'Apellidos',
      placeholderGender:'Genero',
      placeholderAdress:'Dirección',
      placeholderPhone:'Celular',
      placeholderEmail:'Correo',
      placeholderUser:'Usuario',
      placeholderPassword:'Contraseña',
      placeholderRepeatPassword:'Repetir Contraseña',
      accept:'Aceptar',
      yes:'Si',
      no:'No'
    },
    mdcalidad:[
      { name:'Novedades', icon:'ion-md-stats', call:'novedades' },
    ],
    modulos:[
      { name:'Control de efectivo', icon:'ion-ios-calculator', call:'register' },
      { name:'Historial de cortes', icon:'ion-ios-calendar', call:'cortes' },
      { name:'Pago de Premios', icon:'ion-md-wallet', call:'pagos' },
      //{ name:'Control de caja', icon:'ion-ios-stats', call:'caja' },
      { name:'Recogida', icon:'ion-ios-unlock', call:'recogidapunto' },
      { name:'Planilla', icon:'ion-ios-calculator', call:'planilla' }
    ],
    modulosmonitoreo:[
      { name:'Calendario', icon:'ion-ios-calendar', call:'calendario' },
      //{ name:'Puntos', icon:'ion-ios-apps', call:'ptvs' },
      { name:'Recogidas', icon:'ion-ios-basket', call:'recogidas' },
      { name:'Semaforo', icon:'ion-ios-basket', call:'semaforo' },
      { name:'Vehiculos', icon:'ion-ios-car', call:'vehiculos' },
    ],
    modulostesoreria:[
      { name:'Entregas', icon:'ion-ios-calculator', call:'entrega' },
      { name:'Novedades', icon:'ion-md-stats', call:'novedades' },
      { name:'Parametrizar', icon:'ion-md-wallet', call:'parametrizar' },
      { name:'Rangos', icon:'ion-md-options', call:'rangos' },
      { name:'Semaforo', icon:'ion-ios-basket', call:'semaforo' }
    ],
    moduloscentroefectivo:[
      { name:'Recepción', icon:'ion-ios-calculator', call:'recepcion' },
      { name:'Verificación', icon:'ion-ios-calendar', call:'verificacion' },
      //{ name:'Semaforo', icon:'ion-ios-basket', call:'semaforo' },
      //{ name:'Parametrizar', icon:'ion-md-wallet', call:'parametrizar' }
    ],
    typenumbers:
    {
      ciemil : 100000,
      cincuentamil : 50000,
      veintemil : 20000,
      diezmil : 10000,
      cincomil: 5000,
      dosmil : 2000,
      mil : 1000
    },
    billetes:
    [ 
      { cienmil : 100000 },
      { cincuentamil : 50000 },
      { veintemil : 20000 },
      { diezmil : 10000 },
      { cincomil: 5000 },
      { dosmil : 2000 },
      { mil : 1000 }
    ],
    typservices:[
      { id:1, name:'Chance' },
      { id:2, name:'Giros' }
    ],
    typerol:{
      asesora:'ASESORA',
      auxtesoreria:'AUXILIAR DE TESORERIA',
      cajera:'CAJERO (A)',
      guarda:'GUARDA DE SEGURIDAD',
      jefeseguridad:'JEFE DE SEGURIDAD',
      jefetesoreria:'JEFE DE TESORERIA',
    },
    zonas:[
      { id:1087, name:'Zona Norte', color:'btn-outline-warning' },
      { id:1125, name:'Zona Centro', color:'btn-outline-primary' },
      { id:1152, name:'Zona Sur', color:'btn-outline-danger'  },
      { id:1193, name:'Zona Dagua', color:'btn-outline-success'}
    ],
    ccostos:[
      { id:1078, name:'SEDE PRINCIPAL'},
      { id:1087, name:'PUNTO VENTA FIJO NORTE' },
      { id:1088, name:'MAQUINAS PORTATIL NORTE'  },
      { id:1125, name:'PUNTO VENTA FIJO CENTRO'},
      { id:1126, name:'MAQUINAS PORTATIL CENTRO',  },
      { id:1153, name:'MAQUINAS PORTATIL SUR' },
      { id:1193, name:'OFICINA DAGUA'  },
      { id:1152, name:'PUNTO VENTA FIJO SUR'},
      { id:1185, name:'CORDOBA',  },
      { id:1186, name:'BOCANA' },
      { id:1187, name:'JUANCHACO'  },
      { id:1240, name:'MALAGA'},
      { id:1238, name:'OFICINAS GIROS',  },
      { id:1229, name:'JUANCHACO_COMISION',  },
      { id:1287, name:'MOVILES BUENAVENTURA' }
    ],
    valorpagopremio:350000,
    rolsecuriry:[
      { id:1,  name: 'conductor' },
      { id:2, name: 'tripulante1' },
      { id:3, name: 'tripulante2' }, 
      { id:4, name: 'tulero' }
    ],
    tipocambios:[
      { id:1, name:'CAMBIO PLACA'},
      { id:2, name:'CAMBIO RUTA'},
      { id:3, name:'CAMBIO ROL'},
      { id:4, name:'CAMBIO PLACA Y RUTA'},
      { id:5, name:'CAMBIO PLACA Y ROL'},
      { id:6, name:'CAMBIO RUTA Y ROL'},
      { id:7, name:'CAMBIO PLACA, RUTA Y ROL'}
    ]
  }