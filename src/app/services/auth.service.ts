import { Injectable } from '@angular/core';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  public isLoggedIn: Boolean
  public user: any

  constructor(private router: Router) {

  }

  logout()
  {
    sessionStorage.removeItem('token')
    sessionStorage.removeItem('loginuser')
    sessionStorage.removeItem('territorio')
    sessionStorage.removeItem('username')
    sessionStorage.removeItem('tokensession')
    sessionStorage.removeItem('role')
    this.router.navigate(['/'])
  }

}
