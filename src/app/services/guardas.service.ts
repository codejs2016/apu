import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ConfigService } from './config-service';

@Injectable({
  providedIn: 'root'
})
export class GuardasService {

  
  readonly URL_API = this.config.getConfig().bussinesServer.url
  constructor(
    private http: HttpClient, 
    private config:ConfigService)
  {}
   
  get()
  {
    return this.http.get(this.URL_API+"/api/guardas")
  }

  getVehiculos()
  {
    return this.http.get(this.URL_API+"/api/guardas/vehiculos")
  }

  getRutas()
  {
    return this.http.get(this.URL_API+"/api/guardas/rutas")
  }

  saveProgramacion(data: any)
  {
    return this.http.post(this.URL_API+"/api/guardas/saveprogramacion",data)
  }

  saveAgenda(data: any)
  {
    return this.http.post(this.URL_API+"/api/programacion",data)
  }

  updateGuaraAgenda(data: any)
  {
    return this.http.post(this.URL_API+"/api/programacion/update/guarda",data)
  }

  deleteAgenda(data: any)
  {
    return this.http.post(this.URL_API+"/api/programacion/deleteagenda/",data)
  }

  deleteGuardaAgenda(data: any)
  {
    return this.http.post(this.URL_API+"/api/programacion/delete/guarda",data)
  }

  validateGuarda(data:any)
  {
    return this.http.post(this.URL_API+"/api/guardas/validate",data)
  }

  getProgramacion()
  {
    return this.http.get(this.URL_API+"/api/programacion")
  }

  getProgramacionDays()
  {
    return this.http.get(this.URL_API+"/api/programacion/days")
  }

  getRecurso(id:number)
  {
    return this.http.get(this.URL_API+`/api/programacion/recurso/${id}`)
  }

  status(data: any)
  {
    return this.http.post(this.URL_API+`/api/programacion/status/`,data)
  }
}


