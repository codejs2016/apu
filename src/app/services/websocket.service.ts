import { Injectable } from '@angular/core';
import { Socket } from 'ngx-socket-io';

@Injectable({
  providedIn: 'root'
})
export class WebsocketService{
  
  constructor(private socket: Socket)
  {

  }

  sendEfectivo(data: any)
  {
    this.socket.emit('efectivo', data)
  }

  sendSemaforo()
  {
    this.socket.emit('semaforo')
  }

  sendRecogida(data: any)
  {
    this.socket.emit('notificar', data)
  }
  
  getSums(data: any)
  {
    this.socket.emit('getSums', data)
  }
  
  getCortes(data: any)
  {
    this.socket.emit('getefectivocortes', data)
  }

  getMonitoreo()
  {
    this.socket.emit('monitoreozonas')
  }

  getRecogidasByGuarda(id:any)
  {
    this.socket.emit('rguarda', id)
  }

  printerCorte(data: any)
  {
    this.socket.emit('printcorte', data)
  }

  printCorteCE(data: any)
  {
    this.socket.emit('printcorteCE', data)
  }

  printerPagoPremio(data: any)
  {
    this.socket.emit('printerPagoPremio', data)
  }

  printerPlanilla(data: any)
  {
    this.socket.emit('printerPlanilla', data)
  }

  getObsGD(data: any)
  {
    this.socket.emit('getObsGD', data)
  }
  
}
