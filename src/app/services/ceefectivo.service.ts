import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ConfigService } from './config-service';

@Injectable({
  providedIn: 'root'
})
export class CentroEfectivoService {

  
  readonly URL_API = this.config.getConfig().bussinesServer.url
  constructor(
    private http: HttpClient, 
    private config:ConfigService)
  {}
   
  get()
  {
    return this.http.get(this.URL_API+"/api/cefectivo")
  }

  all(data: any)
  {
    return this.http.post(this.URL_API+"/api/cefectivo",data)
  }

  getEntregas()
  {
    return this.http.get(this.URL_API+"/api/cefectivo/entregas")
  }
  

}


