import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ConfigService } from './config-service';

@Injectable({
  providedIn: 'root'
})
export class TesoreriaService {

  readonly URL_API = this.config.getConfig().bussinesServer.url
  constructor(
    private http: HttpClient, 
    private config:ConfigService)
  {}

  getRecogidas(id:any)
  {
    return this.http.get(this.URL_API+"/api/tesoreria/"+`${id}`)
  }

  getObservacion()
  {
    return this.http.get(this.URL_API+"/api/tesoreria/")
  }

  save(data:any)
  {
    return this.http.post(this.URL_API+"/api/tesoreria/",data)
  }

  searhCortes(data:any)
  {
    return this.http.post(this.URL_API+"/api/tesoreria/cortes",data)
  }

  novedades(data:any)
  {
    return this.http.post(this.URL_API+"/api/tesoreria/novedades",data)
  }
  
  verificacion(data:any)
  {
    return this.http.post(this.URL_API+"/api/tesoreria/verificacion",data)
  }

  verificacionok(data:any)
  {
    return this.http.post(this.URL_API+"/api/tesoreria/verificacionok",data)
  }

  entrega(data:any)
  {
    return this.http.post(this.URL_API+"/api/tesoreria/entrega",data)
  }
  
}
