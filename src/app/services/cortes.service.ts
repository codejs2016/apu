import { Injectable } from '@angular/core';
import { HttpClient } from "@angular/common/http";
import { ConfigService } from "./config-service";

@Injectable({
  providedIn: 'root'
})
export class CortesService {

  readonly URL_API = this.config.getConfig().bussinesServer.url;

  constructor(private http: HttpClient, private config: ConfigService) {}
  
  getCorteById(_id: string) {
    return this.http.get(this.URL_API + "/api/cortes" + `/${_id}`);
  }
  

  getRegister(_id: any) {
    return this.http.get(this.URL_API + "/api/recibe" + `/${_id}`);
  }
  


}
