import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ConfigService } from './config-service';

@Injectable({
  providedIn: 'root'
})
export class PuntosService {

  readonly URL_API = this.config.getConfig().bussinesServer.url
  constructor(
    private http: HttpClient, 
    private config:ConfigService)
  {}
   
  getZonas(id:any)
  {
    return this.http.get(this.URL_API+"/api/puntos/"+`${id}`)
  }

  getObservacionesSeguridad()
  {
    return this.http.get(this.URL_API+"/api/seguridad/")
  }

  getCantidadTulas(id:number)
  {
    return this.http.get(this.URL_API+"/api/puntos/info/"+`${id}`)
  }
}


