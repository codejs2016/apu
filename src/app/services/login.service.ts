import { Injectable, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ConfigService } from './config-service';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class LoginService implements OnInit {

  readonly URL_API = this.config.getConfig().bussinesServer.url
  public loggiedInStatus = false
 
  constructor(
    private http: HttpClient, 
    private config:ConfigService
    ) 
   {
   }

  ngOnInit(){}

  loginUser(dataUser: any)
  {
    return this.http.post(this.URL_API+'/api/login', dataUser)
  }

  isLoggedIn(token: string): Observable<isLoggedIn>
  {
    return this.http.get<isLoggedIn>(this.URL_API+'/api/login/isLoggedIn'+ `/${token}`)
  }
}

